package implementations;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.ICouponsDao;
import entities.Coupon;

public class Coupons implements ICouponsDao {
	private static int numCouponsPerPage = 5; // Determines how many coupons will be shown in a single page.
	private static SessionFactory factory; // session factory instance reference.
	private static Coupons coupons = null; // self object retrieved in the singleton function.
	private Session session;

	
	/**
	 * Default constructor. setting up the factory instance reference.
	 */
	
	private Coupons() {
		factory = Controller.getSessionFactroy();
	}
	
	/**
	 * Setting the num of coupons that be shown on single page.
	 * @param _new - new number.
	 */
	
	public static void setNumCouponsPerPage(int _new) {
		numCouponsPerPage = _new;
	}
	
	
	/**
	 * Retrieving the number of coupons that be shown in a single page.
	 * @return num coupons per page.
	 */
	
	public static int getNumCouponsPerPage(){
		return numCouponsPerPage;
	}
	
	/**
	 * if this is the first time the method called, allocating coupons instance and returns the instance;
	 * otherwise just returns the instance.
	 * @return Coupons instance.
	 */
	
	public static Coupons getInstance() {
		if(coupons == null)
			coupons = new Coupons();
		return coupons;
	}

	/**
	 * Retrieves the coupon that its id is the id from the parameter.
	 * @param id - the coupon with this id.
	 * @return Coupon associated with this id.
	 */
	
	@Override
	public Coupon getCoupon(int id) {
		session = factory.openSession();
		session.beginTransaction();
		Coupon c = (Coupon) session.get(Coupon.class, id);
		closeSession();
		return c;
	}
	
	/**
	 * Updates a coupon.
	 * @param coupon - the coupon details that will be updated.
	 * @return true if updated successfully; otherwise false.
	 */

	@Override
	public boolean updateCoupon(Coupon coupon) {
		session = factory.openSession();
		session.beginTransaction();
		try {
			// Updates coupon ..
			session.saveOrUpdate(coupon);
			closeSession();
		} catch(HibernateException ex) {
			System.err.println(ex.getMessage());
		}
		return true;
	}

	/**
	 * deleting a coupons that linked with the parameter id.
	 * @param id - the id to delete.
	 * @return true if deleted successfully; otherwise false.
	 */
	
	@Override
	public boolean deleteCoupon(int id) {
		session = factory.openSession();
		session.beginTransaction();
		Coupon c = (Coupon) session.get(Coupon.class, id);
		if(c == null) // tells that the coupon we want to delete is not exist. cannot do it ..
		{
			closeSession();
			return false;
		}
		
		try {
			session.delete(c);
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("deleteCoupon: " + ex.getMessage());
		}
		return true;
	}
	
	/**
	 * Adding a new coupon to the database.
	 * @param coupon - the coupon that will be saved in the database.
	 * @return true if saved successfully; otherwise false.
	 */

	@Override
	public boolean addCoupon(Coupon coupon) {
		session = factory.openSession();
		session.beginTransaction();
		Coupon c = (Coupon) session.get(Coupon.class, coupon.getId());
		if(c != null) // coupon already exist ..
		{
			closeSession();
			return false;
		}
		
		try {
			// saves coupon to the database.
			session.save(coupon);
			closeSession();
		} catch( HibernateException ex) {
			System.err.println("addCoupon: " + ex.getMessage());
		}
		return true;
	}

	/**
	 * Retrieve the last-inserted coupon's id.
	 * @return last-inserted coupons'd id.
	 */
	
	@Override
	public int getLastId() {
		session = factory.openSession();
		session.beginTransaction();
		Query max = session.createQuery("select max(id) from Coupon");
		// table null so the first one will be 1. (return zero because it's adds getLastId() +1 )
		if((Integer) max.list().get(0) == null) // if the list is null then no coupons.
			return 0; // insertion will user getLastId() + 1 , then return 0 in order that first coupon will be 1.
		int maxId = (Integer) max.list().get(0);
		closeSession();
		return maxId;
	}

	/**
	 * Retrieves a list of all the coupons in the database.
	 * @return all coupons in the database.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Coupon> getCoupons() {
		session = factory.openSession();
		session.beginTransaction();
		// Fetch coupons
		List<Coupon> c = (List<Coupon>) session.createQuery("from Coupon").list();
		closeSession();
		return c;
	}

	/**
	 * Retrieving  the given coupons page.
	 * @param pageNum - the number of page.
	 * @param isActive - whether include non-active coupons or not.
	 * @return List of `numCouponsPerPage` coupons respectively to the page number.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Coupon> getPage(int pageNum, boolean isActive) {
		session = factory.openSession();
		session.beginTransaction();
		Query query;
		if(isActive) { // if so, select only coupons that their expire date is late than today. will not filter with time, 
					  // it's not required anyway (it's used in the admin control panel, not in user panel)
			query = session.createQuery("from Coupon where (CURDATE() < fadeoutDate)");
		} else { // fetch all
			query = session.createQuery("from Coupon");
		}
		
		// starting result from (pageNum -1) *numPerPage to pageNum*numPerPage gives us the desired page!
		query.setFirstResult((pageNum - 1) * numCouponsPerPage);
		query.setMaxResults(numCouponsPerPage); // where to stop - right after a page has been filled.
		List<Coupon> page = (List<Coupon>) query.list();
		try {
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("getPage: " + ex.getMessage());
		}
		return page;
	}

	/**
	 * Retrieves how many pages there are.
	 * @return num of pages relatively to num coupons per page.
	 */
	
	@Override
	public long numPages() {
		session = factory.openSession();
		session.beginTransaction();
		// count num coupons
		long count = (long) session.createQuery("select count(*) from Coupon").uniqueResult();
		closeSession();
		// divide by numPerPage give num pages.
		return (long) (count / numCouponsPerPage) + ((count % numCouponsPerPage) != 0 ? 1 : 0); 
	}
	

//	/**
//	 * Retrieves all the active coupons.
//	 * @return List of all the active coupons.
//	 */
//	
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Coupon> getActiveCoupons() {
//		session = factory.openSession();
//		session.beginTransaction();
//		// Fetch all 
//		Query query = session.createQuery("from Coupon where (CURDATE() < fadeoutDate) ORDER BY id DESC");
//		List<Coupon> res = query.list();
//		closeSession();
//		return res;
//	}

	/**
	 * Retrieves all the coupons in a specific categoty.
	 * @param categoty - the category of coupons.
	 * @return all coupons in categoty "categoty".
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCouponsInCategory(String categoty) {
			session = factory.openSession();
			session.beginTransaction();
			// Fetch all active coupons whose businesses' parent's category is as got in the argument
			SQLQuery query =  session.createSQLQuery("SELECT * FROM COUPONS inner join Businesses on " +
													 "Coupons.business_id = Businesses.business_id WHERE Businesses.category = '" + categoty + "' " +
													 "AND ((CURDATE() < Coupons.fadeoutDate) OR ((CURDATE() = Coupons.fadeoutDate) " +
													 "AND (CURTIME() < CAST(Coupons.fadeoutHour as TIME))))");
			List<Object[]> coupons = query.list();
			closeSession();
			return coupons;
	}
	
	/**
	 * Retrieves all the active coupons from a specific category, 
	 * that their parent location's distance is not greater than "maxDistance" from user location!
	 * @param category - category of the coupons.
	 * @param maxDistance - show only coupons that the user can use them (i.e. the businesses to use them) is in range of maxDistance Miles.
	 * @param lat - user's latitude location coordinate.
	 * @param lng - user's longitude location coordinate. 
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getCouponsInCategoryWithMaxDistance(String category,
			int maxDistance, String lat, String lng) {
		session = factory.openSession();
		session.beginTransaction();
		// Fetch all the coupons, that their linked business location distance radius is < maxDistance.
		// NOTE: 3959 is the earth radius in miles.
		String query = "SELECT * FROM `Coupons` inner join (SELECT latitude, longitude, business_id, category, " +
					   "3959 * acos( " +
					   "cos(radians(" + lat + ")) " +
					   "*cos(radians(`latitude`)) " +
					   "*cos(radians(`longitude`) - radians(" + lng + ")) " +
					   "+sin(radians(" + lat + ")) " +
					   "*sin(radians(`latitude`))) as distance FROM `Businesses`) " +
					   "`Businesses` on (Coupons.business_id = Businesses.business_id) HAVING distance < " + maxDistance + " AND (businesses.category = '" + category + "') AND " +
					   "((CURDATE() < Coupons.fadeoutDate) OR ((CURDATE() = Coupons.fadeoutDate) AND (CURTIME() < CAST(Coupons.fadeoutHour as TIME)))) ORDER BY distance";
		SQLQuery q =  session.createSQLQuery(query);
		List<Object[]> c = q.list();
		closeSession();
		return c;
			
	}

	/**
	 * Committing the transaction and closes the session.
	 */
	
	private void closeSession() {
		try {
			if(!session.getTransaction().wasCommitted())
				session.getTransaction().commit();
		} catch(HibernateException ex) {
			System.err.println("closeSession: " + ex.getMessage());
		}
		if(session.isOpen())
			session.close();
	}

}
