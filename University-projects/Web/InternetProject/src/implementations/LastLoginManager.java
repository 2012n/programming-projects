package implementations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.ILastLoginDAO;
import entities.LastLogin;

public class LastLoginManager implements ILastLoginDAO{
	
	private static SessionFactory factory; // session factory instance reference.
	private static LastLoginManager lastlogin = null; // self object for singleton class.
	private Session session; // session
	
	/**
	 * Default constractor. setting up the factory reference.
	 */
	
	private LastLoginManager() {
		factory = Controller.getSessionFactroy();
	}
	
	/**
	 * If this is the first time this method called, it'll allocate lastLogin and returns instance; otherwise returns instance.
	 * @return LastLoginManager instance.
	 */
	public static LastLoginManager getInstance() {
		if(lastlogin == null)
			lastlogin = new LastLoginManager();
		return lastlogin;
	}
	
	/**
	 * Retrieves the last login that logged in the database.
	 * @return last login info.
	 */
	
	@Override
	public LastLogin getLastLogin() {
		session = factory.openSession();
		session.beginTransaction();
		LastLogin l = (LastLogin) session.get(LastLogin.class, 0);
		closeSession();
		return l;
	}

	/**
	 * Updates the last login information.
	 * @param LastLogin object to update the current with;
	 * @return true if successfully updated; otherwise false.
	 */
	
	@Override
	public boolean updateLastLogin(LastLogin last) {
		session = factory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(last);
		closeSession();
		return true;
	}
	
	/**
	 * Commiting transaction and closes session.
	 */
	
	private void closeSession() {
		if(!session.getTransaction().wasCommitted())
			session.getTransaction().commit();
		if(session.isOpen())
			session.close();
	}
}
