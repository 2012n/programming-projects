package implementations;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import dao.IUserDao;
import entities.User;


public class Users implements IUserDao {
	private static SessionFactory factory; // session factory instance reference
	private static Users users = null; // self object instance for the singleton method.
	private Session session; // session.

	/**
	 * Default constructor. setting up the factory reference.
	 */
	
	private Users() {
		factory = Controller.getSessionFactroy();
	}

	/**
	 * If this is the first time this method called, it'll allocate users and return instance; otherwise return instance.
	 * @return users instance
	 */
	
	public static Users getInstance() {
		if(users == null)
			users = new Users();
		return users;
	}

	/**
	 * Retrieve the user that it's user_id is id..
	 * @param id - the user id.
	 * @return the user with `id` id.
	 */
	
	@Override
	public User getUser(int id) {
		session = factory.openSession();
		session.beginTransaction();
		User u = (User) session.get(User.class, id);
		closeSession();
		return u;
	}

	
	/**
	 * updates a user.
	 * @param user - the user to update.
	 * @return true if updated successfully; otherwise false.
	 */
	
	@Override
	public boolean updateUser(User user) {
		session = factory.openSession();
		session.beginTransaction();
		// Encrypts the user password using MD5 hash.
		user.setPassword(MD5(user.getPassword()));
		try {
			session.update(user);
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("updateUser: " + ex.getMessage());
		}
		return true;
	}

	
	/**
	 * Deletes a user linked with `id` id.
	 * @param id - the used linked with this Id to be deleted.
	 * @return true if deleted successfully; otherwise false.
	 */
	
	@Override
	public boolean deleteUser(int id) {
		session = factory.openSession();
		session.beginTransaction();
		User u = (User) session.get(User.class, id);
		if(u == null) { // Error: No such user - nothing to delete!
			closeSession();
			return false;
		}
		
		session.delete(u);
		
		try {
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("deleteUser: " + ex.getMessage());
		}
		return true;
	}

	/**
	 * Adds a new user to the database.
	 * @param user - the user to be added.
	 * @return true is added successfully; otherwise false.
	 */
	
	@Override
	public boolean addUser(User user) {
		session = factory.openSession();
		session.beginTransaction();
		User u = (User) session.get(User.class, user.getUser_id());
		if(u != null) { // already exist .. 
			closeSession();
			return false;
		}
		
		// Encrypt user password using MD5 hash.
		user.setPassword(MD5(user.getPassword()));
		session.save(user);
		try {
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("addUser: " + ex.getMessage());
		}
		return true;
	}

	/**
	 * Retrieved the last-insered user's id.
	 * @return last-inserted user's id or 0 if table empty.
	 */
	
	@Override
	public int getLastId() {
		session = factory.openSession();
		session.beginTransaction();
		List<Integer> lastId = (List<Integer>) session.createQuery("select max(id) from User").list();
		if(lastId.get(0) == null) // no users, then the first id will be 0 + 1 (1)
			return 0;
		closeSession();
		return lastId.get(0);
	}

	/**
	 * Retrieves a list of users.
	 * @return List of all the users in the database.
	 */
	
	@Override
	public List<User> getUsers() {
		session = factory.openSession();
		session.beginTransaction();
		List<User> u = (List<User>) session.createQuery("from User").list();
		closeSession();
		return u;
	}
	
	/**
	 * Commiting transaction and closes session.
	 */
	
	private void closeSession() {
		if(!session.getTransaction().wasCommitted())
			session.getTransaction().commit();
		if(session.isOpen())	
			session.close();
	}

	/**
	 * Authenticating login - looking for match between pair of username and password.
	 * @return true if username matchs password; otherwise false.
	 */
	
	@Override
	public boolean authenticateLogin(String username, String password) {
		session = factory.openSession();
		session.beginTransaction();
		// Try to log in
		if(password == "") // Password cannot be empty
			return false;
		User i = (User) session.createQuery("from User where username='" + username + "' and password='" + MD5(password) + "'").uniqueResult();
		closeSession();
		return i != null;
	}

	/**
	 * Encrypting given input to MD5. using for encrypting user password.
	 * @param md5 - string to encrypt.
	 * @return MD5-encrypted string.
	 */
	
	public String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}
