package implementations;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import entities.Business;
import entities.Coupon;
import entities.LastLogin;
import entities.User;

/**
 * Servlet implementation class WebController
 */
@WebServlet("/Web")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;       
	private static SessionFactory sessionFactory = null;
	// Set managers
	Coupons couponManager = Coupons.getInstance();
	Businesses businessManager = Businesses.getInstance();
	Users adminsManager = Users.getInstance();
	LastLoginManager lastLoginManager = LastLoginManager.getInstance();
	
	/**
	 * if this is the first time called, create a new SessionFactory and return it. otherwise return instance.
	 * @return instance of SessionFactory.
	 */
	public static SessionFactory getSessionFactroy() {
		if(sessionFactory == null) 
			sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		return sessionFactory;
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRervletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String dispatchTo = request.getPathInfo();
		// treat the request by requested URL.
		switch (dispatchTo) {
		case "/login":
			treatLogin(request, response);
			break;
		case "/dologin":
			treatDoLogin(request, response);
			break;
		case "/install":
			treatInstall(request, response);
			break;
		case "/index":
		case "/":
			treatIndex(request, response);
			break;
		case "/treatindexlist":
			treatIndexList(request, response);
			break;
		case "/yourcoupons":
			treatYourCoupons(request, response);
			break;
		case "/admincp":
			treatAdminCP(request, response);
			break;
		case "/logout":
			treatLogout(request, response);
			break;
		case "/addnewcoupon":
			treatAddNewCoupon(request, response);
			break;
		case "/delete":
			treatDeletion(request, response);
			break;
		case "/addadmin":
			treatAddAdmin(request, response);
			break;
		case "/loadPage":
			treatLoadPage(request, response);
			break;
		case "/update":
			treatUpdate(request, response);
			break;
		case "/addbusiness":
			treatAddNewBusiness(request, response);
			break;
		/****************** TREAT ANDROID APPLICATION REQUESTS **********/
		case "/home":
			request.getRequestDispatcher("home.jsp").forward(request, response);
			break;
		case "/categoty":
			treatLoadUserCoupons(request, response);
			break;
		case "/loadCouponsToClient":
			treatLoadCouponsToClient(request, response);
			break;
		case "/info":
			treatCouponInfo(request, response);
			break;
		default: // the given path not definied.
			out.println("404: path \'" + dispatchTo + "\' has not been definied.");
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response); // post doesn't need a special treatment.
	}
	
	/**
	 * Loads the install view.
	 * @param request - request.
	 * @param response - response.
	 */
	
	private void treatInstall(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.getRequestDispatcher("install.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Loads the login view.
	 * @param request - request.
	 * @param response - response.
	 */
	protected void treatLogin(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.getRequestDispatcher("login.jsp").forward(request, response); // if login given, redirect to the view.
		} catch(IOException | ServletException ex) {
			System.err.println(ex.getMessage());
		}
	}
	

	/**
	 * Authenticating the login request using the Users.AuthenticateLogin method.
	 * If authenticating successed, it will return "OK" to the user; otherwise a error message.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatDoLogin(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		String user = request.getParameter("username").trim(), 
			   pass = request.getParameter("password").trim();
		
		// Make sure everything passed..
		if(user == null || pass == null) {
			out.println("Fill both username and password!");
			return;
		}
		
		// Authenticate the log in
		if(adminsManager.authenticateLogin(user, pass)) {
			// Login successed! Log this login ..
			String uagent = request.getHeader("User-Agent"), 
				   browser = "";
			
			// Determine which browser used to log in
			if(uagent.indexOf("Firefox") != -1)
				browser = "Firefox";
			else if(uagent.indexOf("MSIE") != -1) 
				browser = "Internet Explorer";
			else if(uagent.indexOf("Chrome") != -1)
				browser = "Google Chrome";
			else if(uagent.indexOf("Opera") != -1)
				browser = "Opera";
			else if(uagent.indexOf("Safari") != -1)
				browser = "Safari";
			else
				browser = "unknown";
			
			
			LastLogin thisLogin = new LastLogin(0, user, browser); // log this login as the last.
			lastLoginManager.updateLastLogin(thisLogin); // save last login
			
			// login success, so create an Admin cookie
			Cookie admin = new Cookie("admin", user);
			admin.setPath("/");
			admin.setMaxAge(60*60*24); // 60 seconds * 60 is an hour, * 24 is 24 hours => day.
			response.addCookie(admin);
			
			// Send "OK" to the client to proceed to adminCP.
			out.print("OK");
		} else // Authenticate failed. Username or\and password incorrect.
			out.println("Sorry, but username or password incorrect.");
	}
	
	/**
	 * setting up the home (index) page by sending the view nessacery info from the database to render.
	 * @param request - request object.
	 * @param response - response object.
	 */
	protected void treatIndex(HttpServletRequest request, HttpServletResponse response) {
		String cat = request.getParameter("cat");
		if(cat == null) { // No category has been given, show cat list (the view will do it)
			try {
				request.getRequestDispatcher("index.jsp").forward(request, response);
				return;
			} catch (ServletException | IOException ex) {
				System.err.println(ex.getMessage());
			}
		}
		
		// List all the coupons in category `cat`
		List<Object[]> c = couponManager.getCouponsInCategory(cat);
		// Set up active coupons
		request.setAttribute("coupons", c); // sending the coupons that the view will use
		try {
			// forward the request..
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			System.err.println(e.getMessage());
		} 
	}
	
	/**
	 * Adds or Remove favorite user coupons.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatIndexList(HttpServletRequest request, HttpServletResponse response) {
		String action = request.getParameter("action"),
			   id = request.getParameter("id");
		// Getting parameters: action - whether add the coupon or delete.
		// 					   ID: the coupon's id to act with.
		
		// Make sure everything sent.
		if(id == null || action == null) return;
		
		if(action.equals("remove")) {
			// Remove this ID from the session attributes.
			request.getSession().removeAttribute(id);
		} else { // remove not specified, so add.
			// adding the coupon id.
			request.getSession().setAttribute(id, id);
		}
	}
	
	/**
	 * List the user selected coupons.
	 * @param request - request object
	 * @param response - response object.
	 */
	
	protected void treatYourCoupons(HttpServletRequest request, HttpServletResponse response) {
		
		Enumeration<String> sessionStorage = request.getSession().getAttributeNames();
		// Getting all the selected coupons' ids.
		ArrayList<Coupon> coups = new ArrayList<>();
		int id;
		while(sessionStorage.hasMoreElements()) {
			try { // Try to understand whether it's a coupon ID or not.
				id = Integer.parseInt(sessionStorage.nextElement());
			} catch(NumberFormatException ex) {
				// Not a coupon id
				continue;
			}
			// It is a coupon ID, add this coupon to the list.
			coups.add(couponManager.getCoupon(id));
		}
		
		// send selected coupon to the `yourcoupons.jsp` view.
		request.setAttribute("chosenCoupons", coups);
		
		try {
			// load view ..
			request.getRequestDispatcher("yourcoupons.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			System.err.println(e.getMessage());
		}
		
	}
	
	
	/**
	 * Validate whether it's admin session or not. if not, redirecting to login page.
	 * otherwise, Sets up the admin pc and moves data to the view.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatAdminCP(HttpServletRequest request, HttpServletResponse response) {
		// verify a logged-on administrator before access admincp
		Cookie[] cookie = request.getCookies();
		if(cookie == null) { // no cookies at all - no admin cookie.
			try {
				// Sending to the login view in order to login ..
				response.sendRedirect("login");
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			return;
		}
		
		String adminName = "";
		boolean isAdmin = false;
		for(Cookie c : cookie) {
			if(c.getName().equals("admin")) { // an admin cookie found => it's an admin session
				isAdmin = true;
				adminName = c.getValue();
				break;
			}
		}
		
		if(!isAdmin) { // no admin cookie found
			try {
				// Sending to the login view in order to login ..
				response.sendRedirect("login");
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
			return;
		}
		
		// **** prepare admin cp content ******
		
		// last login information
		LastLogin lli = lastLoginManager.getLastLogin();
		// Send data to the view:
		request.setAttribute("adminName", adminName);
		// send last login user & last login browser
		request.setAttribute("LastLoginUsername", lli == null ? "Never" : lli.getUsername());
		request.setAttribute("LastLoginBrowser", lli == null ? "Never" : lli.getBrowser());
		
		// num coupon pages
		request.setAttribute("numCouponsPerPage", Coupons.getNumCouponsPerPage());
		request.setAttribute("numCouponsPages", couponManager.numPages());
		request.setAttribute("firstCouponPage", couponManager.getPage(1, false));
		// num business pages
		request.setAttribute("numBusinessesPerPage", Businesses.getNumBusinessesPerPage());
		request.setAttribute("numBusinessesPages", businessManager.numPages());
		request.setAttribute("firstBusinessPage", businessManager.getPage(1));
		// Admins
		request.setAttribute("adminList", adminsManager.getUsers());
		// Dispatch to view
		try {
			// Load adminCP view.
			request.getRequestDispatcher("admincp.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			System.err.println(e.getMessage());
		}
	}

	
	/**
	 * Logging the admin out - removing the admin cookie.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatLogout(HttpServletRequest request, HttpServletResponse response) {
		Cookie deleteAdminCookie = new Cookie("admin", null);
		deleteAdminCookie.setMaxAge(0); // override the cookie death time to immediately die
		deleteAdminCookie.setPath("/");
		response.addCookie(deleteAdminCookie); 
		try {
			// not recognized as admin anymore - move to index.
			response.sendRedirect("index");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} 
	}
	
	/**
	 * Adds a new coupon to the database.
	 * @param request - request object
	 * @param response - response object.
	 */
		
	protected void treatAddNewCoupon(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		String business_id = request.getParameter("bid"),
			   image = request.getParameter("image"),
			   details = request.getParameter("details"),
			   date = request.getParameter("date"),
			   time = request.getParameter("time");
		
		// Make sure everything's here.
		if (business_id == null || image == null || details == null || date == null
				|| time == null) {
			// At least one of the arguments has not been sent.
			out.print("Please specify all arguments!");
			return;
		}

		// Validate input
		
		// 1. check whether business id field is really an Integer
		int id;
		try {
			id = Integer.parseInt(business_id);
		} catch(NumberFormatException ex) { // an invalid ID given!
			out.println("Can\'t parse business_id \"" + business_id  + " \" as an integer.");
			return;
		}
		
		// 2. check whether a details provided or not.
		if(details.equals("")) {
			out.println("You should provide details about this coupon.");
			return;
		}
		
		// 3. Validate the date format.
		if(!date.matches("\\d{4}-\\d{2}-\\d{2}")) {
			out.println("date \"" + date + "\" not representing a date in the format of `yyyy-mm-dd`.");
			return;
		} else { // format is YYYY-MM-DD for sure
			String[] _date = date.split("-"); // Separate days , months and year
			int _day = 0, _month = 0;
			try { 
				_month = Integer.parseInt(_date[1]);
				_day = Integer.parseInt(_date[2]);
			} catch(NumberFormatException ex) {
				out.println("Failed to cast " + _date[1] + " or " + _date[2] + " to an integer");
				return;
			}
			
			// Have month and day as integer
			if(!(_day >= 1 && _day <= 31)) {
				out.println("Out of range day. can be 1 to 31.");
				return;
			} if(!(_month >= 1 && _month <= 12)) {
				out.println("out of range month. can be 1 to 12");
				return;
			}
		}
		
		// 4. Validate the time format
		if(!time.matches("\\d{2}:\\d{2}")) {
			out.println("time \"" + time + "\" not representing a time in the format of `hh:mm`.");
			return;
		} else { // time is hh:mm for sure
			int _hour = 0, _minutes = 0;
			String[] _time = time.split(":");
			try { 
				_hour = Integer.parseInt(_time[0]);
				_minutes  = Integer.parseInt(_time[1]);
			} catch(NumberFormatException ex) {
				out.println("Failed to cast " + _hour + " or " + _time + " to an integer.");
				return;
			} 
			
			// Have hour and minute
			if(!(_hour >= 0 && _hour <= 59)) {
				out.println("Hour should be in the range of 0 to 59");
				return;
			}
			
			if(!(_minutes >= 0 && _minutes <= 59)) {
				out.println("Minutes should be in the range of 0 or 59");
				return;
			}
		}
		
		// Validate that the business is exist
		if(businessManager.getBusiness(id) == null) {
			out.println("You are trying to add a coupon to business #" + id + ", but there\'s no such business.");
			return;
		}
		
		// All checks passed, can insert the coupon.
		int insertId = couponManager.getLastId() + 1;
		Coupon c = new Coupon(insertId, id, image, details, date, time);
		if(couponManager.addCoupon(c))
			out.print("OK"); // Let the client know everything has went right.
		else // For some reason could not add this coupon ..
			out.println("Problem during adding this coupon.");
	}
	
	/**
	 *  Perform a deletion of a coupon \ business \ admin.
	 * @param request - request object
	 * @param response - response object
	 */
	
	protected void treatDeletion(HttpServletRequest request, HttpServletResponse response) {
		String pid = request.getParameter("id"),
			   from = request.getParameter("from");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// make sure everything received.
		if(pid == null || from == null) {
			out.println("Can\'t perform deletion due to missing arguments.");
			return;
		}

		int id;
		try {
			id = Integer.parseInt(pid);
		} catch(NumberFormatException ex) { // an invalid id has given!
			out.println("Can\'t perform deletion because " + pid + " is uncastable to integer.");
			return;
		}
		
		// Check whether delete a coupon or business or admin (case-insensitive)
		
		switch (from.toLowerCase()) {
		case "coupons": // should delete a coupon
			if(couponManager.deleteCoupon(id))
				out.print("OK");
			else // Problem during delete this coupon ..
				out.println("Can\'t delete coupon #" + id + ": something went wrong.");
			break;
			
		case "businesses": // Should delete a business
			if(businessManager.deleteBusiness(id)) 
				out.print("OK");
			else // Problem during delete this business ..
				out.println("Can\'t delete business #" + id + ": something went wrong.");
			break;
			
		case "admins":// should delete an admin
			if(adminsManager.deleteUser(id))
				out.print("OK");
			else // Problem during delete this admin .. 
				out.println("Can\'t delete admin #" + id + ": something went wrong.");
			break;
		}
	}
	
	/**
	 * Adding a new admin.
	 * @param request - request object
	 * @param response - response object.
	 */
	
	protected void treatAddAdmin(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("username"),
			   password = request.getParameter("password");
		
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		// Make sure everything received.
		if(name == null || password == null) {
			out.print("Username or password arguments are missing. Can\'t proceed.");
			return;
		}
	
		// Trim username
		name = name.trim();
		
		if(name.equals("")) {
			out.println("Administrator username cannot be empty!");
			return;
		} else if(password.equals("")) {
			out.println("Password cannot be empty!");
			return;
		}
		
		// get last id.
		int newId = adminsManager.getLastId() + 1;
		
		// Add the admin.
		if(adminsManager.addUser(new User(newId, name, password)))
			out.print("OK");
		else // an error occurred.
			out.println("Can\'t add this administrator, try to change the username.");
	}
	
	/**
	 * Loads a coupon or business page to the client.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatLoadPage(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		String whatToLoad = request.getParameter("whatToLoad"),
			   pageNum = request.getParameter("pageID"),
			   active = request.getParameter("active");
		
		// whatToLoad - should be either coupons or businesses.
		// pageNum - which page to load?
		// active (coupons only) - whether to load only active coupons or not.
		
		// make sure everything's received.
		if(whatToLoad == null || pageNum == null || active == null) {
			out.println("There\'s at least one missing arguments. Can\'t proceed.");
			return;
		}
		
		int thePage; // the page to load.
		
		try {
			thePage = Integer.parseInt(pageNum);
		} catch(NumberFormatException ex) { // argument value not represent a integer.
			out.println("Given page id arguments: " + pageNum + ", is uncastable to Integer.");
			return;
		}
		
		// Choose whether to load coupon page or business page.
		switch (whatToLoad.toLowerCase()) {
		case "coupons":
			List<Coupon> c_page = couponManager.getPage(thePage, active.equalsIgnoreCase("true"));
			for(Coupon c : c_page) { 
				// Write info to load in the view.
				out.println("<tr>" +
								"<td>" + c.getId() + "</td>" +
								"<td><img class=\"thumbnail\" src=\"" + (c.getImage().equals("") ? "../img/couponimg.png" : c.getImage()) + "\" /></td>" +
								"<td>" + c.getBusiness_id() + "</td>" + 
								"<td>" + c.getDetails() + "</td>" + 
								"<td>" + c.getFadeoutDate() + "</td>" + 
								"<td>" + c.getFadeoutHour() + "</td>" +
								"<td><i class=\"icon-pencil editcoupon\" mode=\"edit\"></i></td>" + 
								"<td><i class=\"icon-remove\"></i></td>" +
							"</tr>");
			}
			break;
		//	Load business page.
		case "businesses":
			List<Business> b_page = businessManager.getPage(thePage);
			for(Business b : b_page) {
				out.println("<tr>" +
								"<td>" + b.getBusiness_id() + "</td>" +
								"<td><img class=\"thumbnail\" src=\"" + (b.getImage().equals("") ? "../img/businessimg.png" : b.getImage())+ "\" /></td>" +
								"<td>" + b.getName() + "</td>" +
								"<td>" + b.getDetails() + "</td>" + 
								"<td>" + b.getCategory() + "</td>" + 
								"<td><a href=\"#showMap\" data-toggle=\"modal\" onclick=\"showBusinessMap('" + b.getLatitude() + "' , '" + b.getLongitude() + "')\"><button type=\"button\" class=\"btn btn-lg btn-block\">Show on map</button></a></td>" +
								"<td><i class=\"icon-pencil businessedit\" latitude=\"" + b.getLatitude() + "\" longitude=\"" + b.getLongitude() + "\" case=\"business\" mode=\"edit\"></i></td>" + 
								"<td><i class=\"icon-remove\"></i></td>" +
							"</tr>");
			}
			break;
		}
	}

	/**
	 * Update a coupon or admin or business.
	 * @param request - request object.
	 * @param response - response object.
	 */
	
	protected void treatUpdate(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		// Get arguments array
		String[] arguments = request.getParameterValues("params[]");
		
		if(arguments == null) {
			out.println("No arguments has been recieved.");
			return;
		}
		
		String table = arguments[0];
		// Table - whether act on coupons , businesses or admins
		
		switch (table.toLowerCase()) {

		/*******************************\
		|***** Update a coupon **********|
		\*******************************/
		
		case "coupons":
			if(arguments.length != 7) { // necessary arguments: {table, id, img, business_id, details, outdate, outtime}
				out.println("Detected only " + arguments.length + " arguments.\n" +
						    "Pass {table, id, img, business_id, details, fadeoutdate, fadeouttme} please!");
				return;
			}
			
			
			String details = arguments[4].trim(),
				   date = arguments[5].trim(),
				   time = arguments[6].trim(),
				   image = arguments[2].trim();
			// Validate input
			
			// 1. check whether business id field is really an Integer
			
			int id, business_id;
			
			try {
				id = Integer.parseInt(arguments[1]);
				business_id = Integer.parseInt(arguments[3].trim());
			} catch (NumberFormatException ex) { // failed to cast id or business_id to Integer.
				out.println("ID or Business_id are uncastable to Ineteger. recheck them.");
				return;
			}
			
			
			// 2. check whether a details provided or not.
			if(details.equals("")) {
				out.println("You should provide details about this coupon.");
				return;
			}
			
			// 3. Validate the date format.
			if(!date.matches("\\d{4}-\\d{2}-\\d{2}")) {
				out.println("date \"" + date + "\" not representing a date in the format of `yyyy-mm-dd`. Update not performed.");
				return;
			} else { // format is YYYY-MM-DD for sure
				String[] _date = date.split("-"); // Separate days , months and year
				int _day = 0, _month = 0;
				try { 
					_month = Integer.parseInt(_date[1]);
					_day = Integer.parseInt(_date[2]);
				} catch(NumberFormatException ex) {
					out.println("Failed to cast " + _date[1] + " or " + _date[2] + " to an integer");
					return;
				}
				
				// Have month and day as integer
				if(!(_day >= 1 && _day <= 31)) {
					out.println("Out of range day. can be 1 to 31.");
					return;
				} if(!(_month >= 1 && _month <= 12)) {
					out.println("out of range month. can be 1 to 12");
					return;
				}
			}
			
			// 4. Validate the time format
			if(!time.matches("\\d{2}:\\d{2}")) {
				out.println("time \"" + time + "\" not representing a time in the format of `hh:mm`. Update not performed.");
				return;
			} else { // time is hh:mm for sure
				int _hour = 0, _minutes = 0;
				String[] _time = time.split(":");
				try { 
					_hour = Integer.parseInt(_time[0]);
					_minutes  = Integer.parseInt(_time[1]);
				} catch(NumberFormatException ex) {
					out.println("Failed to cast " + _hour + " or " + _time + " to an integer.");
					return;
				} 
				
				// Have hour and minute
				if(!(_hour >= 0 && _hour <= 59)) {
					out.println("Hour should be in the range of 0 to 59");
					return;
				}
				
				if(!(_minutes >= 0 && _minutes <= 59)) {
					out.println("Minutes should be in the range of 0 or 59");
					return;
				}
			}
			
			// Validate that the business is exist
			if(businessManager.getBusiness(business_id) == null) {
				out.println("You are trying to add a coupon to business #" + business_id + ", but there\'s no such business.");
				return;
			}
			
			// No found invalid info, so can set up the coupon:
			Coupon c = new Coupon(id, business_id, image, details, date, time);
			if(couponManager.updateCoupon(c))
				out.print("OK"); // updated successfully , let the web requester know.
			else
				out.println("For some reason, coupon #" + id + " cannot be updated.");
			break;
		
		/*******************************\
		|****** Update a admin **********|
		\*******************************/
			
		case "admins":
			if(arguments.length != 4) { // necessary arguments: {table, id, username, password}
				out.println("Detected only " + arguments.length + " arguments.\n" +
							"Pass {table, id, username, password}.");
			}
			
			int user_id;
			try {
				user_id = Integer.parseInt(arguments[1]);
			} catch (NumberFormatException ex) { // Failed to cast user_id to Ineteger.
				out.println(arguments[1] + " cannot be casted to an Integer. fix it.");
				return;
			}
			
			String username = arguments[2].trim(), 
				   password = arguments[3].trim();
			
			if(username.equals("") || password.equals("")) {
				out.println("Error updating this admin: username and\\or password are empty.");
				return;
			}
			// Update the administrator
			User admin = new User(user_id, username, password);
			if(adminsManager.updateUser(admin))
				out.print("OK");
			else
				out.println("For some reason, admin #" + user_id + " cannot be updated.");
		
		/*******************************\
		|****** Update a business *******|
		\*******************************/
		
		case "business":
			if(arguments.length != 8) { // {table, id, name, details, category, lat, lng}
				out.println("Can\'t update business due to missing arguments. only " + arguments.length + " arguments given!");	
				return;
			}
			
			int b_id;
			try {
				b_id = Integer.parseInt(arguments[1]);
			} catch (NumberFormatException ex) { // failed to cast the business_id to Integer.
				out.println(arguments[1] + " is uncastable to an Integer and for that reason it can\'t represent a business ID.");
				return;
			}
			
			String bname = arguments[3].trim(),
				   business_details = arguments[4].trim(),
				   category = arguments[5].trim(),
				   business_image = arguments[2].trim(),
				   latitude = arguments[6].trim(),
				   longitude = arguments[7].trim();
			
			// validate input
			if(bname.equals("")) {
				out.println("You should provide business name.");
				return;
			} 
			
			if(business_details.equals("")) {
				out.println("You should provide some details about this business.");
				return;
			}
			
			if(category.equals("")) {
				out.println("You should provide the category of this business ..");
				return;
			}
			
			// set up the business.
			Business b = new Business(b_id, business_details, business_image, bname, latitude, longitude, category);
			if(businessManager.updateBusiness(b)) 
				out.print("OK");
			else
				out.println("Business #" + b_id + " cannot be updated.");
		default:
			break;
		}
	}
	
	/**
	 * Adds new business.
	 * @param request - request object
	 * @param response - response object.
	 */
	
	protected void treatAddNewBusiness(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		String bname = request.getParameter("bname"),
			   image = request.getParameter("image"),
			   details = request.getParameter("details"),
			   latitude = request.getParameter("latitude"),
			   longitude = request.getParameter("longitude"),
			   categoty = request.getParameter("category");
		// Make sure all the arguments received.
		if(bname == null || image == null || details == null || latitude == null || longitude == null || categoty == null) {
			out.println("Can\'t add new business duo to missing arguments.");
			return;
		}
	
		// validate input
		if(bname.equals("")) {
			out.println("You should provide business name.");
			return;
		} 
		
		if(details.equals("")) {
			out.println("You should provide some details about this business.");
			return;
		}
		
		if(categoty.equals("")) {
			out.println("You should provide the category of this business ..");
			return;
		}
		
		// OK, we've got valid input .. just add
		if(businessManager.addBusiness(new Business(businessManager.getLastId() + 1, details, image, bname, latitude, longitude, categoty)))
			out.print("OK");
		else
			out.println("For unknown reason, can\'t add this business to the database. Try again.");
	}
	
	/**
	 * Loads user coupons from a given category.
	 * @param request - request object.
	 * @param response - response obejct.
	 */
	
	protected void treatLoadUserCoupons(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		// The category of coupons
		String cat = request.getParameter("cat");
		
		if(cat == null) {
			out.println("You should specify a category");
			return;
		}

		// The valid categories.
		String[] available = {"food", "toys", "entertainment", "smartphones", "electronics"};
		
		boolean found = false;
		for(String s : available) { // decide whether the argument valid
			if(s.equalsIgnoreCase(cat)) {
				found = true;
				break;
			}
		}
		
		if(!found) { // no such category
			out.println("You should specify a valid category.");
			return;
		}
		
		// show the coupons
		List<Object[]> coupons = couponManager.getCouponsInCategory(cat);
		// Pass the coupons to the view
		request.setAttribute("cuponsByBusiness", coupons);
		try {
		request.getRequestDispatcher("categoty.jsp").forward(request, response);
		} catch(IOException | ServletException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Show coupon information to Android application
	 * @param request - request object
	 * @param response - response object.
	 */
	
	protected void treatCouponInfo(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		String cid = request.getParameter("cid");
		
		if(cid == null) {
			out.println("No coupon id has been received.");
			return;
		} 
		
		int id;
		try {
			id = Integer.parseInt(cid);
		} catch(NumberFormatException ex) {
			out.println("Cannot convert " + cid + " to integer.");
			return;
		}
		
		// Get coupon
		Coupon c = couponManager.getCoupon(id);
		// move this coupn forward
		request.setAttribute("getcouponinfo", c);
		
		try {
			request.getRequestDispatcher("info.jsp").forward(request, response);
		} catch (ServletException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads coupon to client with maximux distance radius from user location with specific category.
	 * @param request - request object
	 * @param response - response object
	 */
	
	protected void treatLoadCouponsToClient(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		// Loads a coupon to the android client, a coupons in distance radius < `maxDistance`
		String cat = request.getParameter("cat"),
			   lat = request.getParameter("lat"),
			   lng = request.getParameter("lng"),
			   maxDistance = request.getParameter("maxDistance");
		
		int max;
		try {
			max = Integer.parseInt(maxDistance);
		} catch(NumberFormatException ex) {
			out.println("Cant use " + maxDistance + " as integer.");
			return;
		}
		
		// Load the available coupons to the android client
		List<Object[]> appropriateCoupons = couponManager.getCouponsInCategoryWithMaxDistance(cat, max, lat, lng);
		
		// write header
		out.println("<thead style='background-color: #DDD; font-weight: bold;'>" +
					"	<td>Img</td>" +
					"	<td>details</td>" +
					"	<td>location</td>" +
					"	<td>Distance from you</td>" +
					"	<td>more info</td>" +
					"</thead>");
		
		
		// write columns
		
		// No coupons available under these terms
		if(appropriateCoupons == null) {
			out.println("<tr>" +
							"<td colspan='7'>No coupons for this search condition</td>" +
						"</td>");
			return;
		}
		
		for(Object[] row : appropriateCoupons) {
			// need img, details, ends , location and more info btn
			String img = row[5] + "",
				   details = row[2] + "",
				   innerFunc = "setupMap('" + row[6] + "', '" + row[7] + "')",
				   location = "<a onclick=\"" + innerFunc + "\" href='#showMap' data-inline='true' data-mini='true' data-role='button' data-rel='popup' data-position-to='window' data-transition='pop'>Show on map</a>",
				   distance = "Approx. " + Math.round(Double.parseDouble(row[10] + "") * 10.0) / 10.0 + " Mils away", // Round up to 1 decimal digit
				   moreInfo =  "<a href='info?cid=" + row[0] + "' data-role='button'  data-ajax='false' data-inline='true' data-mini='true'>more info</a>";
			
			// write coupon details formatted in a table-row
			out.println("<tr>" +
							"<td><img src=\'" + img + "\' alt=\'\' style=\'height: 20px; height: 20px;\' /></td>" +
							"<td>" + details + "</td>" + 
							"<td>" + location + "</td>" +
							"<td>" + distance + "</td>" +
							"<td>" + moreInfo + "</td>" +
						"</tr>");
		}		
	}
}
