package implementations;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.IBusinessDao;
import entities.Business;


public class Businesses implements IBusinessDao {
	private static int numBusinessesPerPage = 5; // Determines how many businesses will be shown in a single page.
	private static SessionFactory factory; // session factory instance reference.
	private static Businesses businesses = null; // self object for singelton class.
	private Session session; // session object.

	/**
	 * Default constructor. setting up the factory reference.
	 */
	
	private Businesses() {
		factory = Controller.getSessionFactroy();
	}

	/**
	 * If this is the first time the method called, allocating businesses and returns instance; 
	 * otherwise returns the instance.
	 * @return Businesses instance.
	 */
	
	public static Businesses getInstance() {
		if(businesses == null)
			businesses = new Businesses();
		return businesses;
	}
	
	/**
	 * Retrieves the number of businesses that will be shown in a single page.
	 * @return number of businesses in a page.
	 */
	
	public static int getNumBusinessesPerPage() {
		return numBusinessesPerPage;
	}

	/**
	 * Sets how many businesses will be shown in a single page.
	 * @param numBusinessesPerPage - new num per page.
	 */
	
	public static void setNumBusinessesPerPage(int numBusinessesPerPage) {
		Businesses.numBusinessesPerPage = numBusinessesPerPage;
	}

	/**
	 * return the businesses with id -> id.
	 * @param id - the business id.
	 * @return the business ID that linked with given id.
	 */
	
	@Override
	public Business getBusiness(int id) {
		session = factory.openSession();
		session.beginTransaction();
		// Fetch the business with the given id
		Business b = (Business) session.get(Business.class, id);
		closeSession();
		return b;
	}

	/**
	 * updates a business
	 * @param Business - the business to update
	 * @return true if updated successfully; otherwise false.
	 */
	
	@Override
	public boolean updateBusiness(Business business) {
		session = factory.openSession();
		session.beginTransaction();
		try {
			// Update business.
			session.saveOrUpdate(business);
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("updateBusiness: " + ex.getMessage());
		}
		return true; // no error caught, then everything alright.
	}

	/**
	 * deleting the business linked with `id` id.
	 * @param id - the business with the given id.
	 * @return true if deleted successfully; otherwise false. (for example, if no such business)
	 */
	
	@Override
	public boolean deleteBusiness(int id) {
		session = factory.openSession();
		session.beginTransaction();
		Business b = (Business) session.get(Business.class, id);
		if(b == null) { // that tells us that there's no such business, so deletion cannot be performed.
			closeSession();
			return false;
		}
		
		// delete the business
		session.delete(b);
		
		// delete all the associated coupons with this business.
		SQLQuery deleteCoupons = session.createSQLQuery("delete from Coupons where business_id = '" + id + "'");
		deleteCoupons.executeUpdate();
		
		try {
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("deleteBusiness: " + ex.getMessage());
		}
		
		return true; // deleted successfully :)
	}

	/**
	 * Adding a new business to the database.
	 * @param business - the business that will be saved in database.
	 * @return true if added successfully; otherwise false.
	 */
	
	@Override
	public boolean addBusiness(Business business) {
		session = factory.openSession();
		session.beginTransaction();
		Business b = (Business) session.get(Business.class, business.getBusiness_id());
		if(b != null) { // mean that there's already business with same id. duplicate id not allowed.
			closeSession();
			return false;
		}
		
		// Save the business to the database.
		session.save(business);
		
		try {
			closeSession();
		} catch(HibernateException ex) {
			System.err.println("addBusiness: " + ex.getMessage());
		}
		return true;
	}
	

	/**
	 * Retrieving the id of the business inserted latest.
	 * @return last-inserted business' id.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public int getLastId() {
		session = factory.openSession();
		session.beginTransaction();
		// getting the max id from the table, which is the id of the last inserted business.
		List<Integer> b = (List<Integer>) session.createQuery("select max(id) from Business").list();
		if(b.get(0) == null) // there is no business there ..
			return 0; 
		
		closeSession();
		return b.get(0); // the last id..
	}

	/**
	 * Retrieves all the businesses into a list.
	 * @return all businesses in the database.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Business> getBusinesses() {
		session = factory.openSession();
		session.beginTransaction();
		// Fetch all the businesses.
		List<Business> b = (List<Business>) session.createQuery("from Business").list();
		closeSession();
		return b;
	}
	
	/**
	 * @return Whether a business with given id already exist in the database or not.
	 */

	@Override
	public boolean isExist(int id) {
		session = factory.openSession();
		session.beginTransaction();
		// Fetching the business with this id. checking how many businesses there are:
		boolean res = session.createQuery("from Business WHERE id=" + id).list().size() == 0;
		closeSession();
		return res;
	}

	/**
	 * Retrieving the given businesses page.
	 * @param pageNum - the number of page.
	 * @return List of `numBusinessesPerPage` businesses respectively to the page number.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Business> getPage(int pageNum) {
		Session session = factory.openSession();
		session.beginTransaction();
		Query query = session.createQuery("from Business");
		// seperate to page
		query.setFirstResult((pageNum - 1) * numBusinessesPerPage); // starting from result (pageNum - 1) * num per page.
		query.setMaxResults(numBusinessesPerPage); // (pageNum - 1) * numPerPage to pageNum * numPerPage gives us the desired page.
		List<Business> page = (List<Business>) query.list();
		try {
			session.getTransaction().commit();
			session.close();
		} catch(HibernateException ex) {
			System.err.println("getPage: " + ex.getMessage());
		}
		
		return page;
	}
	
	/**
	 * Retrieves how many pages there are.
	 * @return num pages relatively to num businesses per page.
	 */

	@Override
	public long numPages() {
		session = factory.openSession();
		session.beginTransaction();
		// count how many businesses there are
		long count = (long) session.createQuery("select count(*) from Business").uniqueResult();
		closeSession();
		// results divided by num per page.
		return (long) (count / numBusinessesPerPage) + (count % numBusinessesPerPage != 0 ? 1 : 0);
	}
	
	/**
	 * Committing the transaction and close the session.
	 */

	private void closeSession() {
		if(session.isOpen()) {
			if(!session.getTransaction().wasCommitted())
				session.getTransaction().commit();
			session.close();
		}
	}
}
