package dao;

import java.util.List;

import entities.User;

/**
 * The Administrator DAO interface.
 * @author Nimrod
 */
public interface IUserDao {
	/**
	 * Retrieved the admin with given id.
	 * @param id - admin id.
	 * @return - admin with the given id if exists; otherwise null.
	 */
	
	public User getUser(int id);
	
	/**
	 * Updates the admin with the given details.
	 * @param user - the details to update.
	 * @return true if updates successfully; otherwise false.
	 */
	
	public boolean updateUser(User user);
	
	/**
	 * Deletes the admin with given id.
	 * @param id - the admin with this id to delete.0
	 * @return true if deleted successfully; otherwise false.
	 */
	
	public boolean deleteUser(int id);
	
	/**
	 * Adds new administrator.
	 * @param user - the admin details
	 * @return true if added successfully; otherwise false.
	 */
	
	public boolean addUser(User user);
	
	/**
	 *  Retrieves the id of the last inserted administrator.
	 * @return 0 if table empty; otherwise the last-inserted admin's id.
	 */
	
	public int getLastId();
	
	/**
	 * Lists all the administrators.
	 * @return list of all the admins
	 */
	
	public List<User> getUsers();
	
	/**
	 * Matching a pair of username and password
	 * @param username - admin username
	 * @param password - admin password
	 * @return true if there is a pair that matchs given username and password; otherwise false.
	 */
	
	public boolean authenticateLogin(String username, String password);
}
