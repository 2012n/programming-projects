package dao;

import java.util.List;

import entities.Business;

/**
 * The business DAO interface.
 * @author Nimrod
 */

public interface IBusinessDao {
	/**
	 * Retrieves a business with the given id.
	 * @param id - id of this business.
	 * @return - business with given id if exists; otherwise null.
	 */
	
	public Business getBusiness(int id);
	
	/**
	 * Updates a business.
	 * @param business - the new details of the business.
	 * @return true if business updated successfully; otherwise false.
	 */
	
	public boolean updateBusiness(Business business);
	
	/**
	 * Deletes a business with a given id.
	 * @param id - the coupon's id to delete.
	 * @return - true if deleted successfully; otherwise false.
	 */
	
	public boolean deleteBusiness(int id);
	
	/**
	 * Adds a new business to the database.
	 * @param business - the business to add.
	 * @return true if added successfully; otherwise false.
	 */
	
	public boolean addBusiness(Business business);
	
	/**
	 * @return Last-inserted business' id.
	 */
	
	public int getLastId();
	
	/**
	 * @return List of all the businesses in the database.
	 */
	
	public List<Business> getBusinesses();
	
	/**
	 * @param id - Business id.
	 * @return true if exists business with given id; otherwise false.
	 */
	
	public boolean isExist(int id); // whether a business with this id exist?
	
	/**
	 * Create a page of business.
	 * @param page - page umber.
	 * @return a page of businesses.
	 */
	
	public List<Business> getPage(int page);
	
	/**
	 * @return How many pages there are.
	 */
	
	public long numPages();
}
