package dao;

import java.util.List;
import entities.Coupon;

/**
 * The coupons DAO interface.
 * @author Nimrod
 */

public interface ICouponsDao {
	
	/**
	 * Retrieves a coupon with the given id.
	 * @param id - id of desired coupon.
	 * @return coupon with the given id if exist; otherwise null.
	 */
	
	public Coupon getCoupon(int id);
	
	/**
	 * Updates a coupon.
	 * @param coupon - the new details of the coupon.
	 * @return true if updates successfully; otherwise false.
	 */
	
	public boolean updateCoupon(Coupon coupon);
	
	/**
	 * Deletes a coupon with the given id.
	 * @param id - the coupon's id to remove.
	 * @return - true if deleted successfully; otherwise false.
	 */
	
	public boolean deleteCoupon(int id);
	
	/**
	 * Add a coupon to the database.
	 * @param coupon - the coupon to add.
	 * @return true if added successfully; otherwise false.
	 */
	
	public boolean addCoupon(Coupon coupon);
	
	/**
	 * @return Last-inserted coupon's id.
	 */
	
	public int getLastId();
	
	/**
	 * @return List of all coupons in the database.
	 */
	
	public List<Coupon> getCoupons();
	
	/**
	 * Create a page of coupons.
	 * @param page - page number.
	 * @param isActive - whether include only active coupons or not.
	 * @return a page of coupons.
	 */
	
	public List<Coupon> getPage(int page, boolean isActive);
	
	/**
	 * @return How many pages there are.
	 */
	
	public long numPages();
	
	/**
	 * Retrieves coupons in a specific given category.
	 * @param categoty - coupons' category.
	 * @return - list of the coupons in the given category.
	 */
	
	public List<Object[]> getCouponsInCategory(String categoty);
	
	/**
	 * Retrieves coupons in a specific given category, that their parent business are in the radius range of "maxDistance" from the user (userLat, userLng)
	 * @param category - Category of the coupons.
	 * @param maxDistance - distance range in miles.
	 * @param userLat - Distance from latitude coordinate.
	 * @param userLng - Distance from longitude coordinate.
	 * @return
	 */
	
	public List<Object[]> getCouponsInCategoryWithMaxDistance(String category,
			int maxDistance, String userLat, String userLng);
}
