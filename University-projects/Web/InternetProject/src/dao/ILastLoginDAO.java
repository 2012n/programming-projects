package dao;

import entities.LastLogin;

/**
 * The last-login object DAO interface.
 * @author Nimrod
 */
public interface ILastLoginDAO {
	/**
	 * Retrieves the last login information.
	 * @return last user name & browser that logged in.
	 */
	
	public LastLogin getLastLogin();
	
	/**
	 * Updates the last log in information
	 * @param last - LastLogin object that contains username and browser.
	 * @return true if updated successfully; otherwise false.
	 */
	
	public boolean updateLastLogin(LastLogin last);
}
