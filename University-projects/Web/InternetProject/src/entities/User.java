package entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
	@Id
	private int user_id;
	private String username, password;

	/**
	 * Default constructor for the User class.
	 */
	
	public User() {
		user_id = 0;
		username = "";
		password = "";
	}
	
	/**
	 * A full-constructor for the user class.
	 * @param user_id - new user id. usually incremented value of the last one.
	 * @param username - username.
	 * @param password - password.
	 */
	public User(int user_id, String username, String password) {
		this.user_id = user_id;
		this.password = password;
		this.username = username;
	}

	/**
	 * Retrieving user id.
	 * @return user id.
	 */
	
	public int getUser_id() {
		return user_id;
	}

	/**
	 * Set user id
	 * @param user_id new user id
	 */
	
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * Retrieving user's passowrd
	 * @return user's password
	 */
	
	public String getPassword() {
		return password;
	}

	/**
	 * Set user password
	 * @param password new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Retrieving username
	 * @return username.
	 */

	public String getUsername() {
		return username;
	}

	/**
	 * set new username
	 * @param username new username
	 */
	
	public void setUsername(String username) {
		this.username = username;
	}
}
