package entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LastLogin {
	@Id
	int userID; // The user ID that logged on
	String username;
	String browser; // the browser that he used

	
	
	/**
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the browser
	 */
	public String getBrowser() {
		return browser;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param browser the browser to set
	 */
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	/**
	 * Last login object constructor.
	 * @param userID - last user Id that signed in
	 * @param username - the username
	 * @param browser - the browser the admin used to login the admin cp
	 */
	
	public LastLogin(int userID, String username, String browser) {
		this.userID = userID;
		this.username = username;
		this.browser = browser;
	}
	
	/**
	 * Default constuctor.
	 */
	
	public LastLogin() {
		this.userID = 0;
		this.username = "";
		this.browser = "";
	}
}
