package entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Businesses")
public class Business {
	@Id
	private int business_id;
	private String details, image, name;
	private String latitude, longitude; // business location
	private String category;
	
	/**
	 * Default business constructor
	 */
	
	public Business()  {
		business_id = 0;
		details = "";
		image = "";
		name = "";
		latitude = "0.0";
		longitude = "0.0";
	}
	
	/**
	 * Full Business constructor.
	 * @param business_id - business id.
	 * @param details - details about the business. 
	 * @param image - a thumbnail image of the business.
	 * @param name - the business name.
	 * @param latitude - the latitude of the business on the map.
	 * @param longitude - the longitude of the business on the map.
	 * @param cat - the category of the business
	 */
	
	public Business(int business_id, String details, String image, String name, String latitude, String longitude, String cat) {
		this.business_id = business_id;
		this.details = details;
		this.image = image;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		category = cat;
	}

	/**
	 * @return the business_id
	 */
	public int getBusiness_id() {
		return business_id;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param business_id the business_id to set
	 */
	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
}
