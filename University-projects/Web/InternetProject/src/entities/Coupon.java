package entities;

import javax.persistence.*;

@Entity
@Table(name = "Coupons")
public class Coupon {

	@Id
	private int id;
	private int business_id; // the business' id that offers this coupon.
	private String image, details, fadeoutDate, fadeoutHour;
	
	/**
	 * Default coupon constructor.
	 */
	
	public Coupon() {
		id = 0;
		business_id = 0;
		image = "";
		details = "";
		fadeoutDate = "";
		fadeoutHour = "";
	}
	
	/**
	 * Full Coupon constuctor.
	 * @param id - coupon id.
	 * @param business_id - the business_id that this coupon belongs to.
	 * @param image - a thumbnail image URL link that can describe the coupon.
	 * @param details - details about the coupon
	 * @param fadeoutDate - expire date of the coupon
	 * @param fadeoutHour - expire time of the coupon
	 */
	
	public Coupon(int id, int business_id, String image, String details, String fadeoutDate, String fadeoutHour) {
		this.id = id;
		this.business_id = business_id;
		this.image = image;
		this.details = details;
		this.fadeoutDate = fadeoutDate;
		this.fadeoutHour = fadeoutHour;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the business_id
	 */
	public int getBusiness_id() {
		return business_id;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @return the fadeoutDate
	 */
	public String getFadeoutDate() {
		return fadeoutDate;
	}

	/**
	 * @return the fadeoutHour
	 */
	public String getFadeoutHour() {
		return fadeoutHour;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param business_id the business_id to set
	 */
	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @param fadeoutDate the fadeoutDate to set
	 */
	public void setFadeoutDate(String fadeoutDate) {
		this.fadeoutDate = fadeoutDate;
	}

	/**
	 * @param fadeoutHour the fadeoutHour to set
	 */
	public void setFadeoutHour(String fadeoutHour) {
		this.fadeoutHour = fadeoutHour;
	}
}
