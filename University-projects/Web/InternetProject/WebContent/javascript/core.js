
/**
 * Core.js , the javascript file for the admin cp function!
********************/



var userX = 33.207933, userY = 35.570246; // save user x,y

/**
 * Shows a request for user location. in case or approval, location will be saved. otherwise a default location will be applied.
 */
function setUserLocation() {
	// Request user location
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(position) {
		  	userX = position.coords.latitude;
		  	userY = position.coords.longitude;
		});
	}
}

$(document).ready(function() {	

	/**
	 * Resized the map after the modal opened
	 */
	
	$('#addBusiness').on('shown', function () {
		google.maps.event.trigger(editmap, "resize");
	});
	
	/**
	 * Resized the map after the modal opened
	 */
	
	$('#showMap').on('shown', function() {
		google.maps.event.trigger(businessMap, "resize");
		businessMap.panTo(loc);
	});

	/**
	 * Resized the map after the modal opened
	 */
	
	$("#editBusiness").on('shown', function() {
		google.maps.event.trigger(editmap, "resize");
	});

	/**
	 * Send to the server request to add new business.
	 */
	$("#submitNewBusiness").click(function() {
		// Getting the values from the form.
		var texts = $("#addBusiness .modal-body input");
		var bname = texts[0].value;
		var image = texts[1].value;
		var details = $("#addBusiness .modal-body textarea").val();
		var coordinates;
		var cat = $("#business_cat").val();
		if(editmarker == undefined) {
			$("#addbusiness_error").html("<i class='icon-warning-sign'></i> Business location has not been set.");
			$("#addbusiness_error").fadeIn("medium");
			return;
		}
		coordinates = editmarker.getPosition(); // get position that marked on the map.
		// send data to the server:
		$.post("addbusiness",
				{bname: bname, image: image, details: details, latitude: coordinates.lat(), longitude: coordinates.lng(), category: cat}
				,function(data){ // you know data send successfully, treat if added successfully:
	      			if(data == "OK") { // bussiness added successfully, then hide the modal.
						$("#addBusiness").modal("hide");
	      			} else { // problem occurred.
						$("#addbusiness_error").html("<i class='icon-warning-sign'></i> " + data);
						$("#addbusiness_error").fadeIn("medium");
	      			}
	    	});
		});

	/**
	 * Send a request to the server to add new coupon.
	 */
	
	$("#addnewcouponbtn").click(function() {
		// Getting the values from the form.
		var texts = $("#addCouponForm .modal-body input");
		var bid = texts[0].value;
		var image = texts[1].value;
		var date = texts[2].value;
		var time = texts[3].value;
		var details = $("#addCouponForm .modal-body textarea").val();
		$.post("addnewcoupon",
				{bid: bid, image: image, details: details, date: date, time: time}, function(data) {
					if(data == "OK") // OK returned from the server, hide the modal.
						$("#addCouponForm").modal("hide");
					else { // server return error message, show it.
						$("#addcoupon_error").html("<i class='icon-warning-sign'></i> " + data);
						$("#addcoupon_error").fadeIn("medium");
					}
		}).fail(function() { // Could not send request.
			$("#addcoupon_error").html("<i class='icon-warning-sign'></i> An unknown error occurred while trying to add a new coupon. try again.");
			$("#addcoupon_error").fadeIn("medium");
		});
	});

	
	/**
	 * Send a request to the server to add new administrator.
	 */
	
	$("#submitnewadministrator").click(function() {
		// Getting values.
		var texts = $("#addAdmin .modal-body input");
		var name = texts[0].value;
		var pass = texts[1].value;
		
		$.post("addadmin",
				{username: name, password: pass}, function(data) {
					if(data == "OK") { // Server returned OK, hide the modal.
						$("#addAdmin").modal("hide");
					} else { // bad input sent, show the error
						$("#addadmin_error").html("<i class='icon-warning-sign'></i> " + data);
						$("#addadmin_error").fadeIn("medium");
					}
				}).fail(function() {
					$("#addadmin_error").html("<i class='icon-warning-sign'></i> An unknown error occurred while trying to add a new administrator. try again.");
					$("#addadmin_error").fadeIn("medium");
				});
	});

	/**
	 * Loads a business \ coupon page.
	 */

	$(".loadPage").click(function() {
		// get page number
		var page =  $(this).html();
		// get the load attribute that tells whether load a business or coupon.
		var whatLoad = $(this).attr('load');
		// The table that the loaded page will be filled in.
		var fillTable = $("#" + whatLoad).find('tbody:first');
		// if coupons: determine whether load only active coupons or not.
		var showActive = $("#onlyActive").prop('checked');
		// request the page
		$.post("loadPage",
				{whatToLoad: whatLoad, pageID: page, active: showActive}, 
				function(data) {
					fillTable.hide();
					fillTable.html(data);
					fillTable.fadeIn("medium");
		}).fail(function() { // for some reason could not send the request
			alert("An unknown error occurred while trying to load this coupon page. try again.");
		});	
	});

	/**
	 * Send a request to remove administrator \ coupon \ business.
	 */
	
	$(document).on('click', ".icon-remove", function() {
		// Get the row that `remove` button clicked.
		var row = $(this).parent('td').parent('tr');
		// the id of the coupon \ business \ admin to be removed.
		var id = $(this).parent('td').parent('tr').find('td:first').html();
		// get what to be deleted - business, admin or coupon.
		var deleteFrom = row.parent('tbody').parent('table').attr('id');
		// send delete request.
		$.post("delete", {id: id, from: deleteFrom}, function(data) {
			if(data == "OK") { 
				row.hide("medium"); // row deleted successfully, hide it.							
			} else { // "OK" not returned means a problem occurred.
				alert(data);
			}
		}).fail(function() { // the request failed to send
			alert("Falied to send request to the server. nothing happened.");
		});
	});
	
	/**
	 * Edit a business: turn all column to editable textbox.
	 * This is a special case because we have to treat the map edit in other way.
	 */
	
	$(document).on('click', '.icon-pencil.businessedit, .icon-ok-sign.businessedit, .icon-warning-sign.businessedit', function() {
		// reference to the pencil button that clicked.
		var pencil = $(this);
		var mode = $(this).attr('mode'); // mode: edit or save?
		var row = $(this).closest('tr'); // get the row that the user wants to edit.
		var id = row.find('td').html(); // get this first column content - the id of the business.
		var lat = $(this).attr('latitude'); // get the latitude coordinate of the business.
		var lng = $(this).attr('longitude'); // get the longitude coordinate of the business.
		// edit mode. turn everything to textbox.
		if(mode == "edit") {
			// Isolate img src and put it
			var imgtxt = "<input type='text' value='" + row.find("td:nth-child(2)").find('img').attr('src') + "' />";
			// set {img} editable
			row.find("td:nth-child(2)").html(imgtxt);
			// Turn {name, details, Categoty} to textbox
			row.find('td:not(:nth-last-child(-n+3)):not(:first):not(:eq(0))').each(function() { // turn every coulumn (but the 3 lasts) to editable textbox
				var asText = "<input type='text' value='" + $(this).html() + "' />";
				$(this).html(asText);
			});
			
			var oclk = "selectBusinessMap(' " + lat + "', '" + lng + "', 'editPlaceMap')"; // send the onclick function
			var comm = "<button type='button' class='btn btn-lg btn-block' href='#editBusiness' onclick=\"" + oclk + "\" data-toggle='modal' >open map</button>"
			row.find("td:eq(5)").html(comm); // plug a button that let you choose the new location
			// Ok
			pencil.attr('class', 'icon-ok-sign businessedit');
			pencil.attr('mode', 'save');
		} else {
			if(editmarker !== undefined) { // If the location re-choosed, set the new location
				lat = editmarker.getPosition().lat();
				lng = editmarker.getPosition().lng();
			}
			
			// Prepering to the a update request: 
			// setting up the arguments.
			var params = new Array();
			params.push("business"); // we submit a *business* update
			params.push(id); // the business id
			// get image url
			var gottenImg = row.find("td:nth-child(2)").find('input').val();
			// load image + add to array
			row.find("td:nth-child(2)").html("<img src='" + gottenImg + "' alt='' class='thumbnail' />");
			params.push(gottenImg);
			// turn again to plain text and add the argument to the array
			row.find('td:not(:nth-last-child(-n+3)):not(:first):not(:eq(0)) input').each(function() {
				var text = $(this).val();
				params.push(text);
				$(this).parent('td').html(text);
			});
			
			// restore the button
			comm = "<a href=\"#showMap\" data-toggle=\"modal\" onclick=\"showBusinessMap('" + lat + "' , '" + lng + "')\"><button type=\"button\" class=\"btn btn-lg btn-block\">Show on map</button></a>"
			row.find("td:eq(5)").html(comm);
			
			// push location
			params.push(lat);
			params.push(lng);
			// Update
			$.post("update", {params: params}, function(data) {
				pencil.attr('class', 'icon-pencil businessedit');
				if(data != "OK") // problem during update
					alert(data);
			}).fail(function() {
				// indicate that the edit failed.
				pencil.attr('class', 'icon-warning-sign businessedit');
				pencil.attr('title', 'error occurred. try again.');
			});
			pencil.attr('mode', 'edit');
		}
	});
	
	/**
	 * Edit a coupon or administrator
	 */
	
	$(document).on('click', '.icon-pencil.editcoupon, .icon-ok-sign.editcoupon, .icon-warning-sign.editcoupon', function() {
		var pencil = $(this); // reference to the pencil icon
		var mode = $(this).attr('mode'); // mode: edit or save?
		var row = $(this).closest('tr'); // get row
		var cat = "coupons";
		var id = row.find('td').html(); // get first column HTML that holds ID.
		if(mode == "edit") { // switch to textboxes
			// Isolate img src and put it
			var imgtxt = "<input type='text' value='" + row.find("td:nth-child(2)").find('img').attr('src') + "' />";
			// set {img} editable
			row.find("td:nth-child(2)").html(imgtxt);
			// make editable the rest..
			row.find('td:not(:nth-last-child(-n+2)):not(:first):not(:eq(0))').each(function() {
				var asText = "<input type='text' value='" + $(this).html() + "' />";
				$(this).html(asText);
			});							
			pencil.attr('class', 'icon-ok-sign editcoupon');
			pencil.attr('mode', 'save');
			
		} else { // save edit:
			var params = new Array();
			params.push(cat);
			params.push(id);
			// get image url
			var gottenImg = row.find("td:nth-child(2)").find('input').val();
			// load image + add to array
			row.find("td:nth-child(2)").html("<img src='" + gottenImg + "' alt='' class='thumbnail' />");
			params.push(gottenImg);
			// turn again to plain text and add the argument to the array
			row.find('td:not(:nth-last-child(-n+2)):not(:nth-child(2)) input').each(function() {
				var text = $(this).val();
				params.push(text);
				$(this).parent('td').html(text);
			});
			
			$.post("update", {params: params, id: id, cat: cat}, function(data) {
				if(data != "OK") 
					alert(data);
				pencil.attr('class', 'icon-pencil editcoupon');
			}).fail(function() {
				pencil.attr('class', 'icon-warning-sign editcoupon');
				pencil.attr('title', 'error occurred. try again.');
			});
			pencil.attr('mode', 'edit');
		}
	});
	
	/** 
	 * Edit admin
	 */
	
	$(document).on('click', '.icon-pencil.editadmin, .icon-ok-sign.editadmin, .icon-warning-sign.editadmin', function() {
		var pencil = $(this); // reference to the pencil 
		var mode = $(this).attr('mode'); // mode: edit or save?
		var row = $(this).closest('tr'); // get row
		var id = row.find('td').html(); // get first column HTML that holds ID.
		
		if(mode == "edit") { // switch to textboxes
			row.find('td:not(:nth-last-child(-n+2)):not(:first)').each(function() {
				var asText = "<input type='text' value='" + $(this).html() + "' />";
				$(this).html(asText);
			});	
			
			// Change ok to "ok" (press to update)
			pencil.attr('class', 'icon-ok-sign editadmin');
			// change mode to save (next press will save instead of edit)
			pencil.attr('mode', 'save');
			
		} else { // save:
			// Argumnets to send
			var params = new Array();
			params.push("admins");
			params.push(id);
			// turn again to plain text and add the argument to the array
			row.find('td:not(:nth-last-child(-n+2)) input').each(function() {
				var text = $(this).val();
				params.push(text);
				$(this).parent('td').html(text);
			});
			
			$.post("update", {params: params, id: id, cat: "admins"}, function(data) {
				pencil.attr('class', 'icon-pencil editadmin');
			}).fail(function() {
				pencil.attr('class', 'icon-warning-sign editadmin');
				pencil.attr('title', 'error occurred. try again.');
			});

			// Updated, next time will edit
			pencil.attr('mode', 'edit');
		}
	});
}); // jquery end

/** Show where the business is - unreselectalbe **/

var businessMap;

/**
 * Shows a map with the business location marked. you cannot edit the location.
 * @param x - location's latitude coordinate.
 * @param y - location's longitude coordinate.
 */

function showBusinessMap(x, y) {
	// settning up LatLng object.
	// Global so that the resize trigger can read it
	loc = new google.maps.LatLng(x, y);
	var xops = 
	{
		zoom: 15,
		center: loc
	};
	
	// set up the map on `businessMap` div.
	businessMap = new google.maps.Map(document.getElementById("businessMap"), xops);
	
	// Set a marker of the business
	google.maps.event.addListenerOnce(businessMap, 'idle', function(){
		businessMap.panTo(loc);
		new google.maps.Marker({
			position: loc,
			map: businessMap
		});
	});
}

var editmap;

/**
 * A map that allows reselect the location of the business.
 * @param x - current latitude coordinate
 * @param y - current longitude coordinate
 * @param loadOn - which div the map will be loaded on
 */

function selectBusinessMap(x, y, loadOn) { // a pickable map
	var loc = new google.maps.LatLng(x, y);
	var editops = {
		zoom: 17,
		center: loc
	};
	
	editmap = new google.maps.Map(document.getElementById(loadOn), editops);
	// Mark the original location
	google.maps.event.addListenerOnce(editmap, 'idle', function(){
		new google.maps.Marker({
			position: loc,
			map: editmap
		});
	});
	// Add a option to select point
	google.maps.event.addListener(editmap, "click", function(event)
	{
		editplaceMarker(event.latLng);
	});	
}

var editmarkersArray = new Array();
var editmarker;

function editplaceMarker(location) {
	// first remove all markers if there are any
	deleteOverlays();
	
	editmarker = new google.maps.Marker({
		position: location, 
		map: editmap
	});

	// add marker in markers array
	editmarkersArray.push(editmarker);

	//map.setCenter(location);
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
	if (editmarkersArray) {
		for (i in editmarkersArray) {
			editmarkersArray[i].setMap(null);
		}
	editmarkersArray.length = 0;
	}
}