<!DOCTYPE HTML>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
		<link type="text/css" rel="stylesheet" href="../style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				//	Sends a login request with the typed details.
				$("#subForm").click(function(){
					var username = $("#username").val();
					var password = $("#password").val();
					if(username == "") {
						alert("username cannot be empty!");
						return;
					} else if(password == "") {
						alert("password cannot be empty!");
					} else { // username and password filled 
						$.post("dologin", {username: username, password: password}, function(answer) {
							if(answer == "OK") // login successed! move to the admin control panel.
								location.href = "admincp"; 
							else
								alert(answer); // show the user message with the login has been failed.
						});
					}
				});
			});
		</script>
		<title>
			Please log in.
		</title>
	</head>
	<body>
		<form class="well span3" method="post">
			<label>Username: </label>
			<input type="text" name="username" id="username" class="span3" placeholder="Type username here.." /><br />
			
			<label>Password: </label>
			<input type="password" name="password" id="password" class="span3" placeholder="Type password here.." /> <br />
			
			<input type="button" id="subForm" class="btn btn-primary" value="Submit" />
		</form>
			
		<script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
	</body>
</html>