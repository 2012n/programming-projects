<%@page import="entities.*" %>
<%@page import="implementations.*" %>
<%@page import="java.util.List" %>

<!DOCTYPE HTML>
<html>
	<head>
		<link rel="shortcut icon" href="../img/favicon.png" type="image/png" />
		<title>CouponsDAO - Admin Control panel</title>
		<link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
		<link type="text/css" rel="stylesheet" href="../style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>		
		<script type="text/javascript" src="../javascript/core.js"></script>
	</head>
	
	<body onload="setUserLocation()">
		<div id="wrapper">
			<!-- main menu -->
			<ul class="nav nav-pills">
				<li><a href="index">index</a></li>
				<li class="active"><a href="admincp">Home</a></li>
				<li><a href="#businesses" class="navitem">Businesses</a></li>
				<li><a href="#coupons" class="navitem">Coupons</a></li>
				<li><a href="#admins" class="navitem">Admins</a></li>
				<li><a href="logout">Logout</a></li>
			</ul>
			
			<hr style="color:gray;" />
			
			<!-- Welcome message -->
			<h4>Hello <%= request.getAttribute("adminName") %>! :)</h4>
			
			<!-- last user accessed -->
			
			<hr style="color:gray;" />	
					
			<h3>Last login:</h3>
			
			<!-- Show the last administrator that logged in, which the used browser -->
			<table class="table table-hover table-striped" id="lastlogin">
				<thead>
					<tr>
						<th>Username</th>
						<th>Browser</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><%= request.getAttribute("LastLoginUsername") %></td>
						<td><%= request.getAttribute("LastLoginBrowser") %></td>
					</tr>
				</tbody>
			</table>
			
			<!-- Last coupons inserted -->
			<hr style="color:gray;" />
			
			<span style="font-size:24px; font-weight:bold;">
				<%= request.getAttribute("numCouponsPerPage") %> Recent Coupons
			</span>
		
			<!-- Add coupon button -->
			<div style="float:right">			
				<a href="#addCouponForm" data-toggle="modal">
					<button type="button" class="btn btn-primary">
						  <i class="icon-plus icon-white"></i>
						Add new coupon
					</button>
				</a>
			</div>
			
			<!-- Display a coupons page -->
			
			<table class="table table-hover table-striped" id="coupons">
				<thead>
					<tr>
						<th>#</th>
						<th>img</th>
						<th>business id</th>
						<th>details</th>
						<th>end date</th>
						<th>end time</th>
						<th style="width:30px;"><i title="edit" class="icon-pencil"></i></th>
						<th style="width:30px;"><i title="remove" class="icon-remove"></i></th>
					</tr>
				</thead>
				<tbody>
					
					<%
						// Retrieves the first page and display it
						List<Coupon> coupons = (List<Coupon>) request.getAttribute("firstCouponPage");
						for(Coupon c : coupons) {
					%>
						<tr>
							<td><%= c.getId() %></td>
							<td><img class="thumbnail" src="<%= (c.getImage().equals("") ? "../img/couponimg.png" : c.getImage())  %>" /></td>
							<td><%= c.getBusiness_id() %></td>
							<td><%= c.getDetails() %></td>
							<td><%= c.getFadeoutDate() %></td>
							<td><%= c.getFadeoutHour() %>
							<td><i class="icon-pencil editcoupon" mode="edit"></i></td>
							<td><i class="icon-remove"></i></td>
						</tr>
					<% } %>
				</tbody>
			</table>
			
			<!-- Choose whether to load only active or not -->
			
			<input type="checkbox" id="onlyActive" /> <i style="font-weight: bold; font-size: 10px;">Show only active coupons?</i>
			
			<!-- Show pages to load. -->
			<div class="pagination">
			  <ul class="loadPagebar" load="coupons">
				<%
					// part to pages
					long numPages = (Long) request.getAttribute("numCouponsPages");
					for(long i = 1; i <= numPages; ++i) {
				%>
						<li><a class="loadPage" load="coupons"><%= i %></a></li>
				<% } %>
			  </ul>
			</div>			
			
			
			<!-- Last X businesses inserted -->
			<hr style="color:gray;" />
			
			<span style="font-size:24px; font-weight:bold;">
				<%= request.getAttribute("numBusinessesPerPage") %> Recent businesses
			</span>
			
			<!-- Add business button -->
			<div style="float:right">
				<a href="#addBusiness" onclick='selectBusinessMap(userX, userY, "placeMap")' data-toggle="modal">
					<button type="button" class="btn btn-primary">
						  <i class="icon-plus icon-white"></i>
						Add new business
					</button>
				</a>
			</div>
			
			<!-- show businsses table -->
			<table id="businesses" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>img</th>
						<th>name</th>
						<th>details</th>
						<th>Category</th>
						<th>location</th>
						<th style="width:30px;"><i title="edit" class="icon-pencil"></i></th>
						<th style="width:30px;"><i title="remove" class="icon-remove"></i></th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<%
						// Get the first page
						List<Business> businesses = (List<Business>) request.getAttribute("firstBusinessPage");
						for(Business b : businesses) {
					%>
						<td><%= b.getBusiness_id() %></td>
							<td><img class="thumbnail" src="<%= (b.getImage().equals("") ? "../img/businessimg.png" : b.getImage()) %>" alt="" /></td>
							<td><%= b.getName() %></td>
							<td><%= b.getDetails() %></td>
							<td><%= b.getCategory() %>
							<td>
								<a href="#showMap" data-toggle="modal" onclick="showBusinessMap('<%= b.getLatitude() %>' , '<%= b.getLongitude() %>')">
									<button type="button" class="btn btn-lg btn-block">Show on map</button>
								</a>
							</td>
							<td><i class="icon-pencil businessedit" latitude="<%= b.getLatitude() %>" longitude="<%= b.getLongitude() %>"  case="business" mode="edit"></i></td>
							<td><i class="icon-remove"></i></td>
						</tr>
					<% } %>
				</tbody>
			</table>
					
			<!-- Show availables pages .. -->
			<div class="pagination">
			  <ul class="loadPage">
			  <%
			  		// Part businesses to pages
					long numBusinessPages = (Long) request.getAttribute("numBusinessesPages");
					for(long i = 1; i <= numBusinessPages; ++i) {
			   %>
					<li><a class="loadPage" load="businesses"><%= i %></a></li>
			   <% } %>
			  </ul>
			</div>
			
			<!-- See all administrators -->
			
			<hr style="color:gray;" />
			<span style="font-size:24px; font-weight:bold;">Admins</span>
			<a href="#addAdmin" data-toggle="modal">
				<button style="float:right" type="button" class="btn btn-primary">
					<i id="addAdministrator" class="icon-plus icon-white"></i> Add new Administrator
				</button>
			</a>
			<table id="admins" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>name</th>
						<th>Password</th>
						<th style="width:30px;"><i title="edit" class="icon-pencil"></i></th>
						<th style="width:30px;"><i title="remove" class="icon-remove"></i></th>
					</tr>
				</thead>
				<tbody>
				
				<%
					List<User> users = (List<User>) request.getAttribute("adminList");
					
					for(User user : users) {
				%>
						<tr>
							<td><%= user.getUser_id() %></td>
							<td><%= user.getUsername() %></td>
							<td>****</td>
							<td><i mode="edit" class="icon-pencil editadmin"></i></td>
							<td><i class="icon-remove"></i></td>
						</tr>
				<% } %>
				</tbody>
			</table>
			
<!------------------------------------------------------------------------------------>
<!-- popups part - hidden on load -->

			<!-- Show business map popup -->
			
			<div class="modal fade" style="width:640px; display:none;" id="showMap" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" align="center">
							<!-- Map will be placed here -->
							<div id="businessMap" style="width:600px; height:400px;"></div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Add new business form -->
			
			<div class="modal fade" id="addBusiness" style="width:640px; display: none;" role="dialog" aria-hidden="true">
				<div class="modal-header">
					<h3>Add new business</h3>
				</div>
				<div class="modal-body">
					<!-- Modal_error - a box that will show error if there is -->
					<div class="modal_error" id="addbusiness_error">x</div>
					<label>Business name:</label>
					<input type="text" style="width:100%;" placeholder="business name" />
					<label>details:</label>
					<textarea style="width:100%;" placeholder="Details about this business"></textarea>
					<label>Category:</label>
					<select id="business_cat">
						<option value="food">Food</option>
						<option value="toys">Toys</option>
						<option value="entertainment">entertainment</option>
						<option value="smartphones">Smartphones</option>
						<option value="electronics">Electronics</option>
					</select>
					<label>thumbnail image:</label>
					<input type="text" style="width:100%;" placeholder="URL refering to the image" />
					<label>Business Location:</label>
					<div id="placeMap" style="width:600px; height:300px;"></div>
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-primary" id="submitNewBusiness">add new business</button>
				</div>
			</div>
			
			<!-- add new coupon form -->
			
			<div class="modal fade" style="display:none;" id="addCouponForm" role="dialog">
				<div class="modal-header">
					<h3>Add new coupon</h3>
				</div>
				<div class="modal-body">
					<!-- Modal_error - a box that will show error if there is -->
					<div class="modal_error" id="addcoupon_error"></div>
					<label>business ID:</label>
					<input type="text" style="width:100%;" placeholder="business ID" />
					<label>details:</label>
					<textarea style="width:100%;" placeholder="Details about this coupon"></textarea>
					<label>thumbnail image:</label>
					<input type="text" style="width:100%;" placeholder="URL refering to the image" />
					<label>Expiry date: (<span style='background-color:#F7FFB3'>FORMAT: yyyy-mm-dd OR datebox; ex. 2014-01-01</span>)</label>
					<input type="date" style="width:100%" maxlength="12" placeholder="dd/mm/yyyy" />
					<label>Expiry time: (<span style='background-color:#F7FFB3'>FORMAT: hh:mm; ex. 11:11</span>)</label>
					<input type="text" style="width:100%;" maxlength="5" placeholder="hh:mm" />
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-primary" id="addnewcouponbtn">add new coupon</button>
				</div>
			</div>
			
			<!-- add new administrator -->
			
			<div class="modal fade" style="display: none;" id="addAdmin" role="dialog">
				<div class="modal-header">
					<h3>Add new administrator</h3>
				</div>
				
				<div class="modal-body">
					<!-- Modal_error - a box that will show error if there is -->
					<div class="modal_error" id="addadmin_error"></div>
					<label>Username</label>
					<input type="text" placeholder="administrator's name" />
					<label>Password</label>
					<input type="password" placeholder="password">
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-primary" id="submitnewadministrator">Add new administrator</button>
				</div>
			</div>
			
			<!-- Edit Business -->
			
			<div class="modal fade" id="editBusiness" style="width:640px; display: none;" role="dialog" aria-hidden="true">
				<div class="modal-header">
					<h3>Set Location</h3>
				</div>
				<div class="modal-body">
					<div id="editPlaceMap" style="width:600px; height:300px;"></div>
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-primary" id="saveLocation" onclick="$('#editBusiness').modal('hide')">Save location</button>
				</div>
			</div>
			
			<!-- Javascript source files -->
			<script type="text/javascript" src="../bootstrap/bootbox.min.js"></script>
			<script type="text/javascript" src="../bootstrap/js/bootstrap.js"></script>
		</div>
	</body>
</html>