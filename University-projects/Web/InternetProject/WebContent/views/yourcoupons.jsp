<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Enumeration"%>
<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List" %>
<%@page import="entities.*" %>
<%@page import="implementations.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
		<link type="text/css" rel="stylesheet" href="../style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<!-- main menu -->
			<ul class="nav nav-tabs">
				<li><a href="index">Coupons</a></li>
				<li class="active"><a href="yourcoupons" class="navitem">Your coupons</a></li>
				<li><a href="admincp">Admin cp</a></li>
			</ul>
			
			<table class="table table-hover table-striped" id="coupons">
				<thead>
					<tr>
						<th>#</th>
						<th>img</th>
						<th>business id</th>
						<th>details</th>
						<th>end date</th>
						<th>end time</th>
					</tr>
				</thead>
				<tbody>
					<%
						// get all chosen coupons and display them  
						ArrayList<Coupon> alist = (ArrayList<Coupon>) request.getAttribute("chosenCoupons");
						for(Coupon c : alist) {
					%>
						<tr>
							<td><%= c.getId() %></td>
							<td><img class="thumbnail" src="<%= (c.getImage().equals("") ? "../img/couponimg.png" : c.getImage())  %>" /></td>
							<td><%= c.getBusiness_id() %></td>
							<td><%= c.getDetails() %></td>
							<td><%= c.getFadeoutDate() %></td>
							<td><%= c.getFadeoutHour() %>
						</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	</body>
</html>