<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Install</title>
</head>
<body>
	<!-- This web page will be used to set a admin first time. it SHOULD NOT be use after install, and should be removed. -->
	<h2>This very simple page will help you set a administrator when you first time install the system.</h2>
	<form action="addadmin" method="post">
		Username: <input type="text" name="username" placeholder="Username" /> <br />
		Password: <input type="password" name="password" placeholder="password" /> <br />
		<input type="submit" value="Set admin user" />
	</form>
</body>
</html>