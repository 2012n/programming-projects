<%@page import="java.util.Enumeration"%>
<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List" %>
<%@page import="entities.*" %>
<%@page import="implementations.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
		<link type="text/css" rel="stylesheet" href="../style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				// once a checkbox click, remove or add the coupon to favourite.
				$(".addcoupon").click(function() {
					var checkbox = $(this); // clicked checkbox reference
					var id = $(this).parent('td').parent('tr').find('td').eq(1).html(); // related coupon's id 
					var action = checkbox.is(':checked') ? "add" : "remove"; // if checkbox checked, then add the coupon; otherwise remove.
					$.post("treatindexlist", {id: id, action: action}); 
				});
			}); // jquery end
		</script>
	</head>
	<body>
		<div id="wrapper">
			<!-- main menu -->
			<ul class="nav nav-tabs">
				<li class="active"><a href="index">Coupons</a></li>
				<li><a href="yourcoupons" class="navitem">Your coupons</a></li>
				<li><a href="admincp">Admin cp</a></li>
			</ul>
					<%
						String cat = request.getParameter("cat");
						if(cat == null) { // no parameter given - show categories list
					%>
							<h2>Please choose category.</h2>
							<ul style="list-style-type: none;">
						<%
							String[] cats = {"food", "toys", "entertainment", "smartphones", "electronics"}; 
							for(String xcat : cats) {
							// Show a button that links to a category coupons
						%>
								<li><a class="btn btn-lg btn-block" href="?cat=<%= xcat %>"><%= xcat %></a></li>				
						<% } %>
							</ul>
					<% 
						} else { // Category specified - show coupons of this catgory
					%>						
						<table class="table table-hover table-striped" id="coupons">
							<thead>
								<tr>
									<th>add</th>
									<th>#</th>
									<th>img</th>
									<th>business id</th>
									<th>details</th>
									<th>end date</th>
									<th>end time</th>
								</tr>
							</thead>
							<tbody>
					<%				
						// load all coupons of this categoty
						List<Object[]> coupons = (List<Object[]>) request.getAttribute("coupons");
							for(Object[] c : coupons) {
								int id = (Integer) c[0];					
						%>
							<tr>
								<td><input type="checkbox" class="addcoupon"<%= session.getAttribute(id +"") == null ? "" : "checked=\"checked\""  %> /></td>
								<td><%= c[0]%></td>
								<td><img class="thumbnail" src="<%= (c[5].equals("") ? "img/couponimg.png" : c[5])  %>" alt="" /></td>
								<td><%= c[1] %></td>
								<td><%= c[2] %></td>
								<td><%= c[3] %></td>
								<td><%= c[4] %>
							</tr>
						<% } %>
				</tbody>
			</table>
			<% } %>
		</div>
	</body>
</html>