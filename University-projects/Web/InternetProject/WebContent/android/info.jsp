<%@page import="entities.Coupon"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<title>CoCouppn!</title>
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	</head>
	<body>
		<div data-role="page">
			<div data-role="header" data-theme="b">
				<h1>Choose food coupon</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="home" data-icon="home">Home</a></li>
						<li><a data-rel="back" data-icon="back">Return</a></li>
					</ul>
				</div>
			</div>
	
			<!-- Show details about this coupon.. -->
			<table id="showcoupons" data-role="table" data-mode="reflow">
				<thead style="background-color: #DDD; font-weight: bold;">
					<tr>
						<td>#</td>
						<td>Img</td>
						<td>Business ID</td>
						<td>details</td>
						<td>end date</td>
						<td>end time</td>
					</tr>
				</thead>
	
				<tbody>
						<%
							// Get the coupon from the controller
							Coupon c = (Coupon) request.getAttribute("getcouponinfo");
							if(c == null) // if a wrong ID given the coupon will be null.
								out.println("<td colspan='6' align='center'>No such coupon!!!!!!!!</td>");
							else { // coupon exist , write it
						%>
					<tr>
						<td><%= c.getId() %></td>
						<td><img src="<%= c.getImage() %>" alt="" style="height: 20px; width: 20px;" /></td>
						<td><%= c.getBusiness_id() %></td>
						<td><%= c.getDetails() %></td>
						<td><%= c.getFadeoutDate() %></td>
						<td><%= c.getFadeoutHour() %></td>
					</tr>
						<% } %>
				</tbody>
			</table>
		</div>
	</body>
</html>