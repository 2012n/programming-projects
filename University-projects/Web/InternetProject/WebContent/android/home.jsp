<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<title>CoCoupon!</title>
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>
	</head>
	<body>
		<div data-role="page" id="home">
			<div data-role="header" data-theme="b">
				<h1>Welcome!</h1>
				<div data-role="navbar">
					<ul>
						<li><a href="home" data-icon="home">Home</a></li>
						<li><a data-rel="back" data-icon="back">Return</a></li>
					</ul>
				</div>
			</div>
	
			<!-- Show available categories -->
			<div data-role="content">
				<strong>Please choose the desired category from the following:</strong>
				<div data-role="controlgroup">
				<%
					String[] cats = {"food", "toys", "entertainment", "smartphones", "electronics"};
					for(String cat : cats) {
					// Set a button for each category
				%>
						<a data-role="button" data-ajax="false" href="categoty?cat=<%= cat %>"><%= cat %></a>
				<% } %>
				</div>
			</div>
		</div>
	</body>
</html>