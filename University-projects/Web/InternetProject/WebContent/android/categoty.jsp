<%@page import="java.util.List" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>CoCouppn!</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.css" />
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
		<script src="https://maps.googleapis.com/maps/api/directions/json?parameters"></script>
		<script type="text/javascript">
			// Get user location
			var userX = 0, userY = 0; // default x,y in case of deny
			var hasLocationSet = false; // userLocation has not been called or denied.
			
			$(document).ready(function() {
				// resizes map to popup size ..
				$("#showMap").bind({
					popupafteropen: function() {
						google.maps.event.trigger(map, "resize");
					}
				});
				
				// Send request to coupons in requested distance.
				$("#slider-fill").change(function() {
					var sliderValue = $(this).val();
					var isNum = isFinite(sliderValue);
					if(isNum) { // Checking whether given value is a number,
						sendCouponsRequest(sliderValue);
					}
				});
			}); // end jquery
					
			// setting up map to show direction for current location to destination
			function setupMap(x, y) {
				var loc = new google.maps.LatLng(x, y);
				var ops = { // set map options: zoom 15 and will center at (x,y)
					zoom: 15,
					center: loc
				};
				
				// draws the map
				map = new google.maps.Map(document.getElementById("businessMap"), ops);

				// set up  direction
				if(!hasLocationSet) // cannot show direction cause no start point
					return;
				
				// start point: detected user location
				var start = new google.maps.LatLng(userX, userY);
				var direction = new google.maps.DirectionsRenderer();
				directionsService = new google.maps.DirectionsService();
				direction.setMap(map);
				
				// set the way: from start (user location) to the coupon's business' location
				var request = {
					origin: start, 
					destination: loc,
					travelMode: google.maps.TravelMode.DRIVING // set direction for a car
				};
				
				directionsService.route(request, function(response, status) {
					if(status == google.maps.DirectionsStatus.OK)
						direction.setDirections(response);
				});
			}
			
			// Fetch close coupons from the server
			function sendCouponsRequest(maxDistance) {
				var xmlhttp;
				// set up the xhr object
				if(window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						// If everything is alright then write the coupons to the showcoupons div
						document.getElementById("showcoupons").innerHTML = xmlhttp.responseText;
					}
				};
				
				// send request for coupons
				xmlhttp.open("GET", "loadCouponsToClient?cat=<%= request.getParameter("cat") %>&lat=" + userX + "&lng=" + userY + "&maxDistance=" + maxDistance, true);
				xmlhttp.send();
			}
			
			function setUserLocation() {
				if(navigator.geolocation) { // check whether a location is available
					navigator.geolocation.getCurrentPosition(function(pos) {
						// Location request accepted 
						// set latitude and longitude
						userX = pos.coords.latitude; 
						userY = pos.coords.longitude;
						hasLocationSet = true;
						// load coupons with distance radius < 10
						sendCouponsRequest(10);
					}, function() {
						// Failed to get location. (request denied)
						document.getElementById("showcoupons").innerHTML = 
							"<strong>Warning: we must have your location! until then, you cannot use this site. <br />" + 
							"Cannot load content as I don't have your location.</strong>";
					});
				} else { // no geolocation support
					alert("You cannot use this site because we have no info about your location");	
				}
			}
		</script>
	</head>
	
	<!-- Get user location when loaded -->
	<body onload="setUserLocation()">	
		<div data-role="page">
			<div data-role="header" data-theme="b">
				<h1>Choose <%= request.getParameter("cat") %> coupon</h1>
				  <div data-role="navbar">
				    <ul>
				      <li><a href="home" data-icon="home">Home</a></li>
				      <li><a data-rel="back" data-icon="back">Return</a></li>
				    </ul>
				  </div>
			</div>
			
			<!-- slider for coupons distance -->
			
			<label for="slider-fill">Max coupon distance: </label>
			<input type="range" name="slider-fill" id="slider-fill" value="10" min="1" max="100" data-highlight="true" />
			
			<table id="showcoupons"  data-role="table" data-mode="column">
				<tr>
					<!-- ajax loader to indicate that process active .. -->
					<!-- when finish load, it will be replaced with the coupons -->
					<td width="100%"><img src="../img/ajax-loader.gif" /></td>
				</tr>
			</table>
			
			<!-- set up map popup -->
			<div data-role="popup" id="showMap" data-overlay-theme="a" data-theme="c" class="ui-corner-all">
				<div data-role="header" data-theme="a" class="ui-corner-top">
					<h1>Business Location</h1>
				</div>
			
				<!-- Load here the direction to the business -->
				<div id="businessMap" style="width:400px; height:250px;"></div>
			</div>
		</div> <!-- end page -->
	</body>
</html>