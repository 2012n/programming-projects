/**
 * Created by nimrod on 10/10/15.
 */

define(['jquery', 'Util', 'Settings'], function($, Util, Settings) {

    var Scientist = function Scientist() {};

    var settings = Settings;
    var table = $("#settings_form").children("tbody");
    var constant = Settings.CONSTANTS;

    Scientist.prototype.loadSettings = function() {
        Object.keys(settings).forEach(function(param) {
            if(constant.indexOf(param) != -1) // a constant
                return false;
            table.append("<tr>" +
                         "   <td>" + param.replace(/_/g, ' ').toLowerCase() + "</td>" +
                         "   <td><input type='text' class='form-control' value='" + settings[param] + "' /></td></tr>");
        });
    };

    Scientist.prototype.updateParams = function() {
        // prepare data - send all parameters
        var data = new Object();
        data["action"] = "updateParams";
        // now read all the params
        table.find("tr").each(function() {
            var tr = $(this);
            var td = tr.find("td");
            var name = (td[0].innerHTML).replace(/ /g, '_').toUpperCase();
            var value = $(td[1]).children('input').val();
            // add to data
            data[name] = value;
        });

        // all data is in
        Util.postRequest(data, function(data) {
            if(data != "OK") {
                console.log("Could not update settings; unknown error.");
                return false;
            } // else:
            alert("Params updated successfully.");
        });
        console.log("Updating params ..");
    };

    Scientist.prototype.downloadSummery = function() {
        window.open(settings.SERVER_ADDR + "/?action=tblGameToExcel", "_blank");
        console.log("Downloading summery ..");
    };

    // Set events
    $("#save_settings").click(function() {
        scientist.updateParams();
    });

    $("#download_summery").click(function() {
       scientist.downloadSummery();
    });
    // Run it
    var scientist = new Scientist();
    scientist.loadSettings();
});