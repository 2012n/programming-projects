/**
 * Created by nimrod on 10/4/15.
 */

/**
 * This file will hold all the need configuration.
 * DO NOT MODIFY ANYTHING BUT VALUES!!
 **/

define(['jquery'], function($) {
    var settings = new Object();
    // Define the const settings - should not be changed.
    // ------------- View settings ----------
    settings.VIEW_TIMER = "time";
    settings.VIEW_EDGE_HITTING_COUNT = "edgeHitting";
    settings.VIEW_PACE = "pace";
    settings.VIEW_PACE_FORMAT = "paceFormat";
    // ------- Server settings --------------
    settings.SERVER_ADDR = "server.php";
    settings.SERVER_METHOD = "post";
    // -------- Message codes ---------------
    settings.MESSAGE_GAME_WON = 0x00;
    settings.MESSAGE_FIRST_BOARD_FOCUSED = 0x01;
    settings.MESSAGE_BLINK_MESSAGEBOARD =  0x02;
    // --------------------------------------
    // ------ DO NOT change the below -------
    settings.PLAYER_SHOW_TRAIL = false;
    settings.WALL_LINE_WIDTH = 2;
    // -------- the consts - the above ------
    settings.CONSTANTS = ["VIEW_TIMER", "VIEW_EDGE_HITTING_COUNT", "VIEW_PACE", "VIEW_PACE_FORMAT", "MESSAGE_GAME_WON", "SERVER_ADDR", "SERVER_METHOD", "CONSTANTS", "PLAYER_SHOW_TRAIL", "WALL_LINE_WIDTH", "MESSAGE_FIRST_BOARD_FOCUSED", "MESSAGE_BLINK_MESSAGEBOARD"];
    // -------------------------

    // retrieve settings from server
    var data_from_server = "";
    $.ajax({
        type: settings.SERVER_METHOD,
        url: settings.SERVER_ADDR,
        data: {action: "readParams"},
        success: function(data) {
            if(data == "BAD") {
                console.log("Could not retrieve settings from server; unknown error.");
                return false;
            } // else:
            try {
                data_from_server = JSON.parse(data);
            } catch(ex) {
                console.log("settings from server: not JSON!");
                console.log(data);
            }
        },

        async: false
    });

    // update settings
    for(var field in data_from_server) {
        var as_int = parseInt(data_from_server[field]);
        settings[field] = isNaN(as_int) ? data_from_server[field] : as_int ;
    }

    // return the settings to use
    return settings;
});
