/**
 * Created by nimrod on 10/4/15.
 * This module contains some utilities;
 */


define(['Settings'], function(Settings) {

    // functions implementations
    // -------------------------

    /**
     * Applies a timer on a specific component (div or span)
     * @returns {number} - interval id for stopping it.
     */

    var applyTimer = function() {
        var container = document.getElementById(Settings.VIEW_TIMER);
        var overall_time = 0;
        var interval_id = setInterval(function() {
            overall_time++;
            // split to hours, minutes, seconds
            var hours = Math.floor(overall_time / 3600);
            var minutes = Math.floor((overall_time / 60) % 60);
            var seconds = overall_time % 60;

            // format the time
            var formattedTime = ((hours < 10 ? "0" : "") + hours) + ":" +
                ((minutes < 10 ? "0" : "") + minutes) + ":" +
                ((seconds < 10 ? "0" : "") + seconds);

            // display the time:
            container.innerHTML = formattedTime;

        }, 1000);

        return interval_id;
    };

    /**
     * Send a post request to the server.
     * @param data - the data.
     * @param success - the sucess callback function. gets one parameter: response.
     */

    var postRequest = function(data, success) {
        $.ajax({
            type: Settings.SERVER_METHOD,
            url: Settings.SERVER_ADDR,
            data: data,
            success: success
        });
    };

    var setStartTime = function() {
      var startTime = document.getElementById("startTime");
      var currentdate = new Date();
      var hours = currentdate.getHours();
     var minutes = currentdate.getMinutes();
        var seconds = currentdate.getSeconds();
        var formattedTime = ((hours < 10 ? "0" : "") + hours) + ":" +
            ((minutes < 10 ? "0" : "") + minutes) + ":" +
            ((seconds < 10 ? "0" : "") + seconds);

        startTime.innerHTML = formattedTime;
    };

    // return the module itself:
    return {
        applyTimer: applyTimer,
        postRequest: postRequest,
        setStartTime: setStartTime
    };

});