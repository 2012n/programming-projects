/**
 * Created by nimrod on 10/4/15.
 * This module describes a cell.
 */

define(function() {
    /**
     * The cell Object.
     * @param x - location on board. (optional)
     * @param y - location on board. (optional)
     * @constructor - constructs the object.
     */

    var Cell = function(x, y) {
        this.visited = false;
        this.walls = 0x1111; // Each bit specifies if walls [top, bottom, left, right] is broken or not. 1 - not, 0 - broken.
        this.x = x;
        this.y = y;
    };

    /**
     * Defines the dummy cell. used to return a neigboor whenever a cell don't have one.
     * @type Cell.
     */

    Cell.outOfBound = new Cell();
    Cell.outOfBound.visited = true;

    /**
     * Defines the walls.
     * @type {{UP: upper wall, DOWN: lower wall, LEFT: left wall, RIGHT: right wall}}
     */

    Cell.walls = {
        UP: 0x1000,
        DOWN: 0x0100,
        LEFT: 0x0010,
        RIGHT: 0x0001
    };

    /**
     * Gets a wall in text representation and returns its bit value.
     * @param wall - wall in string.
     * @returns wall in bits.
     */
    Cell.prototype.textToHex = function(wall) {
        switch (wall) {
            case "UP": return Cell.walls.UP;
            case "DOWN": return Cell.walls.DOWN;
            case "LEFT": return Cells.walls.LEFT;
            case "RIGHT": return Cells.walls.RIGHT;
        }
    };

    // return the cell object
    return Cell;
});
