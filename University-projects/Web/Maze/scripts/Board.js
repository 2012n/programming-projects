/**
* Created by nimrod on 10/4/15.
 * This module is describes the board itself.
 * Dependencies: Cell and Settings.
*/

define(['Cell', 'Settings'], function(Cell, Settings) {

    var Board = function Board(width, height, pixelForCell) {
        this.width = width;
        this.height = height;
        this.pixelForCell = pixelForCell;
        this.cells = [];
        this.canvas;
    };

    /**
     * Initializes the board. create the cells.
     */

    Board.prototype.init = function() {
        console.log("init start");
        for(var y = 0; y < this.height; y++) {
            for(var x = 0; x < this.width; x++)
                this.cells.push(new Cell(x, y));
        }

        this.canvas = document.getElementById("canvas");
        if(!this.canvas) {
            this.canvas = document.createElement("canvas");
            this.canvas.id = "canvas";
            document.body.appendChild(this.canvas);
        }

        this.canvas.width = this.width * this.pixelForCell;
        this.canvas.height = this.height * this.pixelForCell;

        // Draw the background::
        var context = this.canvas.getContext("2d");
        context.fillStyle = Settings.MAZE_WAY_COLOR;
        context.fillRect(0, 0, this.width * this.pixelForCell, this.height * this.pixelForCell);
        console.log("init end");
    };

    /**
     * Returns the cell at a coordinates.
     * @param x - x coordinate.
     * @param y - y coordinate.
     * @returns {Cell} the cell.
     */

    Board.prototype.getCell = function(x, y) {
        return this.cells[y * this.width + x];
    };

    /**
     * Removes a wall.
     * @param x,y - the cell
     * @param wall - the wall to be removed. can be: Cell.walls.UP, Cell.walls.DOWN, Cell.walls.LEFT, Cell.walls.RIGHT.
     */

    Board.prototype.removeWall = function(x, y, wall) {
        var cell = this.getCell(x, y);
        cell.walls ^= wall;
    };

    /**
     * Returns the neigboors of a certain cell.
     * @param x - cell's x.
     * @param y - cell's y.
     * @returns {{UP: neig from up, DOWN: neig from down, LEFT: neig from left, RIGHT: neig from right}}
     */

    Board.prototype.getNeigbors = function(x, y) {
        return {
            UP:   (y > 0 ? this.getCell(x, y - 1) : Cell.outOfBound),
            DOWN: (y < this.height - 1 ? this.getCell(x, y + 1) : Cell.outOfBound),
            LEFT: (x > 0 ? this.getCell(x - 1, y) : Cell.outOfBound),
            RIGHT:(x < this.width - 1 ? this.getCell(x + 1, y) : Cell.outOfBound)
        };
    };

    /**
     * Counts number of unvisited neigboors of a certain cell.
     * @param x - cell's x.
     * @param y - cell's y.
     * @returns {number of unvisited neigboors.}
     */
    Board.prototype.countUnvisitedNeigbords = function(x, y) {
        var neig = this.getNeigbors(x, y);
        return (neig.UP.visited ? 0 : 1 + neig.DOWN.visited ? 0 : 1 + neig.LEFT.visited ? 0 : 1 + neig.RIGHT.visited ? 0 : 1);
    };

    /**
     * Generates a maze board.
     */

    Board.prototype.generate = function() {
        console.log("genearte start");
        var self = this;
        var stack = [];
        var randOpt = ["UP", "DOWN", "LEFT", "RIGHT"];
        var rand;
        var cell;
        var neig;
        var next;
        var unvisitedNeigs = [];

        // create the canvas
        // init canvas
        // get canvas context
        var ctx = this.canvas.getContext("2d");
        var eachX = this.pixelForCell;
        var eachY = this.pixelForCell;


        ctx.lineWidth = Settings.WALL_LINE_WIDTH;
        ctx.strokeStyle = Settings.MAZE_WALL_COLOR;

        // Draw the full canvas first:
        for(var y = 0; y < this.height; y++) {
            for(var x = 0; x < this.width; x++) {
                ctx.strokeRect(x * eachX, y * eachY, eachX, eachY);
            }
        }


        ctx.fillStyle = Settings.MAZE_WAY_COLOR;
        // create the entry:
        ctx.fillRect(2, 0, eachX - 3, eachY - 1);
        // create the out
        ctx.fillRect((this.width - 1) * eachX, (this.height - 1) * eachY, eachX, eachY);

        // Now that we have a drawn canvas, just remove the walls where needed:
        var drawPath = function(x, y) {
            // current cell - mark as visit
            cell = self.getCell(x, y);
            cell.visited = true;

            neig = self.getNeigbors(x, y);
            // drop the unvisited neigs
            unvisitedNeigs = [];
            var i = 0;
            if(!neig["UP"].visited) unvisitedNeigs[i++] = ({"obj": neig["UP"], "wall": Cell.walls.UP});
            if(!neig["DOWN"].visited) unvisitedNeigs[i++] = ({"obj": neig["DOWN"], "wall": Cell.walls.DOWN});
            if(!neig["LEFT"].visited) unvisitedNeigs[i++] = ({"obj": neig["LEFT"], "wall": Cell.walls.LEFT});
            if(!neig["RIGHT"].visited) unvisitedNeigs[i++] = ({"obj": neig["RIGHT"], "wall": Cell.walls.RIGHT});

            if(unvisitedNeigs.length != 0) {
                var nextBuild = unvisitedNeigs[Math.floor(Math.random() * (unvisitedNeigs.length)) + 0];
                var next = nextBuild.obj;
                var wallToBreak = nextBuild.wall;

                stack.push(cell);
                self.removeWall(x, y, wallToBreak);

                // remove wall from canvas
                ctx.beginPath();
                ctx.lineWidth = Settings.WALL_LINE_WIDTH;
                ctx.strokeStyle = Settings.MAZE_WAY_COLOR;
                switch(wallToBreak) {
                    case Cell.walls.UP:
                        ctx.moveTo(x * eachX + 1, y * eachY);
                        ctx.lineTo(x * eachX + eachX - 1, y * eachY);
                        break;
                    case Cell.walls.DOWN:
                        ctx.moveTo(x * eachX + 1, y * eachY + eachY);
                        ctx.lineTo(x * eachX + eachX - 1, y * eachY + eachY)
                        break;
                    case Cell.walls.LEFT:
                        ctx.moveTo(x * eachX, y * eachY + 1);
                        ctx.lineTo(x * eachX, y * eachY + eachY - 1);
                        break;
                    case Cell.walls.RIGHT:
                        ctx.moveTo(x * eachX + eachX, y * eachY + 1);
                        ctx.lineTo(x * eachX + eachX, y * eachY + eachY -1);
                        break;
                }

                ctx.stroke(); // do the draw
                // end remove wall from canvas
                return drawPath(next.x, next.y);
            }

            else { // no neighbords!!
                if(stack.length == 0) {
                    return true;
                } else {
                    var back = stack.pop();
                    return drawPath(back.x, back.y);
                }
            }
        };

        // start point
        drawPath(0, 0);
        console.log("generate end");
    };

    /**
     * Checks if the squared area [x, y, x+w, y+h] contains any edges.
     * @returns false if not; otherwise true;
     */

    Board.prototype.isContainsEdges = function(x, y, w, h) {
        var ctx = this.canvas.getContext("2d");
        var data = ctx.getImageData(x, y, w, h).data;
        for(var i = 0; i < data.length; i += 4) {
            // if there's an edge somewhere:
            var hex = parseInt(data[i].toString(16) + "" + data[i + 1].toString(16) + "" + data[i + 2].toString(16), 16);
            var wallColor = parseInt(Settings.MAZE_WALL_COLOR.substring(1), 16);
            if(data[i + 3] != 0) {
                if(hex == wallColor)
                    return true;
            }
        }

        return false;
    };

    /**
     * Retuns the board canvas.
     * @returns board canvas.
     */

    Board.prototype.getCanvas = function() {
        return this.canvas;
    };

    // return board module
    return Board;
});
