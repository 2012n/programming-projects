/**
 * Created by nimrod on 10/4/15.
 * The player module. represents the player.
 * Dependencies: Settings.
 */

define(['jquery', 'Settings', 'Util'], function($, Settings, Util) {

    var ARROW_LEFT =  Settings.ARROW_LEFT,
        ARROW_UP =    Settings.ARROW_UP,
        ARROW_RIGHT = Settings.ARROW_RIGHT,
        ARROW_DOWN =  Settings.ARROW_DOWN;
    var MOVE_PHASE = Settings.PLAYER_MOVE_PHASE; // how many pixels to move in a single move.
    var self; // holds `this` for anonymous functions.
    var edgeHittingCount = 0; // counts the times player hit the wall
    var canvas = document.getElementById("canvas");

    /**
     * Defines the player.
     * @param size - the size of the player. it will be square of sizexsize.
     * @param color - either name or hex code.
     * @param board - the canvas to draw on.
     * @constructor - builds a player
     */

    var Player = function(size, color, board) {
        this.done = false; // if game is done or not
        this.size = size;
        this.color = color;
        this.x = 0; // x coordinate of player
        this.y = 0; // y coordinate of player
        this.board = board;
        this.canvas = board.getCanvas();
        this.ctx = canvas.getContext("2d");
        self = this;
        this.edgeHittingView = Settings.VIEW_EDGE_HITTING_COUNT;
        this.path = [];
        this.lastTimeHit = false;
    };

    /**
     * Draws the player at certain point.
     * @param newX - the x point.
     * @param newY - the y point.
     */

    Player.prototype.movePlayer = function(newX, newY) {
        if(!Settings.PLAYER_SHOW_TRAIL) { // remove trail
            this.ctx.fillStyle = Settings.MAZE_WAY_COLOR;
            this.ctx.fillRect(this.x, this.y, this.size, this.size);
        }
        // update the new place
        this.x = newX;
        this.y = newY;
        // draw new place
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(this.x, this.y, this.size, this.size);

        // check if in the end:
        if(newX + this.size > this.board.width * this.board.pixelForCell ||
           newY + this.size > this.board.height* this.board.pixelForCell) {
            this.winProc();
        }
    };

    /**
     * What to do thenever you win
     */

    Player.prototype.winProc = function() {
        // stop the timer - game finished
        window.postMessage(Settings.MESSAGE_GAME_WON, "*");
        if(this.done)
            return;

        this.done = true; // game is done!

        var _zigzug = 0,
            _complete_time = document.getElementById(Settings.VIEW_TIMER).innerHTML,
            _average_speed = document.getElementById(Settings.VIEW_PACE).innerHTML,
            _edge_hitting  = document.getElementById(Settings.VIEW_EDGE_HITTING_COUNT).innerHTML,
            _start_time    = document.getElementById("startTime").innerHTML,
            _path          = JSON.stringify(this.path);

        // send to server, etc..
        data = {action: "saveGame", zigzug: _zigzug,
                complete_time: _complete_time, average_speed: _average_speed,
                num_edge_hitting: _edge_hitting, start_time: _start_time, path: _path};

        var success = function(response) {
            console.log("Game won. response: " + response);
        }

        // send data
        Util.postRequest(data, success);
    };

    /**
     * Move the player whenever a arrow key pressed.
     */

    document.addEventListener('keydown', function(e) {
        if(! $("#canvas").is(":focus")) return; // move only if canvas focused
        var edges = self.board.isContainsEdges;
        var edgeHitten = false;
        switch(e.keyCode) {
            case ARROW_LEFT:
                if(edges(self.x - MOVE_PHASE, self.y, self.size, self.size)) { edgeHitten = true; break };
                self.movePlayer(self.x - MOVE_PHASE, self.y);
                break;
            case ARROW_UP:
                if(edges(self.x, self.y - MOVE_PHASE, self.size, self.size)) { edgeHitten = true; break; } // will hit an edged!
                self.movePlayer(self.x, self.y - MOVE_PHASE);
                break;
            case ARROW_RIGHT:
                if(edges(self.x + MOVE_PHASE, self.y, self.size, self.size)) { edgeHitten = true; break; }
                self.movePlayer(self.x + MOVE_PHASE, self.y);
                break;
            case ARROW_DOWN:
                if(edges(self.x, self.y + MOVE_PHASE, self.size, self.size)) { edgeHitten = true; break; }
                self.movePlayer(self.x, self.y + MOVE_PHASE);
                break;
        }

        // if edge hitten, update counter
        if(edgeHitten) {
            if(!self.lastTimeHit) {
                document.getElementById("edgeHitting").innerHTML = (++edgeHittingCount) + "";
                self.lastTimeHit = true; // In this move he hitted the wall.
            }
        }
        else { // moved - not hitten a wall.
            self.path.push({"x": self.x, "y": self.y});
            self.lastTimeHit = false;
        }
    });

    /**
     * Calculates a move pace every Settings.PACE_PX_PER_NUMBER_SEC seconds. sets an interval.
     * @returns the id of the interval.
     */

    Player.prototype.startCalculatePace = function() {
        var prevX = Settings.PLAYER_START_X;
        var prevY = Settings.PLAYER_START_Y;
        var paceSpan = document.getElementById(Settings.VIEW_PACE);
        var sum_pace = 0, pace_rounds = 0, average_pace;
        // update pace format

        document.getElementById(Settings.VIEW_PACE_FORMAT).innerHTML = " moves/" + Settings.PACE_PX_PER_NUMBER_SEC + "s";

        var interval_id = setInterval(function() {
            // Calculate pace and average pace:
            var distance = Math.floor(Math.sqrt(Math.pow(prevX - self.x, 2) + Math.pow(prevY - self.y, 2)));
            pace_rounds++;
            sum_pace += distance;
            average_pace = (sum_pace / pace_rounds);
            // End calcualte pace
            prevX = self.x;
            prevY = self.y;
            // update view
            paceSpan.innerHTML = Math.round(average_pace / Settings.PLAYER_MOVE_PHASE) + "";
        }, Settings.PACE_PX_PER_NUMBER_SEC * 1000);

        return interval_id;
    };

    return Player;

});
