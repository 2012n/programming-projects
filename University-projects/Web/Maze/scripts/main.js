/**
 * Created by nimrod on 10/4/15.
 * The main modules. the entry point.
 */


require(['jquery', 'Board', 'Player', 'Util', 'Settings'], function($, Board, Player, Util, Settings) {
    var canvas = document.getElementById("canvas");
    var pixelForCell = Settings.CANVAS_CELL_SIZE;
    var canvasWidth = Settings.CANVAS_WIDTH;
    var canvasHeight = Settings.CANVAS_HEIGHT;

    var focused = false;

    var b = new Board(canvasWidth / pixelForCell, canvasHeight / pixelForCell, pixelForCell);
    var p = new Player(Settings.PLAYER_SIZE, Settings.PLAYER_COLOR, b);

    // Initialize and run game.
    $.when(b.init()).then(b.generate()).then(p.movePlayer(Settings.PLAYER_START_X, Settings.PLAYER_START_Y));


    // --------------------


    /**
     * Handles a received message
     * @param event
     */

    var recieveMessage = function(event) {
        switch(parseInt(event.data)) {
            case Settings.MESSAGE_GAME_WON:
                clearInterval(timer_id); // stop timer
                clearInterval(pace_id); // clear pace counting.
                break;
            case Settings.MESSAGE_FIRST_BOARD_FOCUSED:
                if(!focused) {
                    pace_id = p.startCalculatePace();
                    // Set the timer
                    timer_id = Util.applyTimer();
                    Util.setStartTime();
                    focused = true;
                }
                break;
            default:
                break;
        }
    };

    var disableScrolling = function() {
        // set focused to start game:
        window.postMessage(Settings.MESSAGE_FIRST_BOARD_FOCUSED, "*");
        // go to canvas
        $("html, body").animate({ scrollTop: $("#info").offset().top }, 0);
        $("body").addClass("stop-scrolling");
    };

    var enableScrolling = function() {
        $("body").removeClass("stop-scrolling");
    }
    // listen
    window.addEventListener("message", recieveMessage, false);
    canvas.onfocus = disableScrolling;
    canvas.onblur = enableScrolling;

});


