<?php

include 'models/controller.php';

$controller = Controller::getInstance(); // allocation a new controller
$controller->createDb();// create a new db and tables if not exist

$action = @$_POST['action'];

if(!isset($action)) {
        $action = @$_GET['action'];
        if(!isset($action))
            $action = filter_input(INPUT_GET, "action"); //die("No action parameter sent!");
    }
switch($action) {
    case "register"://register a new account
        $controller->addUser();
        break;
    case "login"://login to an account
        $controller->login();
        break;
	case "logout":
		$controller->logout();
		break;
    case "saveGame"://save game information to db
        $controller->addDataToTblGame();
        break;
    case "getGames":
        $controller->getGames();
        break;
    case "updateParams":
        $controller->writeParamsFile();
        break;
    case "readParams":
        $controller->readParamsFile();
        break;
    case "tblGameToExcel":
        $controller->getExcelToDownload($controller->tblGame);
        break;
    default:
        echo "unknown request";
        break;
}

?>
