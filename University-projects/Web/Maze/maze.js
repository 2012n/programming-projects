/**
 * the Cell object.
*/

var Cell = function(x, y) {
    this.visited = false;
    this.walls = 0x1111; // Each bit specifies if walls [top, bottom, left, right] is broken or not. 1 - not, 0 - broken.
    this.x = x;
    this.y = y;
};

var Board = function Board(width, height) {
    this.width = width;
    this.height = height;
    this.cells = [];
};

// --------------------------

Cell.outOfBound = new Cell();
Cell.outOfBound.visited = true;

Cell.walls = {
    UP: 0x1000,
    DOWN: 0x0100,
    LEFT: 0x0010,
    RIGHT: 0x0001
};

Cell.prototype.textToHex = function(wall) {
    switch (wall) {
        case "UP": return Cell.walls.UP;
        case "DOWN": return Cell.walls.DOWN;
        case "LEFT": return Cells.walls.LEFT;
        case "RIGHT": return Cells.walls.RIGHT;
    }
};

// --------------------------

/**
 * Initializes the board. create the cells.
 */

Board.prototype.init = function() {
    for(var y = 0; y < this.height; y++) {
        for(var x = 0; x < this.width; x++)
            this.cells.push(new Cell(x, y));
    }
};

/**
 * Returns the cell at a coordinates.
 * @param x - x coordinate.
 * @param y - y coordinate.
 * @returns {Cell} the cell.
 */

Board.prototype.getCell = function(x, y) {
    return this.cells[y * this.width + x];
};

/**
 * Removes a wall.
 * @param x,y - the cell
 * @param wall - the wall to be removed. can be: Cell.walls.UP, Cell.walls.DOWN, Cell.walls.LEFT, Cell.walls.RIGHT.
 */

Board.prototype.removeWall = function(x, y, wall) {
    var cell = this.getCell(x, y);
    cell.walls ^= wall;
};

Board.prototype.getNeigbors = function(x, y) {
    return {
        UP:   (y > 0 ? this.getCell(x, y - 1) : Cell.outOfBound),
        DOWN: (y < this.height - 1 ? this.getCell(x, y + 1) : Cell.outOfBound),
        LEFT: (x > 0 ? this.getCell(x - 1, y) : Cell.outOfBound),
        RIGHT:(x < this.width - 1 ? this.getCell(x + 1, y) : Cell.outOfBound)
    };
};

Board.prototype.countUnvisitedNeigbords = function(x, y) {
    var neig = this.getNeigbors(x, y);
    return (neig.UP.visited ? 0 : 1 + neig.DOWN.visited ? 0 : 1 + neig.LEFT.visited ? 0 : 1 + neig.RIGHT.visited ? 0 : 1);
};
/**
 * Generates a board.
 */

Board.prototype.generate = function() {
    var self = this;
    var stack = [];
    var randOpt = ["UP", "DOWN", "LEFT", "RIGHT"];
    var rand;
    var cell;
    var neig;
    var next;
    var unvisitedNeigs = [];

    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    var eachX = canvas.width / this.width;
    var eachY = canvas.height / this.height;
    // Draw the full canvas first:
    for(var y = 0; y < this.height; y++) {
        for(var x = 0; x < this.width; x++) {
            ctx.beginPath();
            ctx.lineWidth = "2";
            ctx.strokeStyle = "black";
            ctx.rect(x * eachX, y * eachY, eachX, eachY);
            ctx.stroke();
        }
    }

    // Now that we have a drawn plate, just remove the walls:
    ctx.strokeStyle = "white";
    // create the entry:
    ctx.moveTo(0, 0);
    ctx.lineTo(0, eachY);
    ctx.stroke();

    var drawPath = function(x, y) {
        // current cell - mark as visit
        cell = self.getCell(x, y);
        cell.visited = true;

        neig = self.getNeigbors(x, y);
        // drop the unvisited neigs
        unvisitedNeigs = [];
        var i = 0;
        if(!neig["UP"].visited) unvisitedNeigs[i++] = ({"obj": neig["UP"], "wall": Cell.walls.UP});
        if(!neig["DOWN"].visited) unvisitedNeigs[i++] = ({"obj": neig["DOWN"], "wall": Cell.walls.DOWN});
        if(!neig["LEFT"].visited) unvisitedNeigs[i++] = ({"obj": neig["LEFT"], "wall": Cell.walls.LEFT});
        if(!neig["RIGHT"].visited) unvisitedNeigs[i++] = ({"obj": neig["RIGHT"], "wall": Cell.walls.RIGHT});

        if(unvisitedNeigs.length != 0) {
            var nextBuild = unvisitedNeigs[Math.floor(Math.random() * (unvisitedNeigs.length)) + 0];
            var next = nextBuild.obj;
            var wallToBreak = nextBuild.wall;

            stack.push(cell);
            self.removeWall(x, y, wallToBreak);

            // remove wall from canvas
            ctx.moveTo(x * eachX, y * eachY);
            switch(wallToBreak) {
                case Cell.walls.UP:
                    ctx.lineTo(x * eachX + eachX, y * eachY);
                    break;
                case Cell.walls.DOWN:
                    ctx.moveTo(x * eachX, y * eachY + eachY);
                    ctx.lineTo(x * eachX + eachX, y * eachY + eachY)
                    break;
                case Cell.walls.LEFT:
                    ctx.lineTo(x * eachX, y * eachY + eachY);
                    break;
                case Cell.walls.RIGHT:
                    ctx.moveTo(x * eachX + eachX, y * eachY);
                    ctx.lineTo(x * eachX + eachX, y * eachY + eachY);
                    break;

            }


            ctx.stroke(); // do the draw
            // end remove wall from canvas

            return drawPath(next.x, next.y);
        }

        else { // no neighbords!!
            if(stack.length == 0) {
                return true;
            } else {
                var back = stack.pop();
                return drawPath(back.x, back.y);
            }
        }
    };

    // start point
    drawPath(0, 0);



};
