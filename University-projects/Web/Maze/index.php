<?php
    $pageParam = @$_GET['page'];

    $cookie_Type = @$_COOKIE['accountType'];
    $msg = @$_GET['msg'];
    $msgType = @$_GET['msgType'];
    $cookie_id = @$_COOKIE['Player_id'];

    $pages = array("index");
    $msgTypes = array("info", "warning", "success", "danger");

    if(!isset($msg))
        $msg = "";
    if(!isset($msgType) || !in_array($msgType, $msgTypes))
        $msgType = "info";


    // Login

    if(!isset($cookie_Type)) { // No login cookie => a guest.
        $cookie_Type = "guest";
        $cookie_id = "guest";
		// Add the option to register.
		array_push($pages, "login", "register"); 
    } 

    // check privileges.
    switch($cookie_Type) {
        case "guest": // it's a guest. goto login anyway.
            if($pageParam != "register") {
                $pageParam = "login";
                $msg = "Please login to continue. if you are not registered yet, please register.";
                $msgType = "info";
            }
        break;

		case "professor":
			array_push($pages, "scientist");
		break;
    }

    if(!isset($pageParam) || !in_array($pageParam, $pages))
        $pageParam = "index";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Maze</title>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="style/scientist.css" />
    <link rel="stylesheet" type="text/css" href="style/register.css" />
    <link rel="stylesheet" href="style/maze_page/style.css">
</head>
<body  class="page1">
<header>
  <div class="container_12">
    <div class="grid_12" style="padding-top: 20px;">
        <div style="font-weight: bold; color: black; font-size: 36px;">
            Maze

            <?php
                if($cookie_id)
            ?>
              <span style="float: right; font-size: 16px">hello <?= $cookie_id ?> (<small><a href="?action=logout">logout</a></small>)</span>


        </div>
      <div class="clear"></div>
      <div class="menu_block">
        <nav>
          <ul class="sf-menu">
              <?php
                foreach($pages as $page) {
                    $current = "";
                    if($page == $pageParam)
                        $current = " class='current' ";
                ?>
              <li<?= $current ?>><a href='?page=<?= $page ?>'><?= $page ?></a></li>
                <?php } ?>
          </ul>
            <script>
                function F(){
                    $.ajax({
                          type: "post",
                          url: "server.php",
                          data: {action: "ifScientist"},
                          success: function(data) {
                                    alert("OK");
                            },
                                 async: false
                             });
                  }
            </script>
        </nav>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</header>

<div class="content">
    <div class="container_12">
        <div class="grid_9" align="center">
        <?php if($msg != "") { ?>
            <div class="alert alert-<?= $msgType ?>"><?= $msg ?></div>
        <?php }
            require_once("pages/". $pageParam . ".html");
        ?>
        </div>
    </div>
</div>

<footer>
  <div class="container_12">
    <div class="grid_12">
      <div class="socials"> <a href="#"></a> <a href="#"></a> <a href="#"></a> <a href="#"></a> </div>
      <div class="copy">Nimrod Wagner &amps Hed Bisker &copy; 2015</div>
    </div>
  </div>
</footer>
<?php
// Load the relevant require JS
$requireScript = array("index" => "main", "scientist" => "Scientist", "login" => "", "register" => "");
if($requireScript[$pageParam] != "") {
    ?>
    <script data-main="scripts/<?= $requireScript[$pageParam] ?>" src="scripts/require.js"></script>
<?php } ?>
</body>
</html>