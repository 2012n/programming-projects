<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set('include_path', ini_get('include_path').';../Classes/');
/** PHPExcel */
include 'Classes/PHPExcel.php';
/** PHPExcel_Writer_Excel2007 */
include 'Classes/PHPExcel/Writer/Excel2007.php';

/**classes of the models*/
include 'models/game.php';
include 'models/player.php';
/**
 * Description of controller
 *
 * @author Hed
 */
/**
 * Class Controller
 * control about the request the response from the site
 */
class Controller {
    var $servername;
    var $username;
    var $password;
    var $dbName;
    var $tblGame;
    var $tblPlayer;
	var $conn;

    /**
     * Constructor for Controller class
     * @param string $servername - the name of the server
     * @param string $username - username to get in the server
     * @param string $password - password to get in the server
     * @param string $dbName - name of the db
     * @param string $tblGame - name of the first table
     * @param string $tblPlayer - name of the second table
     */
    
    private static $instance = null;
     public static function getInstance(){
                    if(Controller::$instance == null){
                            Controller::$instance = new Controller();
                    }
                    return Controller::$instance;
         }
                
    private function Controller($servername = "localhost", $username = "root", $password = "", $dbName = "mazedb", $tblGame = "Game", $tblPlayer = "Player")
    {
        $this->servername = $servername;
        $this->username = $username;
        $this->password = $password;
        $this->dbName = $dbName;
        $this->tblGame = $tblGame;
        $this->tblPlayer = $tblPlayer;
		
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbName);
    }


    /**
     * create a new bd if not existed yet
     */
    public function createDb() {
        if (!$this->conn->select_db($this->dbName)) {
            $this->conn->query('CREATE DATABASE '. $this->dbName);
            $this->conn->select_db($this->dbName);
            $this->createTables();
        }
    }

    /**
     * create the tables of not existed yet
     */
    private function createTables() {
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sqlArr = array(
            "CREATE TABLE ".$this->tblGame." (
            Game_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            Player_id VARCHAR(10) NOT NULL,
            zigzug FLOAT(30) NOT NULL,
            complete_time VARCHAR(8) NOT NULL,
            average_speed FLOAT(30) NOT NULL,
            num_edge_hitting INT(50)  NOT NULL,
            path TEXT  NOT NULL,
            game_start_time TEXT NOT NULL
            )",
            "CREATE TABLE ".$this->tblPlayer." (
            Player_id VARCHAR(10)PRIMARY KEY,
            name VARCHAR(30) NOT NULL,
            password VARCHAR(30) NOT NULL
            )");

        if ($this->conn->query($sqlArr[0]) === TRUE) {
            echo "Table MyGuests created successfully";
        } else {
            echo "Error creating table: " . $this->conn->error;
        }
        if ($this->conn->query($sqlArr[1]) === TRUE) {
            echo "Table MyGuests created successfully";
        } else {
            echo "Error creating table: " . $this->conn->error;
        }
    }

    /**
     * add new username to db in Player table
     */
    public function addUser() {
        $Player_id = $_POST["Player_id"];
        $name = $_POST["name"];
        $password = $_POST["password"];
        if($this->userExist() == false) {
            if (!$this->conn) {
                die("Connection failed: " . mysqli_connect_error());
            }
            $sql = "INSERT INTO ".$this->tblPlayer." VALUES ('".$Player_id."', '".$name."', '".$password."');";
            $this->conn->query($sql);
            echo 'Registering successfully';
        }
        else
            echo "User already exist";
    }
    
    /**
     * check if the user exist
     * @return bool - return true if user is exist, else return false if user not exist
     */
    private function userExist(){
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "SELECT * FROM `".$this->tblPlayer.
            "` WHERE Player_id='".$_POST["Player_id"]."'";
	
        $result = $this->conn->query($sql) or die($this->conn->error. "<br />". $sql);
        if (mysqli_num_rows($result) > 0)
            return true;
        else
            return false;
    }

    /**
     * login to the username
     */
    public function login(){
       echo $_POST['accountType']."    ";
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        
        $userPass = $_POST["password"];
        $sql = "SELECT * FROM ".$this->tblPlayer.
            " WHERE `Player_id`='".$_POST["Player_id"]."' AND `password`='".$userPass."';";
        //echo $sql."     ";
        $result = $this->conn->query($sql);
        
       // echo mysqli_num_rows($result)."    ";
        
        if (mysqli_num_rows($result) > 0) 
        {
            $row = $result->fetch_assoc();
            setcookie("Player_id", $row["Player_id"], time() + 10000000, "/");
            setcookie("accountType", $_POST["accountType"], time() + 10000000, "/");
           //echo $this->password;
            if($userPass == ""){ //normal user
                header('Location: index.php?page=index');
            }
            else{
                
                header('Location: index.php?page=scientist');
            }
            exit();
        }
        else {
                header("Location: index.php?page=login&msg=cant login; bad username or password.&msgType=danger");
        }
    }
	
	/**
	* Removes the login cookie, if exists.
	**/
	
	public function logout() {
		$player_id = @$_COOKIE['Player_id'];
		$accountType = @$_COOKIE['accountType'];
		
		if (isset($player_id) && isset($accountType)) {		
			setcookie("Player_id", FALSE, 1, "/");
			setcookie("accountType", FALSE, 1, "/");				
		
			header("Location: index.php?page=login&message=please login.&msgType=danger");
		}
	
	}
	
    /**
     * write the last game information to the game table
     */
    public function addDataToTblGame(){
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "SELECT MAX(Game_id) AS maxId FROM ".$this->tblGame;
        $result = mysqli_query($this->conn,$sql) or die($this->conn->error. "--> ". $this->tblPlayer);
        $row = $result->fetch_assoc();
        $maxId = $row['maxId'] + 1;
       
        $sql = "INSERT INTO ".$this->tblGame." VALUES ('".$maxId."', '".$_COOKIE["Player_id"]."', '".
            $_POST["zigzug"]."', '".$_POST["complete_time"]."', '".$_POST["average_speed"]."', '".
                $_POST["num_edge_hitting"]."','".$_POST['path']."','".$_POST['start_time']."');";
        
        /*
        setcookie("Game_id",$maxId,time()+10000000);
        setcookie("Game_id",$maxId,time()+10000000);
        setcookie("zigzug",$_POST["zigzug"],time()+10000000);
        setcookie("complete_time",$_POST["complete_time"],time()+10000000);
        setcookie("average_speed",$_POST["average_speed"],time()+10000000);
        setcookie("num_edge_hitting",$_POST["num_edge_hitting"],time()+10000000);
         */
        
        $this->conn->query($sql);
    }

    /**
     * Write all games information to excel file
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function writeAllGamesInformationToExcel(){
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "SELECT * FROM ".$this->tblGame ;
        $result = $this->conn->query($sql);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        for ($i = 0; $i < $result->num_rows; $i++) {
            $row = $result->fetch_assoc();
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.($i+1), $row["Player_id"]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.($i+1), $row["Game_id"]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.($i+1), $row["zigzug"]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.($i+1), $row["complete_time"]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.($i+1), $row["average_speed"]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.($i+1), $row["num_edge_hitting"]);
            //$GameDataArray[$i] = new Game($row["Game_id"],$row["Player_id"],$row["zigzug"],$row["complete_time"],$row["average_speed"],$row["num_edge_hitting"]);
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        /**
         * need to Export to Excel
         */
        $this->conn->query($sql);
    }

/*
    public function generateParamsFile(){
        $fp = fopen('param.json', 'w');
        $response = array("X" => "Y","A" => "B");
        fwrite($fp, json_encode($response));
        fclose($fp);
    }
*/
    
    public function writeParamsFile(){
        $string = file_get_contents('param.json');
        $oldParam = json_decode($string, true);

      $response = array("CANVAS_WIDTH" => Isset($_POST["CANVAS_WIDTH"]) ? $_POST["CANVAS_WIDTH"] : $oldParam["CANVAS_WIDTH"],//int
            "CANVAS_HEIGHT" => Isset($_POST["CANVAS_HEIGHT"]) ? $_POST["CANVAS_HEIGHT"] : $oldParam["CANVAS_HEIGHT"],//int
            "CANVAS_CELL_SIZE" => Isset($_POST["CANVAS_CELL_SIZE"]) ? $_POST["CANVAS_CELL_SIZE"] : $oldParam["CANVAS_CELL_SIZE"],//int
            "MAZE_WAY_COLOR" => Isset($_POST["MAZE_WAY_COLOR"]) ? $_POST["MAZE_WAY_COLOR"] : $oldParam["MAZE_WAY_COLOR"],//string
            "MAZE_WALL_COLOR" => Isset($_POST["MAZE_WALL_COLOR"])?$_POST["MAZE_WALL_COLOR"] : $oldParam["MAZE_WALL_COLOR"],//string
            "PLAYER_MOVE_PHASE" => Isset($_POST["PLAYER_MOVE_PHASE"])? $_POST["PLAYER_MOVE_PHASE"] : $oldParam["PLAYER_MOVE_PHASE"],//int
            "PLAYER_SIZE" => Isset($_POST["PLAYER_SIZE"])? $_POST["PLAYER_SIZE"] : $oldParam["PLAYER_SIZE"],//int
            "PLAYER_COLOR" => Isset($_POST["PLAYER_COLOR"])? $_POST["PLAYER_COLOR"] : $oldParam["PLAYER_COLOR"],//int
            "PLAYER_START_X" => Isset($_POST["PLAYER_START_X"])? $_POST["PLAYER_START_X"] : $oldParam["PLAYER_START_X"],//float
            "PLAYER_START_Y" => Isset($_POST["PLAYER_START_Y"])? $_POST["PLAYER_START_Y"] : $oldParam["PLAYER_START_Y"],//float
            "ARROW_UP" => Isset($_POST["ARROW_UP"])? $_POST["ARROW_UP"] : $oldParam["ARROW_UP"],//char
            "ARROW_DOWN" => Isset($_POST["ARROW_DOWN"])? $_POST["ARROW_DOWN"] : $oldParam["ARROW_DOWN"],//char
            "ARROW_LEFT" => Isset($_POST["ARROW_LEFT"])? $_POST["ARROW_LEFT"] : $oldParam["ARROW_LEFT"],//char
            "ARROW_RIGHT" => Isset($_POST["ARROW_RIGHT"])? $_POST["ARROW_RIGHT"] : $oldParam["ARROW_RIGHT"],//char
            "PACE_PX_PER_NUMBER_SEC" => Isset($_POST["PACE_PX_PER_NUMBER_SEC"])? $_POST["PACE_PX_PER_NUMBER_SEC"] : $oldParam["PACE_PX_PER_NUMBER_SEC"]//int
        );
        if(!file_put_contents('param.json',json_encode($response)))
            echo "Error updating param files! changes not written.";
        else
            echo "OK";
    }

    public function readParamsFile(){
        $string = file_get_contents('param.json');
        if(!$string)
            echo "BAD";
        else
            echo $string;
    }

    public function getGames(){
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $sql = "SELECT * FROM ".$this->tblGame ;
        $result = $this->conn->query($sql);
        $gameArray = array();
        for ($i = 0; $i < $result->num_rows; $i++) {
            $row = $result->fetch_assoc();
            $gameArray[$i] = new Game($row["Game_id"],$row["Player_id"],$row["zigzug"],$row["complete_time"],$row["average_speed"],$row["num_edge_hitting"]);
        }
        echo json_encode($gameArray);
    }

    public function getExcelToDownload($table){
        function cleanData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
        }
        //make the output to excel download
        $filename = "website_data_" . date('Ymd') . ".xls";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        $flag = false;
        $this->conn = mysqli_connect($this->servername, $this->username, $this->password, $this->dbName);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }     
        $result = $this->conn->query("SELECT * FROM ".$table) or die('Query failed!');
        
        
         for ($i = 0; $i < $result->num_rows; $i++) {
            $row = $result->fetch_assoc();
            if(!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, 'cleanData');
            echo implode("\t", array_values($row)) . "\r\n";
        }                   
         exit;
    }
    
} // end of class Controller