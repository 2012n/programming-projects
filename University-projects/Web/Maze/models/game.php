<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of game
 *
 * @author Hed
 */
/**
 * Class Game
 * About the Game table details
 */
class Game{
    var $Game_id;
    var $Player_id;
    var $zigzug;
    var $complete_time;
    var $average_speed;
    var $num_edge_hitting;
    function Game($Game_id, $Player_id, $zigzug, $complete_time, $average_speed, $num_edge_hitting){
        $this->Game_id = $Game_id;
        $this->Player_id = $Player_id;
        $this->zigzug = $zigzug;
        $this->complete_time = $complete_time;
        $this->average_speed = $average_speed;
        $this->num_edge_hitting = $num_edge_hitting;
    }
}