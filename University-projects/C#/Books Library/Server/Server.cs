﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Server.databasesDataSetTableAdapters;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Data;
using RequestForm;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Server
{
    class Server
    {
        // CONSTRANCTS
        private readonly char[] COMMAND_SEPERATE = { '|' };
        private readonly byte[] GOOD_ACT = Encoding.ASCII.GetBytes("1"),
                                BAD_ACT = Encoding.ASCII.GetBytes("2"),
                                NONE = Encoding.ASCII.GetBytes("0"),
                                BAD_LOGIN = Encoding.ASCII.GetBytes("0"),
                                GOOD_LOGIN = Encoding.ASCII.GetBytes("1"),
                                ALREADY_ON = Encoding.ASCII.GetBytes("2");
        private readonly int SUCCESS = 1, REACHED_MAX_ALLOWED = 4;

        // Accessories
        private String service;
        private delegate void StartServiceDelegate(Object socket);
        // Table adapters
        private ReadersTableAdapter readerAccount = new ReadersTableAdapter();
        private BooksTableAdapter bookAdapter = new BooksTableAdapter();
        private CurrentBorrowingsTableAdapter currentBorrows = new CurrentBorrowingsTableAdapter();
        private pastBorrowingsTableAdapter pastBorrows = new pastBorrowingsTableAdapter();
        // Network Classes 
        private Socket socket;
        private IPAddress address = IPAddress.Any;
        private IPEndPoint endPoint;
        private TcpListener listener;
        private BinaryFormatter formatter = new BinaryFormatter();
        // info
        private LinkedList<int> connected_id = new LinkedList<int>();
        
        /// <summary>
        /// Starting up the server
        /// </summary>

        static void Main(string[] args)
        {
            Server server = new Server();
            Thread s = new Thread(new ThreadStart(server.runServer));
            s.Start();
        }

        /// <summary>
        /// Deserializing an Request object from stream
        /// </summary>
        /// <param name="stream">The stream to work with</param>
        /// <returns>The request or null on failure</returns>

        public Request getRequest(NetworkStream stream)
        {
            Request r;
            try
            {
                r = (Request)formatter.Deserialize(stream);
            }
            catch (Exception)
            {
                Console.WriteLine("IO Error! disconnection this socket!");
                r = null;
            }

            return r;
        }

        /// <summary>
        /// Running the server: listening to client,
        /// opening a new service thread to each client.
        /// </summary>
        
        public void runServer()
        {
            try
            {
                endPoint = new IPEndPoint(address, 5000);
                listener = new TcpListener(endPoint);
                // Start listening ..
                listener.Start();
                Console.WriteLine("Server is on.");
                while (true)
                {   // Running the server
                    try
                    {
                        socket = listener.AcceptSocket();
                        // New client connected .. open a new thread
                        Thread thread = new Thread(new ParameterizedThreadStart(startService));
                        thread.Start(socket);
                    }
                    catch (Exception ex)
                    {   // An error occurred with this socket:
                        Console.WriteLine("Error! " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {   // Listening to connection failed. can't continue.
                Console.WriteLine("Cannot listen: " + ex.Message);
                return;
            }
        }

        /// <summary>
        /// service a client 
        /// </summary>
        /// <param name="sParam">
        /// Client socket.
        /// recieve as Object because of a ParameterizedThreadStart settings.
        /// </param>

        public void startService(Object sParam)
        {
            // Save data
            Socket s = sParam as Socket;
            if (s == null) return; // bad socket
            NetworkStream myStream = new NetworkStream(s);
            Request myRequest;
 
            while (s.Connected)
            {
                myRequest = getRequest(myStream); // get request
                if (myRequest == null)
                {
                    // Couldn't get the request! 
                    closeConnection(new Request(), s);
                    break;
                }

                // choose service .....
                handleService(myRequest, myStream, s); // start service..
                Console.WriteLine("{0}: Command >> {1}",myRequest.userID, myRequest.command);
            }
        }

       /// <summary>
        /// Referring to the desired service.
       /// </summary>
       /// <param name="request">Request object</param>
       /// <param name="stream">The stream of this client</param>
       /// <param name="s">The socket of this client</param>

        void handleService(Request request, NetworkStream stream, Socket s)
        {
            service = request.command;

            switch (service)
            {   // Reffer to  a handling function by `command` content.
                case "login":   // Login request
                    login(request, stream);
                    break;
                case "getbooks": // Get whole book list request.
                    searchBooks(stream);
                    break;
                case "searchbybook":    // Search a books by `Title`.
                    searchByBook(request, stream);
                    break;
                case "searchbyauthor":  // Search a books by `Author`.
                    searchByAuthor(request, stream);
                    break;
                case "goodbye": // Close connection with socket `s`.
                    closeConnection(request, s);
                    break;
                case "borrowbook":  // Borrow a book(s).
                    borrowBook(request, stream);
                    break;
                case "myborrows":   // Gets a list of book(s) borrowed by `request.userID`.
                    myborrows(request, stream);
                    break;
                case "returnborrow":    // Returning a book(s)
                    returnBook(request, stream);
                    break;
                case "passedborrows":   // Gets a list of book(s) that's ever been borrowed by `request.UserID`.
                    getPassedBorrowing(request, stream);
                    break;
                default:    // Unsupported request.
                    try { stream.Write(Encoding.ASCII.GetBytes("Unknown command!"), 0, "Unknown command!".Length); }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                    break;
            }
        }

        /// <summary>
        /// Return a book list of books borrowed by reader id 
        /// </summary>
        /// <param name="request">A Request Object</param>
        /// <param name="stream">The  client's stream</param>
        
        private void myborrows(Request request, NetworkStream stream)
        {
            int id = request.userID;
            String data = "";
            var borrowings = currentBorrows.GetDataByBoworrerID(id);

            foreach (DataRow book in borrowings.Rows)
            {
                try
                {
                    data += String.Format("#{0} -- From {1} until {2}.|", // <-- show format
                        book["ISBN"], book["borrowDate"], book["returnDate"]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    break;
                }
            }

            // Return the data to the user
            if (data == "") data = "NONE";
            try { stream.Write(Encoding.ASCII.GetBytes(data), 0, data.Length); }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            // Done ..
        }

        /// <summary>
        /// Get list of book that borrowed by id.
        /// </summary>
        /// <param name="request">Reader id.</param>
        /// <param name="stream">Client stream</param>
        
        void getPassedBorrowing(Request request, NetworkStream stream)
        {
            String data = "";
            var passedBorrowing = pastBorrows.GetDataByReaderID(request.userID);

            foreach (DataRow book in passedBorrowing.Rows)
            {
                data += String.Format("#{0} -- From {1} until {2}.|", // <-- show format
                    book["ISBN"], book["borrowDate"], book["returnDate"]);
            }

            // return data to the user ..
            if (data == "") data = "NONE";
            try { stream.Write(Encoding.ASCII.GetBytes(data), 0, data.Length); }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        /// <summary>
        /// a log in method. searching for users which matchs @userID and @Name .
        /// </summary>
        /// <param name="request">Reader id.</param>
        /// <param name="stream">Client stream</param>
        
        void login(Request request, NetworkStream stream)
        {
            int id = request.userID;

            //  No such id
            if (readerAccount.authenticateLogin(id).Count == 0)
                try { stream.Write(BAD_LOGIN, 0, BAD_LOGIN.Length); }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            //  Already on
            else if (connected_id.Contains(id))
                try { stream.Write(ALREADY_ON, 0, ALREADY_ON.Length); }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
            //  Great .
            else
            {
                try { stream.Write(GOOD_LOGIN, 0, GOOD_LOGIN.Length); }
                catch (Exception) { return; }
                // Add this userID to logged on IDs
                connected_id.AddLast(id);
            }
        }
        
        /// <summary>
        /// Closing the connection with the client.
        /// </summary>
        /// <param name="request">A request containing userID</param>
        /// <param name="socket">The socket to disconnect with</param>
        
        void closeConnection(Request request, Socket socket)
        {
            // connection closed: handler
            try
            {
                socket.Shutdown(SocketShutdown.Both); // Shutdown activity with this client
                socket.Close(); // close the connection
                if (request != null) connected_id.Remove(request.userID);
            }
            catch (Exception)
            {
                Console.WriteLine("An error occurred while trying disconnect you.");
                return;
            }
         }

        /// <summary>
        /// Return a list with all books in the library.
        /// </summary>
        /// <param name="stream">The client's stream to write the books to.</param>
        
        void searchBooks(NetworkStream stream)
        {
            string text = "";
            databasesDataSet.BooksDataTable Books;
            Books = bookAdapter.GetData();

            // fetch the books 
            foreach (DataRow book in Books.Rows)
            {
                text += String.Format("#{0} -- {1} by {2}. {4} of {3} are borrowed.|", // <-- show format
                    book["ISBN"], book["Title"], book["Author"], book["Units"], book["lentUnits"]);
            }

            // Return the data to the user
            if (text == "") text = "NONE";
            try { stream.Write(Encoding.ASCII.GetBytes(text), 0, text.Length); }
            catch (Exception) { Console.WriteLine("Write error!"); return; }
        }

        /// <summary>
        /// Return a list of books with theirs name starts with `request.value` 
        /// </summary>
        /// <param name="request">A details of request</param>
        /// <param name="stream">Client's stream to write to.</param>

        void searchByBook(Request request, NetworkStream stream)
        {

            string text = "";
            var Books = bookAdapter.GetDataByBookName(request.value); // Get books

            // fetch the books 
            foreach (DataRow book in Books.Rows)
            {
                text += String.Format("#{0} -- {1} by {2}. {4} of {3} are borrowed.|", // <-- show format
                    book["ISBN"], book["Title"], book["Author"], book["Units"], book["lentUnits"]);
            }

            // Return the data to the user
            if (text == "") text = "NONE";
            try { stream.Write(Encoding.ASCII.GetBytes(text), 0, text.Length); }
            catch (Exception) { Console.WriteLine("Write error!"); return; }
        }

        /// <summary>
        /// Return a list of books with theirs author's name starts with `request.value` 
        /// </summary>
        /// <param name="request">A details of request</param>
        /// <param name="stream">Client's stream to write to.</param>
        
        void searchByAuthor(Request request, NetworkStream stream)
        {

            string text = "";
            var Books = bookAdapter.GetDataByAuthor(request.value);

            // fetch the books 
            foreach (DataRow book in Books.Rows)
            {
                text += String.Format("#{0} -- {1} by {2}. {4} of {3} are borrowed.|", // <-- show format
                    book["ISBN"], book["Title"], book["Author"], book["Units"], book["lentUnits"]);
            }

            // Return the data to the user
            if (text == "") text = "NONE";
            try { stream.Write(Encoding.ASCII.GetBytes(text), 0, text.Length); }
            catch (Exception) { Console.WriteLine("Write error!"); return; }
        }
        
        /// <summary>
        /// Returning a book from the user
        /// </summary>
        /// <param name="request">Reader id.</param>
        /// <param name="stream">Client stream</param>
        
        void returnBook(Request request, NetworkStream stream)
        {
            List<String> args = request.Arguments;
            int readerID = request.userID, bookID = 0;
            // set readerID
            DataRow theBook = null;
            databasesDataSet.CurrentBorrowingsDataTable Books;
            databasesDataSet.BooksDataTable selectedBook;

            foreach (String book in args)
            {
                theBook = null;
                bookID = int.Parse(book);
                Books = currentBorrows.isBorrowedByReaderID(bookID, readerID);
                if (Books.Count == 0) // this book wasn't borrowed by this reader.
                    continue;
                // Set the the previous
                theBook = Books.Rows[0];
                // Update passed borrows
                try
                {
                    pastBorrows.Insert(int.Parse(theBook["ISBN"].ToString()),
                                        readerID, theBook["borrowDate"].ToString(),
                                        theBook["returnDate"].ToString());
                }
                catch (Exception) { continue; }
                // decrease current num borrowed
                selectedBook = bookAdapter.GetBookByID(bookID);
                theBook = selectedBook[0]; // ID is unique so index 0..
                int lentBooks = int.Parse(theBook["lentUnits"].ToString());

                bookAdapter.UpdateBorrowedBook(lentBooks - 1, bookID);
                // remove from current
                currentBorrows.DeleteByID(int.Parse(theBook["ISBN"].ToString()),
                                          readerID);
            }
            // Ok ..
            try { stream.Write(GOOD_ACT, 0, GOOD_ACT.Length); }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        
        /// <summary>
        /// Borrow book
        /// </summary>
        /// <param name="request">Reader id.</param>
        /// <param name="stream">Client stream</param>
        
        void borrowBook(Request request, NetworkStream stream)
        {
            String msg = ""; // String with bad. if good, it'll be automaticly fixed.
            int readerID = request.userID, bookID;
            bool found = false;
            DataRow myBook = null;
            // Arguments
            List<String> arguments = request.Arguments;
            // Has this reader reached his max allowed borrowes?
            if (readerAccount.hasReachedMaxBorrows(readerID).Count != 0)
            {
                try
                {
                    stream.Write(Encoding.ASCII.GetBytes(REACHED_MAX_ALLOWED.ToString()), 0, REACHED_MAX_ALLOWED.ToString().Length);
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Could\'nt write to stream: " + ex.Message);
                }

               return; 
            }

            databasesDataSet.BooksDataTable Books;

            foreach (String node in arguments)
            {
                try
                {
                    found = false;
                    myBook = null;
                    bookID = int.Parse(node);
                    // Proccessing :
                    Console.WriteLine("Now: " + node);
                    // Are you already borrowing this book? 
                    if (currentBorrows.isBorrowedByReaderID(bookID, readerID).Count != 0) continue; /* Already borrowed .. */
                    Books = bookAdapter.GetBookByID(bookID);
                    
                    foreach (DataRow book in Books.Rows)
                    {
                        if (book["ISBN"].ToString() == bookID.ToString())
                        {
                            myBook = book;
                            found = true;
                            break;
                        }
                    }

                    if (!found) continue; // No such book 

                    if (myBook["lentUnits"].ToString() == myBook["Units"].ToString()) continue; // No books are left

                    // Everything's seems right
                    currentBorrows.addBorrow(bookID, readerID,
                        DateTime.Today.ToShortDateString(),
                        (DateTime.Today.AddMonths(1).ToShortDateString() + ", " + DateTime.Today.ToShortTimeString()));

                    int lentBooks = int.Parse(myBook["lentUnits"].ToString());
                    bookAdapter.UpdateBorrowedBook(lentBooks + 1, bookID);
                    msg += myBook["ISBN"].ToString() + "|"; // book #ISBBN went good.
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error! " + ex.Message);
                }
            }

            String toWrite;

            if (msg == "") toWrite = "NONE";
            else toWrite = SUCCESS + "|" + msg;
            try { stream.Write(Encoding.ASCII.GetBytes(toWrite), 0, toWrite.Length); }
            catch (Exception) { return; }
            foreach (String s in msg.Split(COMMAND_SEPERATE)) Console.Write("{0}, ", s);
            Console.WriteLine();

        }

        /// <summary>
        /// Kill server
        /// </summary>

        ~Server()
        {
            System.Environment.Exit(System.Environment.ExitCode);
        }
    }
}
