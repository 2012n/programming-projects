﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RequestForm
{
    /// <summary>
    /// Repesents a request 
    /// </summary>
    
    [Serializable]
    public class Request
    {
        // Properties
        public String command { get; set; }
        public int userID { get; set; }
        public List<String> Arguments = new List<string>();
        public String value { get; set; }
        // Request variables
    }
}
