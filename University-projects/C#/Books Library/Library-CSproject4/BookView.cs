﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Library_CSproject4;
using RequestForm;

namespace Library_CSproject4
{
    // Declere variables
    public delegate void networkDelegate(CheckedListBox list);
    public delegate void updateBooksBoxDelegate(CheckedListBox list, String text);
    public delegate void LiveUpdateDelegate();

    // Update external
    public partial class BookView : Form
    {
        private int userID;
        // Useful constants
        private static readonly char[] SEPERATOR = { '|' };
        private static readonly int REACHED_MAX_BORROW = 4;

        private Login parent;
        // Connection
        private Connection connection;
        // Frames 
        private UserBorrows borrowWin;
        private PassedBorrowing passedBorrowingWin;
        // Delegates
        public LiveUpdateDelegate updateCurrentBorrowing { get; private set; }
        public LiveUpdateDelegate updatePassedBorrowing  { get; private set; }
        // Hold the respond from the server
        String responde = "";

        /// <summary>
        /// The BookView class constructor.
        /// </summary>
        /// <param name="parent">The login frame who directed here. it should be closed , therefore a reference saved.</param>
        /// <param name="conn">The connection session reference.</param>
        /// <param name="userID">Current reader ID.</param>
        /// 
        public BookView(Login parent, Connection conn, int userID)
        {
            InitializeComponent();
            // Set custom title
            this.Text = "Book view form (UserID: " + userID + ")";
            // Initialize variables
            this.parent = parent;
            this.connection = conn;
            this.userID = userID;
            // Initialize forms ..
            passedBorrowingWin = new PassedBorrowing(generalUpdateBooksBox, connection, userID);
            borrowWin = new UserBorrows(generalUpdateBooksBox, connection, userID);
            // Set delegates for this frame
            updatePassedBorrowing = new LiveUpdateDelegate(passedBorrowingWin.externalUpdate);
            updateCurrentBorrowing = new LiveUpdateDelegate(borrowWin.externalUpdate);
            // Update passed borrwing delegates
            passedBorrowingWin.updateCurrentView = new LiveUpdateDelegate(borrowWin.externalUpdate);
            passedBorrowingWin.updateMainView = new LiveUpdateDelegate(updateBooksBox);
            // Update current borrowings delegates
            borrowWin.updatePassedView = new LiveUpdateDelegate(passedBorrowingWin.externalUpdate);
            borrowWin.updateMainView = new LiveUpdateDelegate(updateBooksBox);
        }

        /// <summary>
        /// Closing the parent form.
        /// </summary>
        
        private void BookView_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Remember to close the login parent form:
            parent.Close();
        }

        /// <summary>
        /// Loading available book as the form loads.
        /// </summary>

        private void BookView_Load(object sender, EventArgs e)
        {
            // Load books
            updateBooksBox();
        }

        /// <summary>
        /// Loads this book view content.
        /// </summary>

        public void updateBooksBox()
        {
            // Get available book list:
            try
            {
                // Send request
                connection.sendRequest(userID, "getbooks");
                // Request sent
                responde = connection.getRespond();
                // Add to view:
                generalUpdateBooksBox(checkedListBox_bookview, responde);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Loads an a general book view.
        /// </summary>
        /// <param name="list">The list container.</param>
        /// <param name="booksList">Book to load.</param>

        public void generalUpdateBooksBox(CheckedListBox list, String booksList)
        {
            // Clear previous text
            list.Items.Clear();
            // Get available book list:
            try
            {
                // Add to view:
                string[] books = booksList.Split(SEPERATOR);
                foreach (string book in books)
                {
                    if (book != "") list.Items.Add(book);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// A specific updater, especially for searching.
        /// </summary>
        /// <param name="param">Argument to search for.</param>

        public void updateBooksBox(String Command, String Value)
        {
            try
            {
                // Request book list
                connection.sendRequest(userID, Command, Value);
                // Get respond
                responde = connection.getRespond();
                if (responde == "NONE") return; // do nothing - there's no new
                // Add to view:
                generalUpdateBooksBox(checkedListBox_bookview, responde);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Handling a borrowing request.
        /// </summary>

        private void button_borrow_Click(object sender, EventArgs e)
        {
            // Check wheter some books checked.
            var selectedItems = checkedListBox_bookview.CheckedItems;
            if (selectedItems.Count == 0) /* Nothing selected */ return;
            Regex filter = new Regex(@"([0-9]{0,4})"); // Get the IDs for wanted books 
            Match m;
            List<String> booksIDs = new List<String>();

            // Add books to 
            foreach (var item in selectedItems)
            {   // Filter in IDs
                m = filter.Match(item.ToString());
                if (m.Success) booksIDs.Add(m.NextMatch().Value);
            }

            try
            {
                connection.sendRequest(userID, "borrowbook", booksIDs);
                responde = connection.getRespond(); // Get result
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // If you can't borrows ..
            if (responde == REACHED_MAX_BORROW.ToString())
            {
                MessageBox.Show("You can't borrow anymore. You\'ve reached max allowed borrowing");
            }
            else
            {
                // Update ..
                updateBooksBox();
                // Update current borrowings
                updateCurrentBorrowing();
            }
        }

        /// <summary>
        /// Opens the borrow frame.
        /// </summary>

        private void youreBorrowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            borrowWin.updateList();
            borrowWin.Show();
        }

        /// <summary>
        ///  Open the passed borrowing book
        /// </summary>

        private void passedBorrowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            passedBorrowingWin.updateList();
            passedBorrowingWin.Show();
        }

        /// <summary>
        /// Search for book by its name as the reader typing
        /// </summary>

        private void textBox_searchbyname_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Alt || e.KeyCode == Keys.Control || e.KeyCode == Keys.ShiftKey) return;
            updateBooksBox("searchbybook", textBox_searchbyname.Text);
        }

        /// <summary>
        /// Search for book by its author's name as the reader typing
        /// </summary>

        private void textBox_searchbyauthor_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Alt || e.KeyCode == Keys.Control || e.KeyCode == Keys.ShiftKey) return;
            updateBooksBox("searchbyauthor", textBox_searchbyauthor.Text);
        }
    }
}
