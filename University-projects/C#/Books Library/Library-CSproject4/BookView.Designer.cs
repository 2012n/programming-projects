﻿namespace Library_CSproject4
{
    partial class BookView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookView));
            this.label_books = new System.Windows.Forms.Label();
            this.label_description = new System.Windows.Forms.Label();
            this.checkedListBox_bookview = new System.Windows.Forms.CheckedListBox();
            this.label_search = new System.Windows.Forms.Label();
            this.textBox_searchbyname = new System.Windows.Forms.TextBox();
            this.label_searchbyname = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_searchbyauthor = new System.Windows.Forms.TextBox();
            this.label_searchbyauthor = new System.Windows.Forms.Label();
            this.button_borrow = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.youreBorrowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.passedBorrowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_books
            // 
            this.label_books.AutoSize = true;
            this.label_books.BackColor = System.Drawing.Color.Transparent;
            this.label_books.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_books.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label_books.Location = new System.Drawing.Point(23, 30);
            this.label_books.Name = "label_books";
            this.label_books.Size = new System.Drawing.Size(84, 25);
            this.label_books.TabIndex = 1;
            this.label_books.Text = "Books.";
            // 
            // label_description
            // 
            this.label_description.AutoSize = true;
            this.label_description.BackColor = System.Drawing.Color.Transparent;
            this.label_description.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label_description.Location = new System.Drawing.Point(102, 41);
            this.label_description.Name = "label_description";
            this.label_description.Size = new System.Drawing.Size(237, 13);
            this.label_description.TabIndex = 2;
            this.label_description.Text = "borrow and search for books you may would like.";
            // 
            // checkedListBox_bookview
            // 
            this.checkedListBox_bookview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListBox_bookview.ForeColor = System.Drawing.SystemColors.WindowText;
            this.checkedListBox_bookview.FormattingEnabled = true;
            this.checkedListBox_bookview.Location = new System.Drawing.Point(28, 58);
            this.checkedListBox_bookview.Name = "checkedListBox_bookview";
            this.checkedListBox_bookview.Size = new System.Drawing.Size(803, 272);
            this.checkedListBox_bookview.TabIndex = 0;
            // 
            // label_search
            // 
            this.label_search.AutoSize = true;
            this.label_search.BackColor = System.Drawing.Color.Transparent;
            this.label_search.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_search.ForeColor = System.Drawing.Color.Blue;
            this.label_search.Location = new System.Drawing.Point(28, 337);
            this.label_search.Name = "label_search";
            this.label_search.Size = new System.Drawing.Size(61, 20);
            this.label_search.TabIndex = 3;
            this.label_search.Text = "Search.";
            // 
            // textBox_searchbyname
            // 
            this.textBox_searchbyname.Location = new System.Drawing.Point(96, 336);
            this.textBox_searchbyname.Name = "textBox_searchbyname";
            this.textBox_searchbyname.Size = new System.Drawing.Size(190, 20);
            this.textBox_searchbyname.TabIndex = 4;
            this.textBox_searchbyname.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox_searchbyname_KeyUp);
            // 
            // label_searchbyname
            // 
            this.label_searchbyname.AutoSize = true;
            this.label_searchbyname.BackColor = System.Drawing.Color.Transparent;
            this.label_searchbyname.Location = new System.Drawing.Point(96, 356);
            this.label_searchbyname.Name = "label_searchbyname";
            this.label_searchbyname.Size = new System.Drawing.Size(61, 13);
            this.label_searchbyname.TabIndex = 5;
            this.label_searchbyname.Text = "Book name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(290, 334);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Or";
            // 
            // textBox_searchbyauthor
            // 
            this.textBox_searchbyauthor.Location = new System.Drawing.Point(330, 336);
            this.textBox_searchbyauthor.Name = "textBox_searchbyauthor";
            this.textBox_searchbyauthor.Size = new System.Drawing.Size(194, 20);
            this.textBox_searchbyauthor.TabIndex = 7;
            this.textBox_searchbyauthor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox_searchbyauthor_KeyUp);
            // 
            // label_searchbyauthor
            // 
            this.label_searchbyauthor.AutoSize = true;
            this.label_searchbyauthor.BackColor = System.Drawing.Color.Transparent;
            this.label_searchbyauthor.Location = new System.Drawing.Point(330, 357);
            this.label_searchbyauthor.Name = "label_searchbyauthor";
            this.label_searchbyauthor.Size = new System.Drawing.Size(101, 13);
            this.label_searchbyauthor.TabIndex = 8;
            this.label_searchbyauthor.Text = "Book\'s author name";
            // 
            // button_borrow
            // 
            this.button_borrow.Location = new System.Drawing.Point(546, 337);
            this.button_borrow.Name = "button_borrow";
            this.button_borrow.Size = new System.Drawing.Size(174, 23);
            this.button_borrow.TabIndex = 9;
            this.button_borrow.Text = "Borrow these books";
            this.button_borrow.UseVisualStyleBackColor = true;
            this.button_borrow.Click += new System.EventHandler(this.button_borrow_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(878, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.youreBorrowsToolStripMenuItem,
            this.passedBorrowsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // youreBorrowsToolStripMenuItem
            // 
            this.youreBorrowsToolStripMenuItem.Name = "youreBorrowsToolStripMenuItem";
            this.youreBorrowsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.youreBorrowsToolStripMenuItem.Text = "Your borrows";
            this.youreBorrowsToolStripMenuItem.Click += new System.EventHandler(this.youreBorrowsToolStripMenuItem_Click);
            // 
            // passedBorrowsToolStripMenuItem
            // 
            this.passedBorrowsToolStripMenuItem.Name = "passedBorrowsToolStripMenuItem";
            this.passedBorrowsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.passedBorrowsToolStripMenuItem.Text = "Passed Borrows";
            this.passedBorrowsToolStripMenuItem.Click += new System.EventHandler(this.passedBorrowsToolStripMenuItem_Click);
            // 
            // BookView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(878, 373);
            this.Controls.Add(this.button_borrow);
            this.Controls.Add(this.label_searchbyauthor);
            this.Controls.Add(this.textBox_searchbyauthor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_searchbyname);
            this.Controls.Add(this.textBox_searchbyname);
            this.Controls.Add(this.label_search);
            this.Controls.Add(this.label_description);
            this.Controls.Add(this.label_books);
            this.Controls.Add(this.checkedListBox_bookview);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "BookView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BookView";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BookView_FormClosed);
            this.Load += new System.EventHandler(this.BookView_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_books;
        private System.Windows.Forms.Label label_description;
        private System.Windows.Forms.CheckedListBox checkedListBox_bookview;
        private System.Windows.Forms.Label label_search;
        private System.Windows.Forms.TextBox textBox_searchbyname;
        private System.Windows.Forms.Label label_searchbyname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_searchbyauthor;
        private System.Windows.Forms.Label label_searchbyauthor;
        private System.Windows.Forms.Button button_borrow;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem youreBorrowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem passedBorrowsToolStripMenuItem;

    }
}