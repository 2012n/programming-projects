﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using RequestForm;

namespace Library_CSproject4
{
    public partial class PassedBorrowing : Form
    {
        // Delegates
        public updateBooksBoxDelegate updateFunc { get; set; }
        public LiveUpdateDelegate updateCurrentView { get; set; }
        public LiveUpdateDelegate updateMainView { get; set; }
        // Useful constants
        private static readonly char[] SEPERATOR = { '|' };
        // Network variables: approach the server:
        public Connection connection { get; private set; }
        public int userID { get; private set; }

        public PassedBorrowing(updateBooksBoxDelegate updater, Connection conn, int userID)
        {
            this.userID = userID;
            connection = conn;
            updateFunc = updater;
            InitializeComponent();
        }

        /// <summary>
        /// Update by delegate only if the frame visible.
        /// </summary>
        
        public void externalUpdate()
        {
            if (this.Visible) updateList(); 
            // else - don't update .. the user anyway won't see 
        }

        /// <summary>
        /// Refreshing this list.
        /// </summary>

        public void updateList()
        {
            String result;
            // send request
            connection.sendRequest(userID, "passedborrows");
            result = connection.getRespond();
            if (result == "NONE") return;
            // Update this
            updateFunc(checkedListBox_passedborrowing, result);
        }

        /// <summary>
        /// Don't close the borrowing frame, just hide it. 
        /// This disables the need to allocate this frame over again in the "BookView" form.
        /// </summary>

        private void PassedBorrowing_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true; // Don't exit
            Hide(); // just hide.
        }

        /// <summary>
        /// Load list as the form load.
        /// </summary>
        
        private void PassedBorrowing_Load(object sender, EventArgs e)
        {
            updateList();
        }

        /// <summary>
        /// Borrow a book, again.
        /// </summary>

        private void button_reborrow_Click(object sender, EventArgs e)
        {
            // Check wheter some books checked.
            var selectedItems = checkedListBox_passedborrowing.CheckedItems;
            if (selectedItems.Count == 0) /* Nothing selected */ return;
            Regex filter = new Regex(@"([0-9]{0,4})");
            // Prepare request
            List<String> args = new List<string>();

            foreach (var item in selectedItems)
            {
                var m = filter.Match(item.ToString());
                if (m.Success) args.Add(m.NextMatch().Value);
            }
            
            try
            {
                // Send request .....
                connection.sendRequest(userID, "borrowbook", args);
                string result = connection.getRespond();
            }
            catch (Exception)
            {
                MessageBox.Show("An error!");
            }

            // Update both others
            updateList();
            updateMainView();
            updateCurrentView();
        }

    }
}
