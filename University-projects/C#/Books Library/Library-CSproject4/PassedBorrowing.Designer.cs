﻿namespace Library_CSproject4
{
    partial class PassedBorrowing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PassedBorrowing));
            this.checkedListBox_passedborrowing = new System.Windows.Forms.CheckedListBox();
            this.button_reborrow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBox_passedborrowing
            // 
            this.checkedListBox_passedborrowing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox_passedborrowing.FormattingEnabled = true;
            this.checkedListBox_passedborrowing.Location = new System.Drawing.Point(13, 13);
            this.checkedListBox_passedborrowing.Name = "checkedListBox_passedborrowing";
            this.checkedListBox_passedborrowing.Size = new System.Drawing.Size(1116, 484);
            this.checkedListBox_passedborrowing.TabIndex = 0;
            // 
            // button_reborrow
            // 
            this.button_reborrow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_reborrow.Location = new System.Drawing.Point(13, 499);
            this.button_reborrow.Name = "button_reborrow";
            this.button_reborrow.Size = new System.Drawing.Size(1116, 57);
            this.button_reborrow.TabIndex = 1;
            this.button_reborrow.Text = "Borrow these again";
            this.button_reborrow.UseVisualStyleBackColor = true;
            this.button_reborrow.Click += new System.EventHandler(this.button_reborrow_Click);
            // 
            // PassedBorrowing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1141, 568);
            this.Controls.Add(this.button_reborrow);
            this.Controls.Add(this.checkedListBox_passedborrowing);
            this.Name = "PassedBorrowing";
            this.Text = "PassedBorrowing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PassedBorrowing_FormClosing);
            this.Load += new System.EventHandler(this.PassedBorrowing_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox_passedborrowing;
        private System.Windows.Forms.Button button_reborrow;
    }
}