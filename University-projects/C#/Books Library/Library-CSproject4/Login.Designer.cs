﻿namespace Library_CSproject4
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.welcome_label = new System.Windows.Forms.Label();
            this.panel_logincontrols = new System.Windows.Forms.Panel();
            this.label_digitsonly = new System.Windows.Forms.Label();
            this.button_login = new System.Windows.Forms.Button();
            this.textBox_userid = new System.Windows.Forms.TextBox();
            this.label_readerID = new System.Windows.Forms.Label();
            this.label_pleaselogin = new System.Windows.Forms.Label();
            this.readersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel_logincontrols.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // welcome_label
            // 
            this.welcome_label.AutoSize = true;
            this.welcome_label.BackColor = System.Drawing.Color.Transparent;
            this.welcome_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcome_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.welcome_label.Location = new System.Drawing.Point(122, 33);
            this.welcome_label.Name = "welcome_label";
            this.welcome_label.Size = new System.Drawing.Size(170, 39);
            this.welcome_label.TabIndex = 0;
            this.welcome_label.Text = "Welcome";
            // 
            // panel_logincontrols
            // 
            this.panel_logincontrols.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_logincontrols.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_logincontrols.Controls.Add(this.label_digitsonly);
            this.panel_logincontrols.Controls.Add(this.button_login);
            this.panel_logincontrols.Controls.Add(this.textBox_userid);
            this.panel_logincontrols.Controls.Add(this.label_readerID);
            this.panel_logincontrols.Location = new System.Drawing.Point(91, 75);
            this.panel_logincontrols.Name = "panel_logincontrols";
            this.panel_logincontrols.Size = new System.Drawing.Size(405, 139);
            this.panel_logincontrols.TabIndex = 1;
            // 
            // label_digitsonly
            // 
            this.label_digitsonly.AutoSize = true;
            this.label_digitsonly.ForeColor = System.Drawing.Color.Red;
            this.label_digitsonly.Location = new System.Drawing.Point(156, 21);
            this.label_digitsonly.Name = "label_digitsonly";
            this.label_digitsonly.Size = new System.Drawing.Size(61, 13);
            this.label_digitsonly.TabIndex = 6;
            this.label_digitsonly.Text = "(Digits only)";
            // 
            // button_login
            // 
            this.button_login.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_login.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_login.Location = new System.Drawing.Point(122, 80);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(174, 41);
            this.button_login.TabIndex = 4;
            this.button_login.Text = "Login";
            this.button_login.UseVisualStyleBackColor = true;
            this.button_login.Click += new System.EventHandler(this.button_login_Click);
            // 
            // textBox_userid
            // 
            this.textBox_userid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_userid.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_userid.Location = new System.Drawing.Point(80, 38);
            this.textBox_userid.Name = "textBox_userid";
            this.textBox_userid.Size = new System.Drawing.Size(253, 25);
            this.textBox_userid.TabIndex = 1;
            this.textBox_userid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_userid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_userid_KeyPress);
            // 
            // label_readerID
            // 
            this.label_readerID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label_readerID.AutoSize = true;
            this.label_readerID.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_readerID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_readerID.ForeColor = System.Drawing.Color.Black;
            this.label_readerID.Location = new System.Drawing.Point(76, 16);
            this.label_readerID.Name = "label_readerID";
            this.label_readerID.Size = new System.Drawing.Size(84, 19);
            this.label_readerID.TabIndex = 0;
            this.label_readerID.Text = "reader ID:";
            // 
            // label_pleaselogin
            // 
            this.label_pleaselogin.AutoSize = true;
            this.label_pleaselogin.BackColor = System.Drawing.Color.Transparent;
            this.label_pleaselogin.Location = new System.Drawing.Point(283, 59);
            this.label_pleaselogin.Name = "label_pleaselogin";
            this.label_pleaselogin.Size = new System.Drawing.Size(70, 13);
            this.label_pleaselogin.TabIndex = 2;
            this.label_pleaselogin.Text = "Please log-in.";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(575, 226);
            this.Controls.Add(this.label_pleaselogin);
            this.Controls.Add(this.panel_logincontrols);
            this.Controls.Add(this.welcome_label);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.panel_logincontrols.ResumeLayout(false);
            this.panel_logincontrols.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.readersBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label welcome_label;
        private System.Windows.Forms.Panel panel_logincontrols;
        private System.Windows.Forms.Label label_pleaselogin;
        private System.Windows.Forms.Label label_readerID;
        private System.Windows.Forms.TextBox textBox_userid;
        private System.Windows.Forms.Button button_login;
        private System.Windows.Forms.BindingSource readersBindingSource;
        private System.Windows.Forms.Label label_digitsonly;
    }
}

