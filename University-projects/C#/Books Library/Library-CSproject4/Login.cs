﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Library_CSproject4;
using System.Media;
using System.Net.Sockets;
using System.Net;
using System.IO;
using RequestForm;

namespace Library_CSproject4
{
    public partial class Login : Form
    {
        
        // LOGIN CONSTANTS:
        private static readonly int LOGIN_FAILURE = 0, ALREADY_ON = 2; 
        private static readonly string    LOGIN_FAILURE_MSG = "Login has been failed. please check your details again";
        private Connection connection;
        public int userID { get; private set; }
        /// <summary>
        /// The login constructor, which initializing a new connection.
        /// </summary>

        public Login()
        {
            try
            {
                connection = new Connection();
            }
            catch (ConnectionException)
            {
                MessageBox.Show("There is no connection to the server.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(Environment.ExitCode);
            }

            InitializeComponent();
        }

        /// <summary>
        /// Login Click handler.
        /// send a login request, and open the bookView viewer in case login passing.
        /// </summary>

        private void button_login_Click(object sender, EventArgs e)
        {
            try { userID = Convert.ToInt32(textBox_userid.Text); }
            catch (Exception)
            {
                MessageBox.Show("Invalid userID format. only digits are allowed!",
                    "Invalid UserID", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            } 
            
            // Ok - validation passed ...... try to login
            try
            {
                // Send login request
                connection.sendRequest(userID, "login");
            }
            catch (Exception)
            {
                MessageBox.Show("Server is down or unaccessable right now. you may want to try later.",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Check login state
            int login = 0;
            string respond;
          
            try {
                respond = connection.getRespond();
                login = int.Parse(respond);
            }
            catch (Exception) { return; }

            // Message - login failed - no such userID
            if (login == LOGIN_FAILURE)
            {
                MessageBox.Show(LOGIN_FAILURE_MSG, "Login failed: Unknown ID.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (login == ALREADY_ON)
            {
                MessageBox.Show("You are already logged in", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            { // Logged in successfully
                // Open the bookview
                Hide(); // Hide the login frame ..
                new BookView(this, connection, userID).Show();
            }
        }

        /// <summary>
        /// Filter out unwanter character. only digits!
        /// </summary>

        private void textBox_userid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)) e.Handled = true;
        }

        /// <summary>
        /// Tell the server you're gone
        /// </summary>

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                connection.sendRequest(userID, "goodbye");
                // bye ..
                connection.close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
