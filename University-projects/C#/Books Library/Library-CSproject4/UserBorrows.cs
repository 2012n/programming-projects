﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using RequestForm;

namespace Library_CSproject4
{
    public partial class UserBorrows : Form
    {
        // Delegates
        public updateBooksBoxDelegate updateFunc { get; set; }
        public LiveUpdateDelegate updatePassedView { get; set; }
        public LiveUpdateDelegate updateMainView { get; set; }
        // Useful constants
        private static readonly int SUCCESS = 1;
        // Network variables: approach the server:
        public Connection connection { get; private set; }
        public int userID { get; private set; }

        /// <summary>
        /// The UserBorrows constructor.
        /// </summary>
        /// <param name="updater">Update the parent books</param>
        /// <param name="conn">Connection object</param>
        /// <param name="userID">the ID of the user's session</param>

        public UserBorrows(updateBooksBoxDelegate updater, Connection conn, int userID)
        {
            this.userID = userID;
            connection = conn.shallowCopy();
            updateFunc = updater;
            InitializeComponent();
        }

        public void externalUpdate()
        {
            if (Visible) updateList();
            // else - don't update .. the form anyway invisible
        }

        /// <summary>
        /// update list
        /// </summary>

        public void updateList()
        {
            String result;
            // Send
            connection.sendRequest(userID, "myborrows");
            // update..
            result = connection.getRespond();
            if (result == "NONE") return;
            updateFunc(checkedListBox_myborrows, result);
        }

        public void updateList(bool allowNone)
        {
            String result;
            // Send
            connection.sendRequest(userID, "myborrows");
            // update..
            result = connection.getRespond();
            updateFunc(checkedListBox_myborrows, result == "NONE" ? "" : result);
        }

        private void UserBorrows_Load(object sender, EventArgs e)
        {
            updateList();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            // Check wheter some books checked.
            var selectedItems = checkedListBox_myborrows.CheckedItems;
            if (selectedItems.Count == 0) /* Nothing selected */ return;
            Regex filter = new Regex(@"([0-9]{0,4})");
            Match m;
            List<String> Arguments = new List<string>();
            foreach (var item in selectedItems)
            {
                m = filter.Match(item.ToString());
                if (m.Success) Arguments.Add(m.NextMatch().Value);
            }

            // Send request .....
            connection.sendRequest(userID, "returnborrow", Arguments);

            try
            {
                if (connection.getRespond() == SUCCESS.ToString())
                {
                    // Refresh
                    updateList(true);
                    // Update passed borrowing
                    updatePassedView();
                    // update main view
                    updateMainView();
                }
                else
                {
                    MessageBox.Show("Die");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Don't close the borrowing frame, just hide it. 
        /// This disables the need to allocate this frame over again in the "BookView" form.
        /// </summary>
        
        private void UserBorrows_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true; // Don't exit
            Hide(); // just hide.
        }
    }
}
