﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Library_CSproject4;
using System.Media;
using System.Net.Sockets;
using System.Net;
using System.IO;
using RequestForm;
using System.Runtime.Serialization.Formatters.Binary;

namespace Library_CSproject4
{
    // Delegates
    internal delegate void updateBookListDelegate(CheckedListBox list);

    /// <summary>
    /// Connection is the class that responsive about the client connections.
    /// </summary>
    
    public class Connection
    {
        // Usrful constanst
        private static readonly int PORT = 5000;
        private static readonly String LOCALHOST = "127.0.0.1";
        // Network variables
        public NetworkStream output { get; private set; }
        public TcpClient client { get; private set; }
        public IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(LOCALHOST), PORT);
        public BinaryFormatter formatter = new BinaryFormatter();
        // Requester
        Request req = new Request();
        // Constract the connection
        private bool connectionFailure = false;
        private Byte[] Buffer = new Byte[10000];

        /// <summary>
        /// The connection constructor.
        /// </summary>
        
        public Connection()
        {
            try
            {
                client = new TcpClient();
                client.Connect(endPoint);
                output = client.GetStream();
            }
            catch (Exception ex)
            {
                connectionFailure = true;
                throw new ConnectionException(ex.Message);
            }
        }

        public Connection shallowCopy()
        {
            return (Connection)this.MemberwiseClone();
        }

        /// <summary>
        /// Closing all open connections.
        /// </summary>

        public void close()
        {
            if (connectionFailure) return;

            try
            {
                client.Close();
                output.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void sendRequest(Request request)
        {
            if (request.command != null && request.command != "")
            {
                formatter.Serialize(output, request);
            }
        }

        public void sendRequest(int userID, String command)
        {
            if (command != null && command != "")
            {
                req.command = command;
                req.userID = userID;
                formatter.Serialize(output, req);
            }
        }

        public void sendRequest(int userID, String command, List<String> Argument_list)
        {
            if (command != null && command != "")
            {
                req.command = command;
                req.userID = userID;
                req.Arguments = Argument_list;
                formatter.Serialize(output, req);
            }
        }

        public void sendRequest(int userID, String command, String Value)
        {
            if (command != null && command != "")
            {
                req.command = command;
                req.userID = userID;
                req.value = Value;
                formatter.Serialize(output, req);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
    
        public String getRespond()
        {
            int numRead;
            char[] respond;

            try
            {
                numRead = output.Read(Buffer, 0, 10000);
                respond = Encoding.ASCII.GetChars(Buffer, 0, numRead);
            }
            catch (Exception)
            {
                throw;
            }

            return new String(respond);
        }
    }

    public class ConnectionException : ApplicationException
    {
        public ConnectionException() : base() { }
        public ConnectionException(String msg) : base(msg) { }
    }
}
