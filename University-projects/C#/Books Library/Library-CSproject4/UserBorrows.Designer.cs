﻿namespace Library_CSproject4
{
    partial class UserBorrows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserBorrows));
            this.checkedListBox_myborrows = new System.Windows.Forms.CheckedListBox();
            this.button_return_these = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBox_myborrows
            // 
            this.checkedListBox_myborrows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox_myborrows.FormattingEnabled = true;
            this.checkedListBox_myborrows.Location = new System.Drawing.Point(13, 13);
            this.checkedListBox_myborrows.Name = "checkedListBox_myborrows";
            this.checkedListBox_myborrows.Size = new System.Drawing.Size(1001, 304);
            this.checkedListBox_myborrows.TabIndex = 0;
            // 
            // button_return_these
            // 
            this.button_return_these.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.button_return_these.Location = new System.Drawing.Point(13, 332);
            this.button_return_these.Name = "button_return_these";
            this.button_return_these.Size = new System.Drawing.Size(1001, 73);
            this.button_return_these.TabIndex = 1;
            this.button_return_these.Text = "return these books";
            this.button_return_these.UseVisualStyleBackColor = true;
            this.button_return_these.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserBorrows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1026, 407);
            this.Controls.Add(this.button_return_these);
            this.Controls.Add(this.checkedListBox_myborrows);
            this.Name = "UserBorrows";
            this.Text = "UserBorrows";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserBorrows_FormClosing);
            this.Load += new System.EventHandler(this.UserBorrows_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox_myborrows;
        private System.Windows.Forms.Button button_return_these;
    }
}