package com.nprogramming.android.couponsapp.Utils;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.nprogramming.android.couponsapp.Exceptions.APIOperationFailedException;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BIRTHDAY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.EMAIL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.FIRSTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LASTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PASSWORD_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.REGISTER_PATH;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;

public class RegisterUtils {
    /**
     * A utility for register actions
     */

    private static final String TAG = RegisterUtils.class.getSimpleName();


    /**
     * registers a new business owner to the system.
     *
     * @param firstname business owner's first name.
     * @param lastname  business owner's last name.
     * @param email     business owner's email.
     * @param password  account's password.
     * @param birthday  business owner's birthday.
     * @return RegisterResponse object to describe successful or fail reason.
     */

    private static ServerResponse register(String firstname, String lastname, String email, String password, long birthday) throws APIOperationFailedException, NetworkErrorException {
        try {
            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(REGISTER_PATH).build();
            RequestBody requestBody = new FormBody.Builder()
                    .add(FIRSTNAME_PARAM, firstname.trim())
                    .add(LASTNAME_PARAM, lastname.trim())
                    .add(EMAIL_PARAM, email.trim())
                    .add(PASSWORD_PARAM, password.trim())
                    .add(BIRTHDAY_PARAM, birthday + "")
                    .build();

            Request request = new Request.Builder().post(requestBody).url(url).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject jsonResponse = new JSONObject(responseString);

            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));
            if (!response.isSuccessful())
                throw new APIOperationFailedException("register: network operating failed: " + response.code(), serverResponse);

            return serverResponse;
        } catch (IOException | JSONException exception) {
            Log.e(TAG, "Error in register: " + exception.getMessage());
            exception.printStackTrace();
            return ServerResponse.CONNECTION_ERROR;
        }
    }

    /**
     * Register a new owner profile.
     */

    public static class RegisterTask extends AsyncTaskLoader<ServerResponse> {
        final String firstname;
        final String lastname;
        final String email;
        final String password;
        final String birthday;

        public RegisterTask(Context context, String firstname, String lastname, String email, String password, String birthday) {
            super(context);

            this.firstname = firstname;
            this.lastname = lastname;
            this.email = email;
            this.password = password;
            this.birthday = birthday;
        }

        @Override
        public ServerResponse loadInBackground() {
            long birthdayTimestamp = Utils.getTimestampFromDateString(this.birthday);

            try {
                return RegisterUtils.register(
                        this.firstname,
                        this.lastname,
                        this.email,
                        this.password,
                        birthdayTimestamp
                );
            } catch (APIOperationFailedException | NetworkErrorException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
