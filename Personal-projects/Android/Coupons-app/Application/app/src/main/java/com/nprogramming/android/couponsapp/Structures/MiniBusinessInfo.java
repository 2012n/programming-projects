package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;

import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.BUSINESS_PROFILE_PICTURE_URL;

public class MiniBusinessInfo implements Parcelable {
    private int businessId;
    private String businessName;
    private int businessCategory;
    private float businessRating;

    public MiniBusinessInfo(int businessId, String businessName, int businessCategory, float businessRating) {
        this.businessId = businessId;
        this.businessName = businessName;
        this.businessCategory = businessCategory;
        this.businessRating = businessRating;
    }

    public MiniBusinessInfo(Parcel parcel) {
        this.businessId = parcel.readInt();
        this.businessName = parcel.readString();
        this.businessCategory = parcel.readInt();
        this.businessRating = parcel.readFloat();
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public float getBusinessRating() {
        return businessRating;
    }

    public String getBusinessLogoPath() {
        return String.format(BUSINESS_PROFILE_PICTURE_URL, businessId);
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public int getBusinessCategory() {
        return businessCategory;
    }

    public void setBusinessCategory(int businessCategory) {
        this.businessCategory = businessCategory;
    }

    public static final Parcelable.Creator<MiniBusinessInfo> CREATOR = new Creator<MiniBusinessInfo>() {
        @Override
        public MiniBusinessInfo createFromParcel(Parcel parcel) {
            return new MiniBusinessInfo(parcel);
        }

        @Override
        public MiniBusinessInfo[] newArray(int i) {
            return new MiniBusinessInfo[i];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(businessId);
        parcel.writeString(businessName);
        parcel.writeInt(businessCategory);
        parcel.writeFloat(businessRating);
    }
}
