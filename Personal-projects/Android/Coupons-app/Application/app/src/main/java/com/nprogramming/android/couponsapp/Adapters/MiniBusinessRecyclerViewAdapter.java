package com.nprogramming.android.couponsapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.BUSINESS_RATING_FORMAT;

public class MiniBusinessRecyclerViewAdapter extends RecyclerView.Adapter<MiniBusinessRecyclerViewAdapter.MiniBusinessViewHolder> implements UserDataCenter.OnUserDataChangedListener, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private Context context;
    private ArrayList<MiniBusinessInfo> dataset;
    private OnBusinessItemClickListener onBusinessItemClickListener;

    public MiniBusinessRecyclerViewAdapter(Context context) {
        this.context = context;
        UserDataCenter.getInstanceAsync(context, this);
    }

    public void setOnBusinessItemClickListener(OnBusinessItemClickListener onBusinessItemClickListener) {
        this.onBusinessItemClickListener = onBusinessItemClickListener;
    }

    @NonNull
    @Override
    public MiniBusinessViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View miniBusinessFragment = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mini_business, parent, false);
        ImageView ivBusinessLogo = miniBusinessFragment.findViewById(R.id.circleImageView_mini_business_logo);
        TextView tvBusinessName = miniBusinessFragment.findViewById(R.id.textView_mini_business_business_name);
        TextView tvBusinessCategory = miniBusinessFragment.findViewById(R.id.textView_mini_business_category);
        TextView tvRating = miniBusinessFragment.findViewById(R.id.textView_mini_business_rating);

        return new MiniBusinessViewHolder(
                miniBusinessFragment,
                ivBusinessLogo,
                tvBusinessName,
                tvBusinessCategory,
                tvRating
        );
    }

    @Override
    public void onBindViewHolder(@NonNull MiniBusinessViewHolder holder, int position) {
        MiniBusinessInfo businessInfo = dataset.get(position);
        holder.bind(context, businessInfo, onBusinessItemClickListener);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    @Override
    public void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo) {

    }

    @Override
    public void onOwnerCouponRemoved(int couponId) {

    }

    @Override
    public void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo) {
        notifyDataSetChanged();
    }

    @Override
    public void onOwnerBusinessRemoved(int businessId) {
        notifyDataSetChanged();
    }

    @Override
    public void onOwnerAboutDataChanged(OwnerAboutInfo newInfo) {

    }

    @Override
    public void onStarChanged(int couponId, CouponUtils.StarredState state) {

    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.dataset = userDataCenter.getOwnerBusinessInfo();
        userDataCenter.registerDataChangedListenerCallback(this);
    }

    @Override
    public void onError(String message) {

    }

    public static class MiniBusinessViewHolder extends RecyclerView.ViewHolder {
        public int businessId;
        public ImageView ivBusinessLogo;
        public TextView tvBusinessName, tvBusinessCategory, tvBusinessRating;

        public MiniBusinessViewHolder(View itemView, ImageView ivBusinessLogo, TextView tvBusinessName, TextView tvBusinessCategory, TextView tvBusinessRating) {
            super(itemView);
            this.ivBusinessLogo = ivBusinessLogo;
            this.tvBusinessName = tvBusinessName;
            this.tvBusinessCategory = tvBusinessCategory;
            this.tvBusinessRating = tvBusinessRating;
        }

        public void bind(Context context, MiniBusinessInfo miniBusinessInfo, final OnBusinessItemClickListener onBusinessItemClickListener) {
            Picasso.get()
                    .load(miniBusinessInfo.getBusinessLogoPath())
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_shop))
                    .into(ivBusinessLogo);
            tvBusinessName.setText(miniBusinessInfo.getBusinessName());
            tvBusinessCategory.setText(Utils.getCategoryString(miniBusinessInfo.getBusinessCategory()));
            tvBusinessRating.setText(String.format(Locale.getDefault(), BUSINESS_RATING_FORMAT, miniBusinessInfo.getBusinessRating()));
            businessId = miniBusinessInfo.getBusinessId();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBusinessItemClickListener.onItemClick(businessId);
                }
            });
        }
    }

    public interface OnBusinessItemClickListener {
        void onItemClick(int businessId);
    }
}
