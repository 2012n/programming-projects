package com.nprogramming.android.couponsapp.Fragments;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v14.preference.MultiSelectListPreference;
import android.support.v14.preference.SwitchPreference;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Utils.Utils;

import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.PREFERENCE_LOCATION_PERMISSION_REQUEST;


/**
 * A simple {@link Fragment} subclass.
 */
public class CouponsAppPreferenceFragment extends PreferenceFragmentCompat {
    private SwitchPreference spLocationPermission;
    private MultiSelectListPreference mslpNotifyCategories;
    private SwitchPreference spNearbyNotifications;
    private EditTextPreference etpMaxDistanceNotification;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preference, rootKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        spLocationPermission = (SwitchPreference) getPreferenceManager().findPreference(getString(R.string.preferences_location_permission_key));
        mslpNotifyCategories = (MultiSelectListPreference) getPreferenceManager().findPreference(getString(R.string.preferences_notify_categories_key));
        spNearbyNotifications = (SwitchPreference) getPreferenceManager().findPreference(getString(R.string.preferences_nearby_notifications_key));
        etpMaxDistanceNotification = (EditTextPreference) getPreferenceManager().findPreference(getString(R.string.preferences_max_distance_notification_key));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViews();
        fillCategoriesList();
    }

    private void setupViews() {
        Activity parent = getActivity();

        if (parent == null)
            throw new NullPointerException("Could not get parent activity.");

        if (ActivityCompat.checkSelfPermission(parent, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            spLocationPermission.setEnabled(false);
            spLocationPermission.setChecked(true);
        } else {
            spLocationPermission.setChecked(false);
        }

        spLocationPermission.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PREFERENCE_LOCATION_PERMISSION_REQUEST);
                return false; // Should notify listeners only when user reacts the permission request.
            }
        });

        etpMaxDistanceNotification.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                try {
                    Integer.parseInt(newValue.toString());
                    return true;
                } catch (NumberFormatException e) {
                    Toast.makeText(getContext(), getString(R.string.message_invalid_number), Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        });
    }

    private void fillCategoriesList() {
        Utils.BusinessCategory[] businessCategories = Utils.BusinessCategory.values();
        String[] values = new String[businessCategories.length];

        for (int i = 0; i < values.length; i++)
            values[i] = businessCategories[i].toString();

        mslpNotifyCategories.setEntries(values);
        mslpNotifyCategories.setEntryValues(values);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PREFERENCE_LOCATION_PERMISSION_REQUEST:
                Context context = getContext();

                if (context == null)
                    throw new NullPointerException("Could not get context.");

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    spLocationPermission.setEnabled(false);
                    spLocationPermission.setChecked(false);
                    editor.putBoolean(getString(R.string.preferences_location_permission_key), true);
                } else {
                    spLocationPermission.setChecked(false);
                    editor.putBoolean(getString(R.string.preferences_location_permission_key), false);
                }

                editor.apply();
                break;
        }

    }
}
