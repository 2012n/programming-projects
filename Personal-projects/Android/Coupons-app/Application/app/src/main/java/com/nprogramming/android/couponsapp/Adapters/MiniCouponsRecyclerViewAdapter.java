package com.nprogramming.android.couponsapp.Adapters;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.COUPON_END_DATE_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_MAX_DISTANCE_FOR_METERS;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_TITLE_MAX_LENGTH;

public class MiniCouponsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int MINI_COUPON_INFO_VIEW_TYPE = 0;
    private static final int LOADING_PAGE_VIEW_TYPE = 1;

    private Context context;

    private ArrayList<Pageable> dataset;
    private Location phoneLocation;
    private OnCouponItemClickListener couponItemClickListener;

    public MiniCouponsRecyclerViewAdapter(Context context, ArrayList<? extends Pageable> dataset, Location phoneLocation) {
        this.dataset = new ArrayList<>(dataset);
        this.phoneLocation = phoneLocation;
        this.context = context;
    }

    public boolean removeCouponFromDataset(int couponId) {
        for (int i = 0; i < dataset.size(); i++) {
            Pageable item = dataset.get(i);
            if (!(item instanceof MiniCouponInfo)) // Loading bar
                continue;

            if (((MiniCouponInfo) item).getCouponId() == couponId) {
                dataset.remove(i);
                notifyDataSetChanged();
                return true;
            }
        }

        return false;
    }

    public boolean removeLoadingIcon() {
        boolean isRemoved = (dataset.remove(dataset.size() - 1) != null);
        notifyItemRemoved(dataset.size());
        notifyDataSetChanged();
        return isRemoved;
    }

    public boolean addItemToDataset(ArrayList<? extends Pageable> miniCouponInfos) {
        boolean isSuccess = dataset.addAll(miniCouponInfos);
        if (isSuccess)
            notifyDataSetChanged();

        return isSuccess;
    }

    public boolean addItemToDataset(Pageable pageable) {
        boolean isSuccess = dataset.add(pageable);
        if (isSuccess)
            notifyDataSetChanged();

        return isSuccess;
    }

    public void setPhoneLocation(Location phoneLocation) {
        this.phoneLocation = phoneLocation;
        notifyDataSetChanged();
    }

    public void setDataset(ArrayList<? extends Pageable> pageables) {
        this.dataset = new ArrayList<>(pageables);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create the fragment
        switch (viewType) {
            case MINI_COUPON_INFO_VIEW_TYPE:
                View miniCouponsFragment = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_mini_coupon, parent, false);
                ImageView businessIcon = miniCouponsFragment.findViewById(R.id.imageView_business_logo);
                TextView couponTitle = miniCouponsFragment.findViewById(R.id.textView_sale_title);
                TextView businessName = miniCouponsFragment.findViewById(R.id.textView_mini_business_business_title);
                TextView validityDates = miniCouponsFragment.findViewById(R.id.textView_valid_time_data);
                TextView couponsInfo = miniCouponsFragment.findViewById(R.id.textView_info_summery);
                TextView couponDistance = miniCouponsFragment.findViewById(R.id.textView_mini_coupons_distance_value);

                return new MiniCouponsViewHolder(miniCouponsFragment, businessIcon, couponTitle, businessName, validityDates, couponsInfo, couponDistance);
            case LOADING_PAGE_VIEW_TYPE:
                View loadingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_icon_layout, parent, false);
                return new LoaderIconViewHolder(loadingView);
            default:
                throw new IllegalArgumentException("given viewtype not exist.");
        }
    }

    @Override
    public int getItemViewType(int position) {
        Pageable item = dataset.get(position);
        if (item instanceof MiniCouponInfo) return MINI_COUPON_INFO_VIEW_TYPE;
        if (item instanceof ProgressBarItem) return LOADING_PAGE_VIEW_TYPE;
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Pageable coupon = dataset.get(position);
        switch (getItemViewType(position)) {
            case MINI_COUPON_INFO_VIEW_TYPE:
                ((MiniCouponsViewHolder) holder).bind(context, (MiniCouponInfo) coupon, couponItemClickListener, phoneLocation);
                break;

        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void setOnCouponItemClickListener(OnCouponItemClickListener listener) {
        couponItemClickListener = listener;
    }

    public static class MiniCouponsViewHolder extends RecyclerView.ViewHolder {
        public ImageView businessLogo;
        public TextView couponTitle;
        public TextView businessName;
        public TextView validityDates;
        public TextView couponInfo;
        public TextView couponDistance;

        public MiniCouponsViewHolder(View itemView, ImageView businessLogo, TextView couponTitle, TextView businessName, TextView validityDates, TextView couponInfo, TextView couponDistance) {
            super(itemView);
            this.businessLogo = businessLogo;
            this.couponTitle = couponTitle;
            this.businessName = businessName;
            this.validityDates = validityDates;
            this.couponInfo = couponInfo;
            this.couponDistance = couponDistance;
        }

        public void bind(final Context context, final MiniCouponInfo coupon, final OnCouponItemClickListener callback, Location phoneLocation) {
            String description = coupon.getDescription();
            String title = coupon.getCouponTitle();

            Picasso.get()
                    .load(coupon.getBusinessLogoPath())
                    .placeholder(context.getDrawable(R.drawable.ic_shop))
                    .into(businessLogo);

            businessName.setText(coupon.getBusinessName());
            validityDates.setText(String.format(Locale.getDefault(), COUPON_END_DATE_FORMAT, Utils.getCouponStyleDateFormat(coupon.getValidityEnd())));

            if (description.length() > MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH)
                couponInfo.setText(description.substring(0, MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH - 3) + " ..");
            else
                couponInfo.setText(description);

            if (title.length() > MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH)
                couponTitle.setText(title.substring(MINI_COUPON_DISPLAY_TITLE_MAX_LENGTH - 3) + " ..");
            else
                couponTitle.setText(title);

            if (phoneLocation != null) {
                Location couponLocation = new Location("");
                couponLocation.setLatitude(coupon.getBusinessLatitude());
                couponLocation.setLongitude(coupon.getBusinessLongitude());

                float distanceInMeters = couponLocation.distanceTo(phoneLocation);
                if (distanceInMeters < MINI_COUPON_DISPLAY_MAX_DISTANCE_FOR_METERS)
                    couponDistance.setText(context.getString(R.string.label_coupon_distance_meter, (int) distanceInMeters));
                else
                    couponDistance.setText(context.getString(R.string.label_coupon_distance_km, distanceInMeters / 1000.0));
            }

            if (callback != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClick(coupon);
                    }
                });
            }
        }
    }

    public static class LoaderIconViewHolder extends RecyclerView.ViewHolder {
        public LoaderIconViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class ProgressBarItem implements Pageable {
    }

    public interface OnCouponItemClickListener {
        void onClick(MiniCouponInfo couponInfo);
    }
}
