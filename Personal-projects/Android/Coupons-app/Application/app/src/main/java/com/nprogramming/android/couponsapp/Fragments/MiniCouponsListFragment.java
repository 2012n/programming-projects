package com.nprogramming.android.couponsapp.Fragments;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nprogramming.android.couponsapp.Activities.BusinessDetailedActivity;
import com.nprogramming.android.couponsapp.Adapters.MiniCouponsRecyclerViewAdapter;
import com.nprogramming.android.couponsapp.Adapters.Pageable;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.DetailedCouponData;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.AppExecutors;
import com.nprogramming.android.couponsapp.Utils.Constants;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.CouponUtils.StarredState;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTERNAL_PROVIDERS.GOOGLE_NAVIGATION_INTENT_URI;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTERNAL_PROVIDERS.WAZE_NAVIGATE_INTENT_URI;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_COUPONS_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_ID_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.COUPON_INFO_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.COUPON_END_DATE_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.BUSINESS_PROFILE_PICTURE_URL;

public class MiniCouponsListFragment extends Fragment implements LoaderManager.LoaderCallbacks<DetailedCouponData>, SharedPreferences.OnSharedPreferenceChangeListener, UserDataCenter.OnUserDataChangedListener, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private RecyclerView recyclerView;
    private MiniCouponsRecyclerViewAdapter recyclerViewAdapter;
    private View rootView;

    private ArrayList<MiniCouponInfo> coupons;
    private Location phoneLocation;

    private UserDataCenter userDataCenter;
    private AppExecutors executors;

    private AlertDialog couponDetailedDialog;
    private View detailedCouponView;

    private OnScrollReachedEndListener onScrollReachedEndListener;

    // Detailed coupon - form pop up
    private CircleImageView civBusinessPicture;
    private ImageView ivStarredState;
    private TextView tvTitle, tvBusinessNameAndAddress, tvValidityEnd, tvDescription, tvBusinessLinkLabel;
    private SupportMapFragment mapFragment;
    private ImageView ivWaze, ivMaps, ivWhatsapp, ivFacebook;

    public MiniCouponsListFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_mini_coupons_list, container, false);

        Bundle arguments = getArguments();
        if (arguments == null || !arguments.containsKey(ARGUMENTS_COUPONS_KEY))
            throw new IllegalArgumentException("Arguments must not be null and must contain ARGUMENTS_COUPONS_KEY key.");

        coupons = arguments.getParcelableArrayList(ARGUMENTS_COUPONS_KEY);

        if (coupons == null)
            return rootView; // Nothing to show.

        UserDataCenter.getInstanceAsync(getContext(), this);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PreferenceManager.getDefaultSharedPreferences(getContext()).unregisterOnSharedPreferenceChangeListener(this);

        if (userDataCenter != null)
            userDataCenter.unregisterDataChangedListenerCallback(this);
    }

    private void setupCouponRecyclerView() {
        if (recyclerView == null)
            return;

        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerViewAdapter = new MiniCouponsRecyclerViewAdapter(getContext(), coupons, phoneLocation);

        recyclerViewAdapter.setOnCouponItemClickListener(new MiniCouponsRecyclerViewAdapter.OnCouponItemClickListener() {
            @Override
            public void onClick(MiniCouponInfo couponInfo) {
                Bundle args = new Bundle();
                args.putParcelable(COUPON_INFO_KEY, couponInfo);
                FragmentActivity parent = getActivity();

                if (parent == null)
                    throw new NullPointerException("Parent activity must not be null.");

                parent.getSupportLoaderManager().restartLoader(
                        Constants.LOADER_IDS.COUPON_DETAILED_DATA_LODER_ID, args, MiniCouponsListFragment.this).forceLoad();

            }
        });

        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING || newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    if (onScrollReachedEndListener != null) {
                        if (!recyclerView.canScrollVertically(1)) {
                            onScrollReachedEndListener.onScrollReachedBottomLimit();
                            recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                        } else if (!recyclerView.canScrollVertically(-1))
                            onScrollReachedEndListener.onScrollReachedTopLimit();
                    }
                }
            }
        });
    }

    public boolean addItems(ArrayList<MiniCouponInfo> pageable) {
        return recyclerViewAdapter.addItemToDataset(pageable);
    }

    public boolean addItem(Pageable pageable) {
        return recyclerViewAdapter.addItemToDataset(pageable);
    }

    public boolean removeCoupon(MiniCouponInfo miniCouponInfo) {
        return recyclerViewAdapter.removeCouponFromDataset(miniCouponInfo.getCouponId());
    }

    public boolean removeAllProgressBars() {
        return recyclerViewAdapter.removeLoadingIcon();
    }

    public boolean removeCoupon(int couponId) {
        return recyclerViewAdapter.removeCouponFromDataset(couponId);
    }

    public void setOnScrollReachedEndListener(OnScrollReachedEndListener onScrollReachedEndListener) {
        this.onScrollReachedEndListener = onScrollReachedEndListener;
    }

    private void setupDetailedCouponDialogView() {
        // TODO: Handle all of this inside the couon detailed fragment
        detailedCouponView = LayoutInflater.from(getContext()).inflate(R.layout.detailed_coupon_layout, null);
        couponDetailedDialog = new AlertDialog.Builder(MiniCouponsListFragment.this.getContext()).create();
        // Initlaize couponDetailedView views
        ivStarredState = detailedCouponView.findViewById(R.id.imageView_coupon_detailed_starred);
        civBusinessPicture = detailedCouponView.findViewById(R.id.imageView_coupons_detailed_business_picture);
        tvTitle = detailedCouponView.findViewById(R.id.textview_coupon_detailed_title);
        tvBusinessNameAndAddress = detailedCouponView.findViewById(R.id.textView_coupon_detailed_business_name_address);
        tvValidityEnd = detailedCouponView.findViewById(R.id.textView_coupon_detailed_validity_value);
        tvDescription = detailedCouponView.findViewById(R.id.textView_coupon_detailed_description);
        tvBusinessLinkLabel = detailedCouponView.findViewById(R.id.textView_coupon_detailed_see_business_profile);
        ivFacebook = detailedCouponView.findViewById(R.id.imageView_coupon_detailed_facebook);
        ivWhatsapp = detailedCouponView.findViewById(R.id.imageView_coupon_detailed_whatsapp);
        ivMaps = detailedCouponView.findViewById(R.id.imageView_coupon_detailed_maps);
        ivWaze = detailedCouponView.findViewById(R.id.imageView_coupon_detailed_waze);
        mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapfragment_coupon_detailed_map);
    }

    private void attachActionsToLowerBar(final DetailedCouponData data) {
        final ImageView[] navigationIcons = new ImageView[]{ivWaze, ivMaps};
        final String[] navigationUris = new String[]{WAZE_NAVIGATE_INTENT_URI, GOOGLE_NAVIGATION_INTENT_URI};

        for (int i = 0; i < navigationIcons.length; i++) {
            final int idx = i;
            navigationIcons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uri = String.format(navigationUris[idx], data.latitude, data.longitude);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    if (intent.resolveActivity(getContext().getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(getContext(), getString(R.string.message_error_open_external_application), Toast.LENGTH_LONG).show();
                }
            });
        }

        final ImageView[] shareIcons = new ImageView[]{ivFacebook, ivWhatsapp};
        final String[] packages = new String[]{"com.facebook.katana", "com.whatsapp"};

        for (int i = 0; i < packages.length; i++) {
            final int finalI = i;
            shareIcons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.message_coupon_share_format, data.couponId));
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage(packages[finalI]);
                    startActivity(sendIntent);
                }
            });
        }
    }

    private void showDetailedCouponWhenLoaded(final DetailedCouponData data) {
        String businessPicturePath = String.format(BUSINESS_PROFILE_PICTURE_URL, data.businessId);
        // Put relevant text
        Picasso.get()
                .load(businessPicturePath)
                .placeholder(getResources().getDrawable(R.drawable.ic_shop))
                .into(civBusinessPicture);


        tvTitle.setText(data.couponTitle);
        tvValidityEnd.setText(String.format(Locale.getDefault(), COUPON_END_DATE_FORMAT, Utils.getCouponStyleDateFormat(data.couponValidityEnd)));
        tvDescription.setText(data.couponDescription);
        tvBusinessNameAndAddress.setText(String.format("%s - %s", data.businessName, data.businessAddress));

        setupStar(ivStarredState, data);
        attachActionsToLowerBar(data);

        tvBusinessLinkLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent businessPageIntent = new Intent(getContext(), BusinessDetailedActivity.class);
                businessPageIntent.putExtra(BUSINESS_ID_KEY, data.businessId);
                startActivity(businessPageIntent);
            }
        });
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                LatLng coordinates = new LatLng(data.latitude, data.longitude);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));

                map.addMarker(new MarkerOptions()
                        .title(data.businessName)
                        .position(coordinates)
                );
            }
        });

        couponDetailedDialog.setView(detailedCouponView);
        couponDetailedDialog.show();

    }

    private void setupStar(final ImageView ivStarredState, final DetailedCouponData data) {
        executors.getDatabaseIO().execute(new Runnable() {
            @Override
            public void run() {
                StarredState initStarredState = userDataCenter.getStarredState(data.couponId);
                int starStateColor = (initStarredState == StarredState.STARRED ?
                        R.color.ratingBarStarredColor :
                        R.color.ratingBarUnstarredColor);

                ivStarredState.setColorFilter(getContext().getResources().getColor(starStateColor));

            }
        });


        ivStarredState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executors.getDatabaseIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final StarredState starredState = userDataCenter.getStarredState(data.couponId);
                        final StarredState newState = (starredState == StarredState.STARRED ? StarredState.UNSTARRED : StarredState.STARRED);
                        userDataCenter.setStarredState(data.couponId, newState);


                        executors.getMainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                ivStarredState.setColorFilter(getContext().getResources().getColor(
                                        (newState == StarredState.STARRED ?
                                                R.color.ratingBarStarredColor :
                                                R.color.ratingBarUnstarredColor)
                                        )
                                );
                            }
                        });
                    }
                });
            }
        });
    }

    @NonNull
    @Override
    public Loader<DetailedCouponData> onCreateLoader(int id, @Nullable Bundle args) {
        switch (id) {
            case Constants.LOADER_IDS.COUPON_DETAILED_DATA_LODER_ID:
                MiniCouponInfo couponInfo = args.getParcelable(COUPON_INFO_KEY);
                return new CouponUtils.GetDetailedCouponDataTask(getContext(), couponInfo);
            default:
                throw new UnsupportedOperationException("MiniCouponsListFragment: onCreateLoader: No handler for ID " + id);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<DetailedCouponData> loader, final DetailedCouponData data) {
        switch (loader.getId()) {
            case Constants.LOADER_IDS.COUPON_DETAILED_DATA_LODER_ID:
                showDetailedCouponWhenLoaded(data);
                break;
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<DetailedCouponData> loader) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (sharedPreferences.contains(getString(R.string.preferences_location_permission_key)) && sharedPreferences.getBoolean(getString(R.string.preferences_location_permission_key), false)) {
            // Location permission granted, update location
            recyclerViewAdapter.setPhoneLocation(Utils.getPhoneLocation(getContext()));
        }
    }

    @Override
    public void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo) {
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onOwnerCouponRemoved(int couponId) {

    }

    @Override
    public void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo) {

    }

    @Override
    public void onOwnerBusinessRemoved(int businessId) {

    }

    @Override
    public void onOwnerAboutDataChanged(OwnerAboutInfo newInfo) {

    }

    @Override
    public void onStarChanged(int couponId, StarredState state) {

    }

    public boolean setInitialData(ArrayList<MiniCouponInfo> data) {
        if (data == null)
            return false;

        recyclerViewAdapter.setDataset(data);
        return true;
    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.userDataCenter = userDataCenter;
        this.executors = AppExecutors.getInstance();

        phoneLocation = Utils.getPhoneLocation(getContext());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView = rootView.findViewById(R.id.recyclerview_coupons_list);

                setupCouponRecyclerView();
                setupDetailedCouponDialogView();
                PreferenceManager.getDefaultSharedPreferences(getContext()).registerOnSharedPreferenceChangeListener(MiniCouponsListFragment.this);
            }
        });
    }

    @Override
    public void onError(String message) {

    }

    public interface OnScrollReachedEndListener {
        void onScrollReachedTopLimit();

        void onScrollReachedBottomLimit();
    }
}
