package com.nprogramming.android.couponsapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.Adapters.UserProfileFragmentPagerAdapter;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.Fragments.LowerNavigationBarFragment;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

public class OwnerProfileActivity extends AppCompatActivity implements UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private TextView ownerName;
    private ImageView profilePicture;
    private View lower_navigation_bar;

    private OwnerAboutInfo ownerAboutInfo;

    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!LoginUtils.isUserLoggedIn(this)) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        setContentView(R.layout.activity_owner_profile);

        ownerName = findViewById(R.id.textView_user_name);
        profilePicture = findViewById(R.id.imageView_profile_picture);

        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_owner_profile_activity));
        setNavigationBar();

        snackbar = Snackbar.make(lower_navigation_bar, getString(R.string.message_getting_data_from_the_server), Snackbar.LENGTH_INDEFINITE);
        snackbar.show();

        UserDataCenter.getInstanceAsync(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.owner_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.owner_profile_menu_item_logout:
                LoginUtils.logout(this);
                return true;
            case R.id.owner_profile_menu_item_settings:
                Intent settingsActivity = new Intent(this, SettingsActivity.class);
                startActivity(settingsActivity);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Sets the profile picture the owner name.
     */

    private void setupNameAndPicture() {
        String username = ownerAboutInfo.getFirstName() + " " + ownerAboutInfo.getLastName();

        String profileImageUrl = ownerAboutInfo.getProfilePictureUrl();

        ownerName.setText(username);
        Picasso.get()
                .load(profileImageUrl)
                .placeholder(getResources().getDrawable(R.drawable.ic_user))
                .into(profilePicture);
    }

    /**
     * makes Profile tab to be the active tab.
     */

    private void setNavigationBar() {
        lower_navigation_bar = findViewById(R.id.lower_navigation);
        LowerNavigationBarFragment fragment = (LowerNavigationBarFragment) getSupportFragmentManager().findFragmentById(R.id.lower_navigation);
        LowerNavigationBarFragment.setNavItemActiveColor(fragment, LowerNavigationBarFragment.PROFILE_IDX);
    }

    /**
     * Settings up the tab layout and attaches the content page.
     */

    private void setupTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.viewpager_page);

        UserProfileFragmentPagerAdapter adapter = new UserProfileFragmentPagerAdapter(this, getSupportFragmentManager());

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onUserDataCenterCreated(final UserDataCenter userDataCenter) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (snackbar != null)
                    snackbar.dismiss();

                OwnerProfileActivity.this.ownerAboutInfo = userDataCenter.getOwnerAboutInfo();

                setupTabLayout();
                setupNameAndPicture();
            }
        });
    }

    @Override
    public void onError(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (snackbar != null)
                    snackbar.dismiss();

                AlertDialog errorDialog = new AlertDialog.Builder(OwnerProfileActivity.this)
                        .setTitle(getString(R.string.label_error))
                        .setMessage(getString(R.string.message_server_connection_error_detailed, message))
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                OwnerProfileActivity.this.finishAffinity();
                            }
                        }).create();
                errorDialog.show();
            }
        });
    }
}
