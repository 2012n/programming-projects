package com.nprogramming.android.couponsapp.Fragments;


import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Utils.CouponUtils.CouponSearchCretirias;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.nprogramming.android.couponsapp.Utils.Utils.DatePickerSettings;

import static com.nprogramming.android.couponsapp.Utils.Utils.fillCategoryDropbox;
import static com.nprogramming.android.couponsapp.Utils.Utils.getPhoneLocation;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFormFragment extends Fragment {
    private Spinner spinnerCategory;
    private SupportPlaceAutocompleteFragment placeAutocompleteFragment;
    private EditText etSearchRadius, etValidityEnd, etBusinessRating;
    private CheckBox chkOpenNow;
    private Button btnSubmit;

    private Location searchLocation;
    private OnSearchFormSubmittion onSearchFormSubmittion;

    // Common drawables
    Drawable errorMessageBackground, formEdittextBackground;

    public SearchFormFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_search_form, container, false);

        placeAutocompleteFragment = (SupportPlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.placeAutocompleteFragment_search_area);
        spinnerCategory = rootView.findViewById(R.id.spinner_search_category);
        etSearchRadius = rootView.findViewById(R.id.editText_search_search_radius);
        etValidityEnd = rootView.findViewById(R.id.editText_search_validity_end);
        etBusinessRating = rootView.findViewById(R.id.editText_search_business_rating);
        chkOpenNow = rootView.findViewById(R.id.checkBox_search_opennow);
        btnSubmit = rootView.findViewById(R.id.button_search_search);

        errorMessageBackground = getContext().getDrawable(R.drawable.error_message_background);
        formEdittextBackground = getContext().getDrawable(R.drawable.form_edittext_background);

        fillCategoryDropbox(getContext(), spinnerCategory);
        Utils.attachDatepickerToEditText(getContext(), etValidityEnd, DatePickerSettings.FUTURE_ONLY);
        stylizePlaceAutocompleteFragment();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (searchLocation == null) // No location specified in box - fallback to current phone location
                    searchLocation = getPhoneLocation(getContext());

                cleanFormErrors();

                CouponSearchCretirias cretirias = new CouponSearchCretirias(
                        (Utils.BusinessCategory) spinnerCategory.getSelectedItem(),
                        etSearchRadius.getText().toString(),
                        etValidityEnd.getText().toString(),
                        etBusinessRating.getText().toString(),
                        searchLocation
                );

                onSearchFormSubmittion.onSearchFormSubmitted(isFormValid(), cretirias);
            }
        });
        return rootView;
    }

    public void setOnSearchFormSubmittion(OnSearchFormSubmittion onSearchFormSubmittion) {
        this.onSearchFormSubmittion = onSearchFormSubmittion;
    }

    private void cleanFormErrors() {
        etValidityEnd.setBackground(formEdittextBackground);
        etBusinessRating.setBackground(formEdittextBackground);
        etSearchRadius.setBackground(formEdittextBackground);
        placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input).setBackground(formEdittextBackground);
    }

    private boolean isFormValid() {
        boolean validity = true;
        if (etValidityEnd.getText().toString().trim().length() != 0 && !etValidityEnd.getText().toString().trim().matches("^\\d{2}/\\d{2}/\\d{4}")) {
            etValidityEnd.setBackground(errorMessageBackground);
            validity = false;
        }

        if (etBusinessRating.getText().toString().trim().length() != 0 && !etBusinessRating.getText().toString().trim().matches("^\\d(\\.\\d)?$")) {
            etBusinessRating.setBackground(errorMessageBackground);
            validity = false;
        }

        if (etSearchRadius.getText().toString().trim().length() != 0 && !etSearchRadius.getText().toString().trim().matches("\\d+$")) {
            etSearchRadius.setBackground(errorMessageBackground);
            validity = false;
        }

        if (searchLocation == null) {
            placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input).setBackground(errorMessageBackground);
            validity = false;
        }

        return validity;
    }

    private void stylizePlaceAutocompleteFragment() {
        View rootView = placeAutocompleteFragment.getView();

        EditText searchBox = rootView.findViewById(R.id.place_autocomplete_search_input);
        View glassIcon = rootView.findViewById(R.id.place_autocomplete_search_button);

        glassIcon.setVisibility(View.GONE);

        searchBox.setBackground(formEdittextBackground);
        searchBox.setHeight(R.dimen.form_input_view_height);

        searchBox.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        searchBox.setTextColor(getResources().getColor(R.color.formInputTextColor));

        searchBox.setHint(getContext().getString(R.string.label_your_location));

        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                SearchFormFragment.this.searchLocation = new Location("");
                SearchFormFragment.this.searchLocation.setLatitude(place.getLatLng().latitude);
                SearchFormFragment.this.searchLocation.setLongitude(place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {

            }
        });


    }

    public interface OnSearchFormSubmittion {
        void onSearchFormSubmitted(boolean isFormValid, CouponSearchCretirias cretirias);
    }
}
