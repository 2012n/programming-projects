package com.nprogramming.android.couponsapp.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.AddCouponFormData;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_COUPONS_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.COUPON_DATA_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.BUSINESS_SPINNER_ITEM_DISPLAY_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.ADD_NEW_COUPON_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileCouponsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Bundle>, UserDataCenter.OnUserDataChangedListener, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private AlertDialog addCouponDialog;
    private FloatingActionButton fabAddCoupon;
    private View rootView;

    private UserDataCenter userDataCenter;
    private ArrayList<MiniBusinessInfo> businessInfos;
    private ArrayList<MiniCouponInfo> couponInfos;

    private MiniCouponsListFragment fragment;
    // Add new coupon
    private EditText etTitle, etDescription, etEndsOn;
    private TextView tvErrorMessages;
    private Spinner spinnerBusinessChooser;
    private Button btnSubmit;
    private ArrayAdapter<BusinessSpinnerItem> spinnerAdapter;


    public UserProfileCouponsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_user_profile_coupons, container, false);
        UserDataCenter.getInstanceAsync(getContext(), this);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (userDataCenter != null)
            userDataCenter.unregisterDataChangedListenerCallback(this);
    }

    private void addMiniCouponsListFragment() {
        fragment = new MiniCouponsListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARGUMENTS_COUPONS_KEY, couponInfos);
        fragment.setArguments(args);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_user_profile_coupons_linearlayout_body, fragment);
        transaction.commit();
    }

    private void setupAddCouponView(View addCouponView) {
        etTitle = addCouponView.findViewById(R.id.editText_add_coupon_title);
        etDescription = addCouponView.findViewById(R.id.edittext_add_coupon_description);
        etEndsOn = addCouponView.findViewById(R.id.editText_add_coupon_ends_on);
        tvErrorMessages = addCouponView.findViewById(R.id.textView_add_coupon_error_messages);
        spinnerBusinessChooser = addCouponView.findViewById(R.id.spinner_add_coupon_select_business);
        btnSubmit = addCouponView.findViewById(R.id.button_add_coupon_submit);


        fillSpinnerAdapter();
        Utils.attachDatepickerToEditText(getContext(), etEndsOn, Utils.DatePickerSettings.FUTURE_ONLY);
        setupSubmitButton();

    }

    private void fillSpinnerAdapter() {
        ArrayList<BusinessSpinnerItem> businessSpinnerItems = new ArrayList<>();
        for (int i = 0; i < businessInfos.size(); i++) {
            businessSpinnerItems.add(new BusinessSpinnerItem(
                    businessInfos.get(i).getBusinessId(),
                    businessInfos.get(i).getBusinessName()
            ));
        }

        spinnerAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, businessSpinnerItems);
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerBusinessChooser.setAdapter(spinnerAdapter);
    }

    private void setupAddCouponActionHandler() {
        if (fabAddCoupon == null || businessInfos == null) {
            return;
        }

        addCouponDialog = new AlertDialog.Builder(getContext()).create();
        View addCouponView = getLayoutInflater().inflate(R.layout.add_coupon_view, null);

        setupAddCouponView(addCouponView);
        addCouponDialog.setView(addCouponView);

        fabAddCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCouponDialog.show();
            }
        });

    }

    private void setupSubmitButton() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorMessages = validateDataForm();
                if (errorMessages.length() != 0) {
                    tvErrorMessages.setVisibility(View.VISIBLE);
                    tvErrorMessages.setText(errorMessages);
                } else {
                    btnSubmit.setEnabled(false);
                    getActivity().getSupportLoaderManager().restartLoader(ADD_NEW_COUPON_LOADER_ID, null, UserProfileCouponsFragment.this).forceLoad();
                }
            }
        });

    }

    private void clearAddCouponDialogData() {
        etTitle.setText("");
        etDescription.setText("");
        etEndsOn.setText("");
    }

    private String validateDataForm() {
        StringBuilder errorMessages = new StringBuilder();
        int errorCount = 0;

        if (etTitle.getText().toString().trim().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s\n.", ++errorCount, getString(R.string.message_error_coupons_title_must_not_be_empty)));
        if (etDescription.getText().toString().trim().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s\n.", ++errorCount, getString(R.string.message_error_coupons_description_must_not_be_empty)));
        if (etEndsOn.getText().toString().trim().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s\n.", ++errorCount, getString(R.string.message_error_coupons_date_must_not_be_empty)));

        return errorMessages.toString().trim();
    }

    @NonNull
    @Override
    public Loader<Bundle> onCreateLoader(int id, @Nullable Bundle args) {
        switch (id) {
            case ADD_NEW_COUPON_LOADER_ID:
                return new CouponUtils.AddNewCouponTask(getContext(), new AddCouponFormData(
                        ((BusinessSpinnerItem) spinnerBusinessChooser.getSelectedItem()).businessId,
                        etTitle.getText().toString(),
                        etDescription.getText().toString(),
                        etEndsOn.getText().toString()
                ));
            default:
                throw new UnsupportedOperationException(String.format("Loader ID %d is not supported here.", id));
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Bundle> loader, Bundle data) {
        switch (loader.getId()) {
            case ADD_NEW_COUPON_LOADER_ID:
                ServerResponse serverResponse = data.getParcelable(RESPONSE_CODE_FIELD);
                if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                    MiniCouponInfo newCouponInfo = data.getParcelable(COUPON_DATA_KEY);
                    userDataCenter.addOwnerCoupon(newCouponInfo);
                    clearAddCouponDialogData();
                    addCouponDialog.dismiss();
                    btnSubmit.setEnabled(true); // For the next time.

                } else
                    Toast.makeText(getContext(), getString(R.string.message_server_data_error, serverResponse), Toast.LENGTH_LONG).show();
                break;
            default:
                throw new UnsupportedOperationException(String.format("Loader ID %d is not supported here.", loader.getId()));
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Bundle> loader) {

    }

    @Override
    public void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo) {
        fragment.addItem(miniCouponInfo);
    }

    @Override
    public void onOwnerCouponRemoved(int couponId) {

    }

    @Override
    public void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo) {
        fabAddCoupon.setVisibility(View.VISIBLE); // It's got a business :-)
        spinnerAdapter.add(new BusinessSpinnerItem(miniBusinessInfo.getBusinessId(), miniBusinessInfo.getBusinessName()));
    }

    @Override
    public void onOwnerBusinessRemoved(int businessId) {

    }

    @Override
    public void onOwnerAboutDataChanged(OwnerAboutInfo newInfo) {

    }

    @Override
    public void onStarChanged(int couponId, CouponUtils.StarredState state) {

    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.userDataCenter = userDataCenter;
        businessInfos = userDataCenter.getOwnerBusinessInfo();
        couponInfos = userDataCenter.getOwnerCouponsInfo();

        Activity parent = getActivity();
        if (parent == null)
            throw new NullPointerException("Could not get parent activity.");

        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fabAddCoupon = rootView.findViewById(R.id.floatingActionBar_add_owning_coupon);

                // Add MiniCouponsListFragment
                addMiniCouponsListFragment();

                if (businessInfos.size() == 0) // No businesses to add to..
                    fabAddCoupon.setVisibility(View.GONE);
                else
                    setupAddCouponActionHandler();

            }
        });

        userDataCenter.registerDataChangedListenerCallback(this);
    }

    @Override
    public void onError(String message) {

    }

    public static final class BusinessSpinnerItem {
        final int businessId;
        final String businessName;

        public BusinessSpinnerItem(int businessId, String businessName) {
            this.businessId = businessId;
            this.businessName = businessName;
        }

        @Override
        public String toString() {
            return String.format(Locale.getDefault(), BUSINESS_SPINNER_ITEM_DISPLAY_FORMAT, businessId, this.businessName);
        }
    }
}
