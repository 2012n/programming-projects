package com.nprogramming.android.couponsapp.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nprogramming.android.couponsapp.Fragments.UserProfileAboutFragment;
import com.nprogramming.android.couponsapp.Fragments.UserProfileBusinessesFragment;
import com.nprogramming.android.couponsapp.Fragments.UserProfileCouponsFragment;
import com.nprogramming.android.couponsapp.R;

public class UserProfileFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final int ABOUT_TAB = 0;
    private static final int COUPONS_TAB = 1;

    private static final int TABS_COUNT = 3;

    private static String[] PAGE_TITLES;

    public UserProfileFragmentPagerAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);

        PAGE_TITLES = new String[]{context.getString(R.string.label_about),
                context.getString(R.string.label_coupons),
                context.getString(R.string.label_businesses)};
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case ABOUT_TAB:
                fragment = new UserProfileAboutFragment();
                break;
            case COUPONS_TAB:
                fragment = new UserProfileCouponsFragment();
                break;
            default: // BUSINESS TAB
                fragment = new UserProfileBusinessesFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position < TABS_COUNT)
            return PAGE_TITLES[position];
        return "";
    }
}
