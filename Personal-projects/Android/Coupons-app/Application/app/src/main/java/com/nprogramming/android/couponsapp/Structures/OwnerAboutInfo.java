package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;


public class OwnerAboutInfo implements Parcelable {
    private int userId;
    private String profilePictureUrl;
    private String firstName;
    private String lastName;
    private String livesIn;
    private Gender gender;
    private String languages;
    private String graduatedOf;
    private String phone;
    private String email;
    private String facebook;
    private long birthday;

    public enum Gender {
        Male,
        Female
    }

    public OwnerAboutInfo(Parcel parcel) {
        this.userId = parcel.readInt();
        this.profilePictureUrl = parcel.readString();
        this.firstName = parcel.readString();
        this.lastName = parcel.readString();
        this.livesIn = parcel.readString();
        this.gender = (Gender) parcel.readSerializable();
        this.languages = parcel.readString();
        this.graduatedOf = parcel.readString();
        this.phone = parcel.readString();
        this.email = parcel.readString();
        this.facebook = parcel.readString();
        this.birthday = parcel.readLong();
    }

    public OwnerAboutInfo(int userId, String profilePictureUrl, String firstName, String lastName, String livesIn, Gender gender, String languages, String graduatedOf, String phone, String email, String facebook, long birthday) {
        this.userId = userId;
        this.profilePictureUrl = profilePictureUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.livesIn = livesIn;
        this.gender = gender;
        this.languages = languages;
        this.graduatedOf = graduatedOf;
        this.phone = phone;
        this.email = email;
        this.facebook = facebook;
        this.birthday = birthday;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLivesIn() {
        return livesIn;
    }

    public void setLivesIn(String livesIn) {
        this.livesIn = livesIn;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setGender(String gender) {
        if (gender.toLowerCase().equals("male"))
            this.gender = Gender.Male;
        else
            this.gender = Gender.Female;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getGraduatedOf() {
        return graduatedOf;
    }

    public void setGraduatedOf(String graduatedOf) {
        this.graduatedOf = graduatedOf;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public static final Parcelable.Creator<OwnerAboutInfo> CREATOR = new Creator<OwnerAboutInfo>() {
        @Override
        public OwnerAboutInfo createFromParcel(Parcel parcel) {
            return new OwnerAboutInfo(parcel);
        }

        @Override
        public OwnerAboutInfo[] newArray(int size) {
            return new OwnerAboutInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(userId);
        parcel.writeString(profilePictureUrl);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(livesIn);
        parcel.writeSerializable(gender);
        parcel.writeString(languages);
        parcel.writeString(graduatedOf);
        parcel.writeString(phone);
        parcel.writeString(email);
        parcel.writeString(facebook);
        parcel.writeLong(birthday);
    }

    @Override
    public int describeContents() {
        return hashCode();
    }


}

