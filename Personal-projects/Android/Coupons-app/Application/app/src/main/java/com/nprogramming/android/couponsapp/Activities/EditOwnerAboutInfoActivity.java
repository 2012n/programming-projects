package com.nprogramming.android.couponsapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo.Gender;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;
import com.nprogramming.android.couponsapp.Utils.Constants;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.OwnerProfileUtils;
import com.nprogramming.android.couponsapp.Utils.OwnerProfileUtils.SendOwnerUpdatedInfoTask;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.IMAGE_PICKER_RESULT_IMAGE_PATH_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.SUBMIT_OWNER_PROFILE_EDIT_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.UPDATE_USER_PROFILE_PICTURE_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.EDIT_OWNER_PROFILE_PICTURE_PICK_IMAGE_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.EDIT_OWNER_PROFILE_PICTURE_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST;

public class EditOwnerAboutInfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ServerResponse>, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private static final String TAG = EditOwnerAboutInfoActivity.class.getSimpleName();

    private SupportPlaceAutocompleteFragment autocompleteFragment;
    private String selectedLivesIn = "";
    private CircleImageView profilePicture;
    private RadioGroup rgGender;
    private TextView tvErrorMessages;
    private EditText etFirstname, etLastname, etLanguages, etGraduatedOf, etPhone, etEmail, etFacebook;
    private Button buttonSubmit;
    private ProgressBar progressBarSubmit, progressBarProfilePicture;
    private ActionBar actionBar;

    private OwnerAboutInfo ownerAboutInfo;
    private UserDataCenter userDataCenter;

    private String pendingPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_owner_about_info);
        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_search_activity));

        UserDataCenter.getInstanceAsync(this, this);
    }

    private String validateFormData() {
        StringBuilder errorMessages = new StringBuilder();
        int errorCount = 0;

        if (etFirstname.getText().toString().trim().length() < Constants.FORMATTING.FIRSTNAME_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_firstname_length_bad_length)));
        if (etLastname.getText().toString().trim().length() < Constants.FORMATTING.LASTNAME_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_lastname_length_bad_length)));
        if (selectedLivesIn.trim().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_living_place_empty)));
        if (etLanguages.getText().toString().trim().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_languages_empty)));
        if (!Patterns.PHONE.matcher(etPhone.getText().toString().trim()).matches())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_invalid_phone_number)));
        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_invalid_email)));

        return errorMessages.toString().trim();
    }

    private void fillViews() {
        if (ownerAboutInfo != null) {
            Picasso.get()
                    .load(ownerAboutInfo.getProfilePictureUrl())
                    .placeholder(getResources().getDrawable(R.drawable.ic_user))
                    .into(profilePicture);

            etFirstname.setText(ownerAboutInfo.getFirstName());
            etLastname.setText(ownerAboutInfo.getLastName());

            autocompleteFragment.setText(ownerAboutInfo.getLivesIn());
            selectedLivesIn = ownerAboutInfo.getLivesIn();

            if (ownerAboutInfo.getGender() == Gender.Male)
                rgGender.check(R.id.radioButton_edit_owner_about_info_gender_male);
            else
                rgGender.check(R.id.radioButton_edit_owner_about_info_gender_female);

            etLanguages.setText(ownerAboutInfo.getLanguages());
            etGraduatedOf.setText(ownerAboutInfo.getGraduatedOf());
            etPhone.setText(ownerAboutInfo.getPhone());
            etEmail.setText(ownerAboutInfo.getEmail());
            etFacebook.setText(ownerAboutInfo.getFacebook());
        } else {
            Toast.makeText(this, getString(R.string.message_error_failed_to_load_data), Toast.LENGTH_LONG).show();
        }
    }

    private void setupViews() {
        actionBar = getSupportActionBar();

        autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_select_livesin);
        autocompleteFragment.setFilter(
                new AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                        .build()
        );

        profilePicture = findViewById(R.id.edit_owner_about_info_profile_picture);

        tvErrorMessages = findViewById(R.id.textview_edit_owner_about_info_error_message);

        etFirstname = findViewById(R.id.editText_edit_about_first_name);
        etLastname = findViewById(R.id.editText_edit_about_last_name);
        rgGender = findViewById(R.id.radioGroup_edit_owner_about_info_gender);
        etLanguages = findViewById(R.id.editText_edit_owner_about_info_languages);
        etGraduatedOf = findViewById(R.id.editText_edit_owner_about_info_graduated_of);
        etPhone = findViewById(R.id.editText_edit_owner_about_info_phone);
        etEmail = findViewById(R.id.editText_edit_owner_about_info_email);
        etPhone = findViewById(R.id.editText_edit_owner_about_info_phone);
        etFacebook = findViewById(R.id.editText_edit_owner_about_info_facebook);

        buttonSubmit = findViewById(R.id.button_edit_owner_about_info_submit);
        progressBarSubmit = findViewById(R.id.progressBar_edit_owner_about_info_loading);
        progressBarProfilePicture = findViewById(R.id.progressBar_edit_owner_about_info_profile_picture_loading);
    }

    private void openImagePicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, EDIT_OWNER_PROFILE_PICTURE_PICK_IMAGE_REQUEST);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case EDIT_OWNER_PROFILE_PICTURE_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openImagePicker();
                else
                    Toast.makeText(EditOwnerAboutInfoActivity.this, getString(R.string.message_image_picker_request_denied), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case EDIT_OWNER_PROFILE_PICTURE_PICK_IMAGE_REQUEST:
                if (data == null) {
                    return;
                }

                Uri selectedImage = data.getData();

                if (selectedImage == null)
                    return;

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                this.pendingPath = picturePath;

                Bundle args = new Bundle();
                args.putString(IMAGE_PICKER_RESULT_IMAGE_PATH_KEY, picturePath);
                getSupportLoaderManager().restartLoader(UPDATE_USER_PROFILE_PICTURE_LOADER_ID, args, this).forceLoad();
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @NonNull
    @Override
    public Loader<ServerResponse> onCreateLoader(int id, @Nullable Bundle args) {
        int profileId = LoginUtils.getLoggedInUserId(this);

        switch (id) {
            case SUBMIT_OWNER_PROFILE_EDIT_LOADER_ID:
                progressBarSubmit.setVisibility(View.VISIBLE);

                return new OwnerProfileUtils.SendOwnerUpdatedInfoTask(this,
                        new OwnerAboutInfo(
                                profileId,
                                null,
                                etFirstname.getText().toString(),
                                etLastname.getText().toString(),
                                selectedLivesIn,
                                rgGender.getCheckedRadioButtonId() == R.id.radioButton_edit_owner_about_info_gender_male ? Gender.Male : Gender.Female,
                                etLanguages.getText().toString(),
                                etGraduatedOf.getText().toString(),
                                etPhone.getText().toString(),
                                etEmail.getText().toString(),
                                etFacebook.getText().toString(),
                                0)
                );
            case UPDATE_USER_PROFILE_PICTURE_LOADER_ID:
                if (!args.containsKey(IMAGE_PICKER_RESULT_IMAGE_PATH_KEY))
                    throw new IllegalArgumentException("IMAGE_PICKER_RESULT_IMAGE_PATH_KEY must be set in the arguments\' bundle.");

                progressBarProfilePicture.setVisibility(View.VISIBLE);
                String picturePath = args.getString(IMAGE_PICKER_RESULT_IMAGE_PATH_KEY);
                return new OwnerProfileUtils.UpdateOwnerProfilePictureTask(this, profileId, picturePath);
            default:
                throw new UnsupportedOperationException("EditOwnerAboutInfoActivity: onCreateLoader: Id handler is not implemented for " + id);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ServerResponse> loader, ServerResponse data) {
        switch (loader.getId()) {
            case UPDATE_USER_PROFILE_PICTURE_LOADER_ID:
                progressBarProfilePicture.setVisibility(View.GONE);
                if (data == ServerResponse.OPERATION_SUCCESS) {
                    Bitmap bitmap = BitmapFactory.decodeFile(pendingPath);
                    profilePicture.setImageBitmap(bitmap);
                } else
                    Toast.makeText(EditOwnerAboutInfoActivity.this, getString(R.string.message_server_data_error, data.toString()), Toast.LENGTH_LONG).show();
                break;
            case SUBMIT_OWNER_PROFILE_EDIT_LOADER_ID:
                if (data == ServerResponse.OPERATION_SUCCESS) {
                    Toast.makeText(EditOwnerAboutInfoActivity.this, getString(R.string.message_profile_updated_successfully), Toast.LENGTH_LONG).show();
                    userDataCenter.ownerInfoChanged(((SendOwnerUpdatedInfoTask) loader).updatedAboutInfo);
                    NavUtils.navigateUpFromSameTask(this);
                } else {
                    Toast.makeText(EditOwnerAboutInfoActivity.this, getString(R.string.message_server_data_error, data.toString()), Toast.LENGTH_LONG).show();
                    progressBarSubmit.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ServerResponse> loader) {

    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.userDataCenter = userDataCenter;
        ownerAboutInfo = userDataCenter.getOwnerAboutInfo();

        setupViews();
        fillViews();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                profilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(EditOwnerAboutInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                            ActivityCompat.requestPermissions(EditOwnerAboutInfoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST);
                        else {
                            openImagePicker();
                        }
                    }
                });

                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {
                        selectedLivesIn = place.getName().toString();

                        Geocoder geocoder = new Geocoder(EditOwnerAboutInfoActivity.this, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        if (addresses.size() > 0)
                            selectedLivesIn += (", " + (addresses.get(0).getCountryName()));

                        EditText autocompleteTextField = Objects.requireNonNull(autocompleteFragment.getView()).findViewById(R.id.place_autocomplete_search_input);
                        autocompleteTextField.setText(selectedLivesIn);
                    }

                    @Override
                    public void onError(Status status) {
                        Log.e(TAG, "On error occurred when selecting place: " + status.getStatusMessage());
                    }
                });

                buttonSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String errorMessages = validateFormData();
                        if (errorMessages.length() != 0) {
                            tvErrorMessages.setText(errorMessages);
                            tvErrorMessages.setVisibility(View.VISIBLE);
                        } else
                            getSupportLoaderManager().restartLoader(SUBMIT_OWNER_PROFILE_EDIT_LOADER_ID, null, EditOwnerAboutInfoActivity.this).forceLoad();
                    }
                });
            }
        });
    }

    @Override
    public void onError(String message) {
        Utils.showErrorMessage(this, (ConstraintLayout) findViewById(R.id.edit_owner_about_info_root_view), R.drawable.ic_frown, getString(R.string.message_server_connection_error), true);
    }
}