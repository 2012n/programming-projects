package com.nprogramming.android.couponsapp.Data;

import android.content.Context;

import com.nprogramming.android.couponsapp.Data.UserFavourites.UserFavouritesModel;
import com.nprogramming.android.couponsapp.Exceptions.APIOperationFailedException;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.AppExecutors;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.OwnerProfileUtils;

import java.util.ArrayList;

public class UserDataCenter {
    private static UserDataCenter INSTANCE = null;

    private int userId;

    private ArrayList<MiniCouponInfo> ownerCouponsInfo;
    private ArrayList<MiniBusinessInfo> ownerBusinessInfo;
    private OwnerAboutInfo ownerAboutInfo;

    public ApplicationDatabase getApplicationDatabase() {
        return applicationDatabase;
    }

    private ApplicationDatabase applicationDatabase;

    private ArrayList<OnUserDataChangedListener> userDataChangedListeners;

    private UserDataCenter(Context context) {
        this.userId = LoginUtils.getLoggedInUserId(context);
        this.userDataChangedListeners = new ArrayList<>();
        this.applicationDatabase = ApplicationDatabase.getInstance(context);
    }

    public static void getInstanceAsync(final Context context, final OnUserDataCenterGettingInstanceFinished onFinish) {
        if (INSTANCE == null) {
            AppExecutors.getInstance().getNetworkIO().execute(new Runnable() {
                @Override
                public void run() {
                    INSTANCE = new UserDataCenter(context);
                    try {
                        INSTANCE.ownerBusinessInfo = OwnerProfileUtils.getOwnerBusinessesByOwnerId(INSTANCE.userId);
                        INSTANCE.ownerCouponsInfo = OwnerProfileUtils.getOwnerCouponsByOwnerId(INSTANCE.userId);
                        INSTANCE.ownerAboutInfo = OwnerProfileUtils.getOwnerAboutDataByOwnerId(INSTANCE.userId);
                    } catch (APIOperationFailedException e) {
                        onFinish.onError(e.getLocalizedMessage());
                        INSTANCE = null;
                        return;
                    }

                    onFinish.onUserDataCenterCreated(INSTANCE);
                }
            });
        } else
            onFinish.onUserDataCenterCreated(INSTANCE);
    }

    public static UserDataCenter getInstanceSync(final Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserDataCenter(context);
            try {
                INSTANCE.ownerBusinessInfo = OwnerProfileUtils.getOwnerBusinessesByOwnerId(INSTANCE.userId);
                INSTANCE.ownerCouponsInfo = OwnerProfileUtils.getOwnerCouponsByOwnerId(INSTANCE.userId);
                INSTANCE.ownerAboutInfo = OwnerProfileUtils.getOwnerAboutDataByOwnerId(INSTANCE.userId);
            } catch (APIOperationFailedException e) {
                INSTANCE = null;
            }
        }

        return INSTANCE;
    }


    public ArrayList<MiniCouponInfo> getOwnerCouponsInfo() {
        return ownerCouponsInfo;
    }

    public ArrayList<MiniBusinessInfo> getOwnerBusinessInfo() {
        return ownerBusinessInfo;
    }

    public OwnerAboutInfo getOwnerAboutInfo() {
        return ownerAboutInfo;
    }

    public int getUserId() {
        return userId;
    }

    public boolean registerDataChangedListenerCallback(OnUserDataChangedListener listener) {
        return userDataChangedListeners.add(listener);
    }

    public boolean unregisterDataChangedListenerCallback(OnUserDataChangedListener listener) {
        return userDataChangedListeners.remove(listener);
    }

    public boolean addOwnerBusiness(MiniBusinessInfo info) {
        if (ownerBusinessInfo.add(info)) {
            for (OnUserDataChangedListener listener : userDataChangedListeners)
                listener.onOwnerBusinessAdded(info);
            return true;
        }
        return false;
    }

    public boolean removeOwnerBusiness(MiniBusinessInfo info) {
        if (ownerBusinessInfo.remove(info)) {
            for (OnUserDataChangedListener listener : userDataChangedListeners)
                listener.onOwnerBusinessRemoved(info.getBusinessId());
            return true;
        }
        return false;
    }

    public boolean addOwnerCoupon(MiniCouponInfo info) {
        if (ownerCouponsInfo.add(info)) {
            for (OnUserDataChangedListener listener : userDataChangedListeners)
                listener.onOwnerCouponAdded(info);
            return true;
        }
        return false;
    }

    public boolean removeOwnerCoupon(MiniCouponInfo info) {
        if (ownerCouponsInfo.remove(info)) {
            for (OnUserDataChangedListener listener : userDataChangedListeners)
                listener.onOwnerCouponRemoved(info.getCouponId());
            return true;
        }
        return false;
    }

    public void ownerInfoChanged(OwnerAboutInfo newInfo) {
        this.ownerAboutInfo = newInfo;
        for (OnUserDataChangedListener listener : userDataChangedListeners)
            listener.onOwnerAboutDataChanged(newInfo);
    }

    public CouponUtils.StarredState getStarredState(int couponId) {
        return applicationDatabase.userFavouritesDAO().isStarred(this.userId, couponId) == 0 ? CouponUtils.StarredState.UNSTARRED : CouponUtils.StarredState.STARRED;

    }

    public int[] getStarredCoupons() {
        return applicationDatabase.userFavouritesDAO().getFavouriteCouponsByUserId(this.userId);
    }

    public void setStarredState(int couponId, CouponUtils.StarredState starredState) {
        if (starredState == CouponUtils.StarredState.STARRED)
            applicationDatabase.userFavouritesDAO().starCouponForUser(
                    new UserFavouritesModel(this.userId, couponId));
        else
            applicationDatabase.userFavouritesDAO().unstarCouponForUser(this.userId, couponId);

        for (OnUserDataChangedListener listener : userDataChangedListeners)
            listener.onStarChanged(couponId, starredState);
    }

    public interface OnUserDataChangedListener {
        void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo);

        void onOwnerCouponRemoved(int couponId);

        void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo);

        void onOwnerBusinessRemoved(int businessId);

        void onOwnerAboutDataChanged(OwnerAboutInfo newInfo);

        void onStarChanged(int couponId, CouponUtils.StarredState state);
    }

    public interface OnUserDataCenterGettingInstanceFinished {
        void onUserDataCenterCreated(UserDataCenter userDataCenter);

        void onError(String message);
    }
}
