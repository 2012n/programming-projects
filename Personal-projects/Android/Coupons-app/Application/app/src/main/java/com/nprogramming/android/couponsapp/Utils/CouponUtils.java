package com.nprogramming.android.couponsapp.Utils;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.Exceptions.APIOperationFailedException;
import com.nprogramming.android.couponsapp.Structures.AddCouponFormData;
import com.nprogramming.android.couponsapp.Structures.DetailedCouponData;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.COUPON_TITLE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DATA_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LAST_VALIDITY_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LATITUDE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.COUPON_DATA_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_PORT;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BUSINESS_ID_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BUSINESS_NAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.CATEGORY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.COUPON_TITLE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.DESCRIPTION_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.ID_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LAST_VALIDITY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LATITUDE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LONGITUDE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PAGE_ITEM_COUNT_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PAGE_NUMBER_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.RADIUS_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.RATING_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.ADD_COUPON_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.BUSINESS_PROFILE_PICTURE_URL;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.GET_COUPON_BY_ID_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.NEARBY_COUPONS_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.SEARCH_COUPONS_PATH;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;

public class CouponUtils {
    private static final String TAG = CouponUtils.class.getSimpleName();

    private static DetailedCouponData getDetailedCouponData(MiniCouponInfo miniCouponInfo) throws APIOperationFailedException {
        BusinessUtils.BusinessPageData businessPageData = BusinessUtils.getBusinessPageData(miniCouponInfo.getBusinessId());

        if (businessPageData == null)
            throw new APIOperationFailedException("Could not retrieve BusinessPageData for business id = " + miniCouponInfo.getBusinessId());

        return new DetailedCouponData(
                miniCouponInfo.getCouponId(),
                miniCouponInfo.getBusinessId(),
                miniCouponInfo.getCouponTitle(),
                businessPageData.businessName,
                businessPageData.address,
                miniCouponInfo.getValidityEnd(),
                miniCouponInfo.getDescription(),
                (float) businessPageData.coordinates.latitude,
                (float) businessPageData.coordinates.longitude
        );
    }

    private static Bundle addNewCoupon(AddCouponFormData data) throws NetworkErrorException {
        Bundle newCouponData = new Bundle();
        try {
            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(ADD_COUPON_PATH).build();

            RequestBody requestBody = new FormBody.Builder()
                    .add(BUSINESS_ID_PARAM, data.businessId)
                    .add(LAST_VALIDITY_PARAM, data.couponValidityEnd)
                    .add(DESCRIPTION_PARAM, data.couponDescription)
                    .add(COUPON_TITLE_PARAM, data.couponTitle)
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject responseBodyJson = new JSONObject(responseString);

            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(responseBodyJson.getInt(RESPONSE_CODE_FIELD));

            if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                newCouponData.putParcelable(
                        COUPON_DATA_KEY,
                        getCouponById(responseBodyJson.getInt(ID_FIELD))
                );
            }

            newCouponData.putParcelable(RESPONSE_CODE_FIELD, serverResponse);
            return newCouponData;
        } catch (IOException | JSONException ex) {
            ex.printStackTrace();
            newCouponData.putParcelable(RESPONSE_CODE_FIELD, ServerResponse.UNKNOWN_ERROR);
            return newCouponData;
        }
    }

    private static MiniCouponInfo getCouponById(String couponId) throws NetworkErrorException {
        // Currently REST API server does not support retrieving array of ids
        try {
            HttpUrl url = new HttpUrl.Builder()
                    .scheme("http")
                    .host(SERVER_HOST)
                    .port(SERVER_PORT)
                    .addPathSegment(GET_COUPON_BY_ID_PATH)
                    .addQueryParameter(ID_PARAM, couponId)
                    .build();


            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();
            JSONObject jsonData = new JSONObject(data);

            if (jsonData.optInt(RESPONSE_CODE_FIELD) == ServerResponse.OPERATION_SUCCESS.ordinal()) {
                JSONObject coupon = jsonData.getJSONObject(DATA_FIELD);
                return new MiniCouponInfo(
                        coupon.optInt(ID_FIELD),
                        coupon.optInt(BUSINESS_ID_FIELD),
                        String.format(BUSINESS_PROFILE_PICTURE_URL, coupon.optInt(BUSINESS_ID_FIELD)),
                        coupon.optString(COUPON_TITLE_FIELD),
                        coupon.optString(BUSINESS_NAME_PARAM),
                        coupon.optLong(LAST_VALIDITY_FIELD),
                        coupon.optString(DESCRIPTION_PARAM),
                        (float) coupon.optDouble(LATITUDE_FIELD),
                        (float) coupon.optDouble(LONGITUDE_PARAM)
                );
            }
        } catch (IOException | JSONException ex) {
            Log.e("CouponUtils", ex.getMessage());
            ex.printStackTrace();
            return null;
        }

        return null;
    }

    private static MiniCouponInfo getCouponById(int id) {
        try {
            return getCouponById(id + "");
        } catch (NetworkErrorException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static ArrayList<MiniCouponInfo> getCouponsByCretirias(CouponSearchCretirias cretirias) throws NetworkErrorException {
        ArrayList<MiniCouponInfo> result = null;

        try {
            HttpUrl url = new HttpUrl.Builder()
                    .scheme("http")
                    .host(SERVER_HOST)
                    .port(SERVER_PORT)
                    .addPathSegment(SEARCH_COUPONS_PATH)
                    .addQueryParameter(CATEGORY_PARAM, cretirias.getCategory())
                    .addQueryParameter(LAST_VALIDITY_PARAM, cretirias.getValidityEnd())
                    .addQueryParameter(RATING_PARAM, cretirias.getRating())
                    .addQueryParameter(RADIUS_PARAM, cretirias.getSearchRadius())
                    .addQueryParameter(LATITUDE_PARAM, cretirias.getLatitude())
                    .addQueryParameter(LONGITUDE_PARAM, cretirias.getLongitude())
                    .build();

            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();

            JSONObject jsonData = new JSONObject(data);

            if (jsonData.optInt(RESPONSE_CODE_FIELD) == ServerResponse.OPERATION_SUCCESS.ordinal()) {
                JSONArray coupons = jsonData.optJSONArray(DATA_FIELD);
                if (coupons != null) {
                    result = new ArrayList<>();
                    for (int i = 0; i < coupons.length(); i++) {
                        JSONObject coupon = coupons.getJSONObject(i);
                        result.add(new MiniCouponInfo(
                                coupon.optInt(ID_FIELD),
                                coupon.optInt(BUSINESS_ID_FIELD),
                                String.format(BUSINESS_PROFILE_PICTURE_URL, coupon.optInt(BUSINESS_ID_FIELD)),
                                coupon.optString(COUPON_TITLE_FIELD),
                                coupon.optString(BUSINESS_NAME_PARAM),
                                coupon.optLong(LAST_VALIDITY_FIELD),
                                coupon.optString(DESCRIPTION_PARAM),
                                (float) coupon.optDouble(LATITUDE_FIELD),
                                (float) coupon.optDouble(LONGITUDE_PARAM)
                        ));
                    }
                }
            }
        } catch (IOException | JSONException ex) {
            Log.e(TAG, "Could not load search results: " + ex.getMessage());
            result = null;
            ex.printStackTrace();
        }

        return result;
    }

    private static ArrayList<MiniCouponInfo> getFavouriteCoupons(Context context) {
        UserDataCenter userDataCenter = UserDataCenter.getInstanceSync(context);
        int[] ids = userDataCenter.getStarredCoupons();

        ArrayList<MiniCouponInfo> result = new ArrayList<>();
        try {
            for (int id : ids) {
                result.add(getCouponById(id));
            }

            return result;
        } catch (Exception ex) {
            Log.e(TAG, "Error getting favourite coupons: " + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    private static ArrayList<MiniCouponInfo> getNearbyCoupons(Location location, int pageNumber, int itemCount) throws APIOperationFailedException, NetworkErrorException {
        ArrayList<MiniCouponInfo> result = null;

        try {
            if (location == null)
                throw new IllegalArgumentException("Location argument must not be null.");

            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(NEARBY_COUPONS_PATH)
                    .addQueryParameter(LATITUDE_PARAM, location.getLatitude() + "")
                    .addQueryParameter(LONGITUDE_PARAM, location.getLongitude() + "")
                    .addQueryParameter(PAGE_NUMBER_PARAM, pageNumber + "")
                    .addQueryParameter(PAGE_ITEM_COUNT_PARAM, itemCount + "")
                    .build();

            Request requst = new Request.Builder().get().url(url).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(requst).execute();

            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject jsonResponse = new JSONObject(responseString);

            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));

            if (!response.isSuccessful())
                throw new APIOperationFailedException("Failed to get results: Server returned " + response.code(), serverResponse);

            if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                try {
                    JSONArray data = jsonResponse.getJSONArray(DATA_FIELD);
                    result = new ArrayList<>();
                    for (int i = 0; i < data.length(); i++) {
                        try {
                            JSONObject coupon = data.getJSONObject(i);
                            result.add(
                                    new MiniCouponInfo(
                                            coupon.optInt(ID_FIELD),
                                            coupon.optInt(BUSINESS_ID_FIELD),
                                            String.format(BUSINESS_PROFILE_PICTURE_URL, coupon.optInt(BUSINESS_ID_FIELD)),
                                            coupon.optString(COUPON_TITLE_FIELD),
                                            coupon.optString(BUSINESS_NAME_PARAM),
                                            coupon.optLong(LAST_VALIDITY_FIELD),
                                            coupon.optString(DESCRIPTION_PARAM),
                                            (float) coupon.optDouble(LATITUDE_FIELD),
                                            (float) coupon.optDouble(LONGITUDE_PARAM)
                                    )
                            );
                        } catch (JSONException ex) {
                            Log.w(TAG, String.format("The %dth item cannot be parsed.", i));
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Could not get data result!");
                    result = null;
                }
            }
        } catch (IOException | JSONException e) {
            result = null;
        }

        return result;
    }

    public static class GetDetailedCouponDataTask extends AsyncTaskLoader<DetailedCouponData> {
        final MiniCouponInfo miniCouponInfo;

        public GetDetailedCouponDataTask(Context context, MiniCouponInfo miniCouponInfo) {
            super(context);
            this.miniCouponInfo = miniCouponInfo;
        }

        @Override
        public DetailedCouponData loadInBackground() {
            try {
                return getDetailedCouponData(miniCouponInfo);
            } catch (APIOperationFailedException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class AddNewCouponTask extends AsyncTaskLoader<Bundle> {
        final AddCouponFormData couponFormData;

        public AddNewCouponTask(@NonNull Context context, AddCouponFormData couponFormData) {
            super(context);
            this.couponFormData = couponFormData;
        }

        @Nullable
        @Override
        public Bundle loadInBackground() {
            try {
                return addNewCoupon(couponFormData);
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class GetCouponSearchResultsTask extends AsyncTaskLoader<ArrayList<MiniCouponInfo>> {
        final CouponSearchCretirias cretirias;

        public GetCouponSearchResultsTask(@NonNull Context context, CouponSearchCretirias cretirias) {
            super(context);
            this.cretirias = cretirias;
        }

        @Nullable
        @Override
        public ArrayList<MiniCouponInfo> loadInBackground() {
            try {
                return getCouponsByCretirias(cretirias);
            } catch (NetworkErrorException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class GetFavouriteCouponsTask extends AsyncTaskLoader<ArrayList<MiniCouponInfo>> {
        public GetFavouriteCouponsTask(@NonNull Context context) {
            super(context);
        }

        @Nullable
        @Override
        public ArrayList<MiniCouponInfo> loadInBackground() {
            return getFavouriteCoupons(getContext());
        }
    }

    public static class GetCouponByIdTask extends AsyncTaskLoader<ArrayList<MiniCouponInfo>> {
        final int couponId;

        public GetCouponByIdTask(@NonNull Context context, int couponId) {
            super(context);
            this.couponId = couponId;
        }

        @Nullable
        @Override
        public ArrayList<MiniCouponInfo> loadInBackground() {
            MiniCouponInfo data = getCouponById(couponId);
            ArrayList<MiniCouponInfo> r = new ArrayList<>();
            r.add(data);
            return r;
        }
    }

    public static class GetNearByCouponsTask extends AsyncTaskLoader<ArrayList<MiniCouponInfo>> {
        final Location location;
        final int pageNumber;
        final int itemCount;

        public GetNearByCouponsTask(@NonNull Context context, Location location, int pageNumber, int itemCount) {
            super(context);
            this.location = location;
            this.pageNumber = pageNumber;
            this.itemCount = itemCount;
        }

        @Nullable
        @Override
        public ArrayList<MiniCouponInfo> loadInBackground()  {
            try {
                return getNearbyCoupons(location, pageNumber, itemCount);
            } catch (APIOperationFailedException | NetworkErrorException e) {
                return null;
            }
        }
    }

    public static final class CouponSearchCretirias implements Parcelable {
        public final String category;
        public final String searchRadius;
        public final String validityEnd;
        public final String rating;
        public final String latitude;
        public final String longitude;

        public CouponSearchCretirias(Utils.BusinessCategory category, String searchRadius, String validityEnd, String rating, Location location) {
            this.category = category == Utils.BusinessCategory.UNDEFINED ? "null" : category.ordinal() + "";
            this.searchRadius = searchRadius.trim() + "";
            this.validityEnd = validityEnd.trim().length() == 0 ? "null" : Utils.getTimestampFromDateString(validityEnd.trim()) + "";
            this.rating = rating.trim();
            this.latitude = (location != null ? location.getLatitude() : 0.0) + "";
            this.longitude = (location != null ? location.getLongitude() : 0.0) + "";
        }

        protected CouponSearchCretirias(Parcel in) {
            category = in.readString();
            searchRadius = in.readString();
            validityEnd = in.readString();
            rating = in.readString();
            latitude = in.readString();
            longitude = in.readString();
        }

        public String getCategory() {
            return category;
        }

        public String getSearchRadius() {
            return searchRadius.length() == 0 ? "null" : searchRadius;
        }

        public String getValidityEnd() {
            return validityEnd.length() == 0 ? "null" : validityEnd;
        }

        public String getRating() {
            return rating.length() == 0 ? "null" : rating;
        }

        public String getLatitude() {
            return latitude.length() == 0 ? "null" : latitude;
        }

        public String getLongitude() {
            return longitude.length() == 0 ? "null" : longitude;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(category);
            dest.writeString(searchRadius);
            dest.writeString(validityEnd);
            dest.writeString(rating);
            dest.writeString(latitude);
            dest.writeString(longitude);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CouponSearchCretirias> CREATOR = new Creator<CouponSearchCretirias>() {
            @Override
            public CouponSearchCretirias createFromParcel(Parcel in) {
                return new CouponSearchCretirias(in);
            }

            @Override
            public CouponSearchCretirias[] newArray(int size) {
                return new CouponSearchCretirias[size];
            }
        };

    }

    public enum StarredState implements Parcelable {
        STARRED,
        UNSTARRED;

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(ordinal());
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<StarredState> CREATOR = new Creator<StarredState>() {
            @Override
            public StarredState createFromParcel(Parcel in) {
                return values()[in.readInt()];
            }

            @Override
            public StarredState[] newArray(int size) {
                return new StarredState[size];
            }
        };
    }
}
