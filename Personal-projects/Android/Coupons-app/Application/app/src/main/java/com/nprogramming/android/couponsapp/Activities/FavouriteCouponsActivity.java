package com.nprogramming.android.couponsapp.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.Fragments.LowerNavigationBarFragment;
import com.nprogramming.android.couponsapp.Fragments.MiniCouponsListFragment;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.ArrayList;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_COUPONS_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_PHONE_LOCATION_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.COUPON_ID_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.ADD_COUPON_TO_FAVOURITE_LIST_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.GET_FAVOURITE_COUPONS_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.CouponUtils.StarredState.UNSTARRED;

public class FavouriteCouponsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<MiniCouponInfo>>, UserDataCenter.OnUserDataChangedListener, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private LinearLayout body;
    private ProgressBar loadingBar;
    private MiniCouponsListFragment fragment;
    private UserDataCenter userDataCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_coupons);

        body = findViewById(R.id.linearlayout_favourite_coupons);
        loadingBar = findViewById(R.id.progressBar_favourite_loading);

        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_favourites_activity));
        setNavigationBar();

        UserDataCenter.getInstanceAsync(this, this);
    }

    private void fillFavouriteCoupons(ArrayList<MiniCouponInfo> data) {
        fragment = new MiniCouponsListFragment();
        Bundle args = new Bundle();

        args.putParcelable(ARGUMENTS_PHONE_LOCATION_KEY, Utils.getPhoneLocation(this));
        args.putParcelableArrayList(ARGUMENTS_COUPONS_KEY, data);
        fragment.setArguments(args);

        body.removeAllViews();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.linearlayout_favourite_coupons, fragment);
        transaction.commit();
    }

    private void setNavigationBar() {
        LowerNavigationBarFragment fragment = (LowerNavigationBarFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_favourite_coupons_lower_bar);
        LowerNavigationBarFragment.setNavItemActiveColor(fragment, LowerNavigationBarFragment.FAVOURITES_IDX);
    }

    @NonNull
    @Override
    public Loader<ArrayList<MiniCouponInfo>> onCreateLoader(int id, @Nullable Bundle args) {
        switch (id) {
            case GET_FAVOURITE_COUPONS_LOADER_ID:
                return new CouponUtils.GetFavouriteCouponsTask(this);
            case ADD_COUPON_TO_FAVOURITE_LIST_LOADER_ID:
                if (args == null || !args.containsKey(COUPON_ID_KEY))
                    throw new IllegalArgumentException("Arguments must not be null and contain COUPON_ID_KEY key.");

                int couponId = args.getInt(COUPON_ID_KEY);
                return new CouponUtils.GetCouponByIdTask(this, couponId);
            default:
                throw new UnsupportedOperationException("FavouriteCouponsActivity: onCreateLoader: No handler for id " + id);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MiniCouponInfo>> loader, ArrayList<MiniCouponInfo> data) {
        switch (loader.getId()) {
            case GET_FAVOURITE_COUPONS_LOADER_ID:
                loadingBar.setVisibility(View.GONE);
                if (data == null)
                    Utils.showErrorMessage(FavouriteCouponsActivity.this, body, R.drawable.ic_warning, getString(R.string.message_server_data_error, ""), true);
                else if (data.size() == 0)
                    Utils.showErrorMessage(FavouriteCouponsActivity.this, body, R.drawable.ic_frown, getString(R.string.message_no_favourites), true);
                else
                    fillFavouriteCoupons(data);
                break;
            case ADD_COUPON_TO_FAVOURITE_LIST_LOADER_ID:
                fragment.addItem(data.get(0));
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MiniCouponInfo>> loader) {

    }

    @Override
    public void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo) {

    }

    @Override
    public void onOwnerCouponRemoved(int couponId) {

    }

    @Override
    public void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo) {

    }

    @Override
    public void onOwnerBusinessRemoved(int businessId) {

    }

    @Override
    public void onOwnerAboutDataChanged(OwnerAboutInfo newInfo) {

    }

    @Override
    public void onStarChanged(final int couponId, final CouponUtils.StarredState state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state == UNSTARRED) {
                    fragment.removeCoupon(couponId);
                } else {
                    Bundle args = new Bundle();
                    args.putInt(COUPON_ID_KEY, couponId);

                    getSupportLoaderManager().restartLoader(ADD_COUPON_TO_FAVOURITE_LIST_LOADER_ID, args, FavouriteCouponsActivity.this).forceLoad();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (userDataCenter != null)
            userDataCenter.unregisterDataChangedListenerCallback(this);
    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.userDataCenter = userDataCenter;

        userDataCenter.registerDataChangedListenerCallback(this);
        getSupportLoaderManager().restartLoader(GET_FAVOURITE_COUPONS_LOADER_ID, null, this).forceLoad();
    }

    @Override
    public void onError(String message) {
        Utils.showErrorMessage(this, body, R.drawable.ic_frown, getString(R.string.message_server_connection_error), true);
    }
}
