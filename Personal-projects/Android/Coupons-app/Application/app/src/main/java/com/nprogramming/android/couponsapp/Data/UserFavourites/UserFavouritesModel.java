package com.nprogramming.android.couponsapp.Data.UserFavourites;

import android.arch.lifecycle.ViewModel;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class UserFavouritesModel extends ViewModel {
    @PrimaryKey(autoGenerate = true)
    int id;

    private int userId;

    private int couponId;

    public UserFavouritesModel(int userId, int couponId) {
        this.userId = userId;
        this.couponId = couponId;
    }

    public UserFavouritesModel() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }
}
