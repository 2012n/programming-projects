package com.nprogramming.android.couponsapp.Activities;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.nprogramming.android.couponsapp.Adapters.MiniCouponsRecyclerViewAdapter;
import com.nprogramming.android.couponsapp.Fragments.LowerNavigationBarFragment;
import com.nprogramming.android.couponsapp.Fragments.MiniCouponsListFragment;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.ArrayList;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_COUPONS_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.PAGE_NUMBER_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.NEARBY_COUPONS_PAGE_ITEM_COUNT;
import static com.nprogramming.android.couponsapp.Utils.Constants.FRAGMENT_TAGS.NEARBY_COUPONS_FRAGMENT_TAG;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.GET_NEARBY_COUPONS_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.NEARBY_COUPONS_PAGE_LOADER_ID;

public class NearbyCouponsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<MiniCouponInfo>>, MiniCouponsListFragment.OnScrollReachedEndListener {
    private MiniCouponsListFragment miniCouponsListFragment;
    private LinearLayout rootLayout;
    private ProgressBar progressBar;
    private Location phoneLocation;
    private int pageNumber;

    private boolean SCROLL_LOADING_LOCK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by);

        initializeMembers();
        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_nearby_activity));
        setNavigationBar();
    }

    private void initializeMembers() {
        rootLayout = findViewById(R.id.activity_nearby_root_layout);
        progressBar = findViewById(R.id.progressBar_activity_near_by);
        pageNumber = 1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);

        phoneLocation = Utils.getPhoneLocation(this);
        if (phoneLocation == null) { // Cannot get phone location
            if (getSupportFragmentManager().findFragmentByTag(NEARBY_COUPONS_FRAGMENT_TAG) != null)
                getSupportFragmentManager().beginTransaction().remove(miniCouponsListFragment).commit();
            Utils.showErrorMessage(this, rootLayout, R.drawable.ic_warning, getString(R.string.message_enable_location), true);
        } else { // Phone location granted
            getSupportLoaderManager().restartLoader(GET_NEARBY_COUPONS_LOADER_ID, null, this).forceLoad();
        }
    }

    private void setNavigationBar() {
        LowerNavigationBarFragment fragment = (LowerNavigationBarFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_nearby_lower_bar);
        LowerNavigationBarFragment.setNavItemActiveColor(fragment, LowerNavigationBarFragment.NEARBY_IDX);
    }

    private void addNearbyCouponsFragment(ArrayList<MiniCouponInfo> nearbyCoupons) {
        miniCouponsListFragment = new MiniCouponsListFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(ARGUMENTS_COUPONS_KEY, nearbyCoupons);

        miniCouponsListFragment.setArguments(arguments);
        miniCouponsListFragment.setOnScrollReachedEndListener(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.activity_nearby_root_layout, miniCouponsListFragment, NEARBY_COUPONS_FRAGMENT_TAG);
        transaction.commit();
    }

    @NonNull
    @Override
    public Loader<ArrayList<MiniCouponInfo>> onCreateLoader(int id, @Nullable Bundle args) {
        switch (id) {
            case NEARBY_COUPONS_PAGE_LOADER_ID:
                if (args == null || !args.containsKey(PAGE_NUMBER_KEY))
                    throw new IllegalArgumentException("Argument must be passed and contain the PAGE_NUMBER_KEY key.");
            case GET_NEARBY_COUPONS_LOADER_ID: // Allow fallthrough
                pageNumber = 1;
                if (id == NEARBY_COUPONS_PAGE_LOADER_ID)
                    pageNumber = args.getInt(PAGE_NUMBER_KEY);

                Context context = NearbyCouponsActivity.this;
                return new CouponUtils.GetNearByCouponsTask(context, Utils.getPhoneLocation(context), pageNumber, NEARBY_COUPONS_PAGE_ITEM_COUNT);
            default:
                throw new IllegalArgumentException(String.format("Loader ID %d is not defined and cannot be handled.", id));
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MiniCouponInfo>> loader, ArrayList<MiniCouponInfo> data) {
        switch (loader.getId()) {
            case GET_NEARBY_COUPONS_LOADER_ID:
            case NEARBY_COUPONS_PAGE_LOADER_ID:
                if (data == null) {
                    Utils.showErrorMessage(this, rootLayout, R.drawable.ic_frown, getString(R.string.message_server_data_error, ""), true);
                    return;
                }

                progressBar.setVisibility(View.GONE);

                if (data.size() == 0) {
                    if (loader.getId() == GET_NEARBY_COUPONS_LOADER_ID)
                        Utils.showErrorMessage(NearbyCouponsActivity.this, rootLayout, R.drawable.ic_frown, getString(R.string.message_no_results), true);
                    else
                        miniCouponsListFragment.removeAllProgressBars();
                } else {
                    if (getSupportFragmentManager().findFragmentByTag(NEARBY_COUPONS_FRAGMENT_TAG) == null) {
                        addNearbyCouponsFragment(data);
                    } else {// Fragment exists

                        if (loader.getId() == GET_NEARBY_COUPONS_LOADER_ID)
                            miniCouponsListFragment.setInitialData(data); // onResume
                        else {
                            miniCouponsListFragment.removeAllProgressBars();
                            miniCouponsListFragment.addItems(data); // new page scroll
                        }
                    }
                }

                SCROLL_LOADING_LOCK = false;
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MiniCouponInfo>> loader) {

    }

    @Override
    public void onScrollReachedTopLimit() {

    }

    @Override
    public void onScrollReachedBottomLimit() {
        if (SCROLL_LOADING_LOCK)
            return; // Loader in progress

        SCROLL_LOADING_LOCK = true;

        Bundle args = new Bundle();
        args.putInt(PAGE_NUMBER_KEY, ++pageNumber);

        miniCouponsListFragment.addItem(new MiniCouponsRecyclerViewAdapter.ProgressBarItem());
        getSupportLoaderManager().restartLoader(NEARBY_COUPONS_PAGE_LOADER_ID, args, this).forceLoad();
    }
}
