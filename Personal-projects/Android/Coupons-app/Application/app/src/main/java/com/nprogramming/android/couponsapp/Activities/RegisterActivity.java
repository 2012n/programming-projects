package com.nprogramming.android.couponsapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.RegisterUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.nprogramming.android.couponsapp.Utils.Utils.DatePickerSettings;

import java.util.Locale;
import java.util.Objects;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.PASSWORD_MIN_LENGTH;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.REGISTER_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BIRTHDAY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.EMAIL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.FIRSTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LASTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PASSWORD_PARAM;

public class RegisterActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ServerResponse> {
    private EditText etDatePicker, etFirstName, etLastName, etPassword, etConfirmPassword, etEmail;
    private TextView tvErrorMessage;
    private CheckBox ckbTermsAgreement;
    private Button submitButton;
    private ProgressBar progressBarLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LoginUtils.isUserLoggedIn(this)) {
            finish();
            return;
        }


        setContentView(R.layout.activity_register);

        setupViews();
        setSubmitCallback();
        Utils.setupActionBarTitle(Objects.requireNonNull(getSupportActionBar()), getString(R.string.title_register_activity));
        Utils.attachDatepickerToEditText(this, etDatePicker, DatePickerSettings.PAST_ONLY);
    }

    /*
    Private helpers
     */

    /**
     * Binds the views into the appropriate variables.
     */

    private void setupViews() {
        etDatePicker = findViewById(R.id.editText_datepicker);
        etFirstName = findViewById(R.id.editText_firstname);
        etLastName = findViewById(R.id.editText_lastname);
        etEmail = findViewById(R.id.editText_register_email);
        etPassword = findViewById(R.id.editText_register_password);
        etConfirmPassword = findViewById(R.id.editText_confirm_password);

        tvErrorMessage = findViewById(R.id.textView_register_error_message);

        ckbTermsAgreement = findViewById(R.id.checkBox_term_agreement);

        submitButton = findViewById(R.id.button_submit);

        progressBarLoading = findViewById(R.id.progressBar_loading);
    }

    /**
     * Sets the callback on form submittion button.
     */

    private void setSubmitCallback() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorMessages = validateFormData();
                if (errorMessages.length() != 0) {
                    tvErrorMessage.setText(errorMessages);
                    tvErrorMessage.setVisibility(View.VISIBLE);
                    return;
                }

                Bundle args = getViewsDataBundle();
                getSupportLoaderManager().restartLoader(REGISTER_LOADER_ID, args, RegisterActivity.this).forceLoad();
            }
        });
    }

    /**
     * Validates the form data.
     *
     * @return String of all detected errors.
     */

    private String validateFormData() {
        StringBuilder errorMessages = new StringBuilder();
        String firstname = etFirstName.getText().toString();
        String lastname = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        boolean birthdayEmpty = etDatePicker.getText().toString().length() == 0;
        boolean isAgreedToTerms = ckbTermsAgreement.isChecked();
        int errorCount = 0;

        if (firstname.isEmpty())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_first_name_empty)));
        if (lastname.isEmpty())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_last_name_empty)));
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_bad_email_format)));
        if (password.length() < PASSWORD_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_password_too_short)));
        if (!password.equals(confirmPassword))
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_passwords_not_match)));
        if (birthdayEmpty)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_birthday_empty)));
        if (!isAgreedToTerms)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_terms_not_agreed)));

        return errorMessages.toString().trim();
    }

    /**
     * Builds a bundle for the form views.
     *
     * @return Bundle with form views data.
     */

    private Bundle getViewsDataBundle() {
        Bundle args = new Bundle();

        args.putString(FIRSTNAME_PARAM, etFirstName.getText().toString());
        args.putString(LASTNAME_PARAM, etLastName.getText().toString());
        args.putString(EMAIL_PARAM, etEmail.getText().toString());
        args.putString(PASSWORD_PARAM, etPassword.getText().toString());
        args.putString(BIRTHDAY_PARAM, etDatePicker.getText().toString());

        return args;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.register_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.register_activity_menu_item_login:
                showLoginForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showLoginForm() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    private void showOwnerProfilePage() {
        Intent ownerProfilePage = new Intent(RegisterActivity.this, OwnerProfileActivity.class);
        startActivity(ownerProfilePage);
        finish();
    }

    @NonNull
    @Override
    public Loader<ServerResponse> onCreateLoader(int id, Bundle args) {
        String firstname = args.getString(FIRSTNAME_PARAM, null);
        String lastname = args.getString(LASTNAME_PARAM, null);
        String email = args.getString(EMAIL_PARAM, null);
        String password = args.getString(PASSWORD_PARAM, null);
        String birthday = args.getString(BIRTHDAY_PARAM, null);

        tvErrorMessage.setVisibility(View.INVISIBLE);
        progressBarLoading.setVisibility(View.VISIBLE);

        return new RegisterUtils.RegisterTask(this, firstname, lastname, email, password, birthday);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ServerResponse> loader, ServerResponse response) {
        progressBarLoading.setVisibility(View.GONE);

        if (response == ServerResponse.OPERATION_SUCCESS) {
            Toast.makeText(RegisterActivity.this, getString(R.string.message_registered_successfully), Toast.LENGTH_LONG).show();
            showOwnerProfilePage();
        } else {
            tvErrorMessage.setText(getString(R.string.message_server_response, response.toString()));
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ServerResponse> loader) {
    }
}
