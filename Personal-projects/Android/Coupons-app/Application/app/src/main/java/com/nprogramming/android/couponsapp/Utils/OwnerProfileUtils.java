package com.nprogramming.android.couponsapp.Utils;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import com.nprogramming.android.couponsapp.Exceptions.APIOperationFailedException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo.Gender;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BIRTHDAY_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_CATEGORY_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_NAME_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_RATING_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.COUPON_TITLE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DATA_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DESCRIPTION_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.EMAIL_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.FACEBOOK_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.FIRST_NAME_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.GENDER_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.GRADUATED_OF_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LANGUAGES_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LAST_NAME_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LAST_VALIDITY_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LATITUDE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LIVES_IN_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LONGITUDE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.PHONE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.PROFILE_PICTURE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_PORT;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.EMAIL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.FACEBOOK_PROFILE_URL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.FIRSTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.GENDER_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.GRADUATED_OF_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.ID_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LANGUAGES_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LASTNAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LIVING_CITY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.OWNER_PROFILE_PICTURE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PHONE_NUMBER_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.BUSINESS_PROFILE_PICTURE_URL;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.OWNER_DATA_INFO_EDIT_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.OWNER_PROFILE_PICTURE_URL;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.PROFILE_BUSINESSES_OWNING_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.PROFILE_COUPONS_OWNING_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.PROFILE_DATA_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.UPLOAD_PROFILE_PICTURE_PATH;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;

public class OwnerProfileUtils {
    private static final String TAG = OwnerProfileUtils.class.getSimpleName();

    public static OwnerAboutInfo getOwnerAboutDataByOwnerId(int ownerId) throws APIOperationFailedException {
        try {
            HttpUrl url = NetworkUtils
                    .getBaseHttpUrlBuilder(PROFILE_DATA_PATH)
                    .addQueryParameter(ID_PARAM, ownerId + "")
                    .build();

            Request request = new Request.Builder().get().url(url).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject jsonResponse = new JSONObject(responseString);

            ServerResponse responseStatus = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));

            if (!response.isSuccessful())
                throw new APIOperationFailedException("getOwnerAboutDataByOwnerId: Operating failed: " + response.code(), responseStatus);

            if (responseStatus == ServerResponse.OPERATION_SUCCESS) {
                JSONObject data = jsonResponse.getJSONObject(DATA_FIELD);

                return new OwnerAboutInfo(
                        ownerId,
                        String.format(OWNER_PROFILE_PICTURE_URL, data.optInt(PROFILE_PICTURE_FIELD)),
                        data.optString(FIRST_NAME_FIELD),
                        data.optString(LAST_NAME_FIELD),
                        data.optString(LIVES_IN_FIELD),
                        data.optInt(GENDER_FIELD) == Gender.Male.ordinal() ? Gender.Male : Gender.Female,
                        data.optString(LANGUAGES_FIELD),
                        data.optString(GRADUATED_OF_FIELD),
                        data.optString(PHONE_FIELD),
                        data.optString(EMAIL_FIELD),
                        data.optString(FACEBOOK_FIELD),
                        data.optLong(BIRTHDAY_FIELD));
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new APIOperationFailedException("getOwnerAboutDataByOwnerId: Operation failed: " + e.getMessage(),
                    e instanceof IOException ? ServerResponse.CONNECTION_ERROR : ServerResponse.UNKNOWN_ERROR);
        }
    }

    public static ArrayList<MiniCouponInfo> getOwnerCouponsByOwnerId(int ownerId) throws APIOperationFailedException {
        try {
            HttpUrl url = NetworkUtils
                    .getBaseHttpUrlBuilder(PROFILE_COUPONS_OWNING_PATH)
                    .addQueryParameter(ID_PARAM, ownerId + "")
                    .build();

            Request request = new Request.Builder().get().url(url).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject jsonResponse = new JSONObject(responseString);

            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));

            if (!response.isSuccessful())
                throw new APIOperationFailedException("getOwnerCouponById: network operating was unsuccessful: " + response.code(), serverResponse);

            if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                ArrayList<MiniCouponInfo> miniCouponInfos = new ArrayList<>();
                JSONArray data = jsonResponse.getJSONArray(DATA_FIELD);

                for (int i = 0; i < data.length(); i++) {
                    try {
                        JSONObject coupon = data.getJSONObject(i);
                        miniCouponInfos.add(
                                new MiniCouponInfo(
                                        coupon.optInt(ID_FIELD),
                                        coupon.optInt(BUSINESS_ID_FIELD),
                                        String.format(BUSINESS_PROFILE_PICTURE_URL, coupon.optInt(BUSINESS_ID_FIELD)),
                                        coupon.optString(COUPON_TITLE_FIELD),
                                        coupon.optString(BUSINESS_NAME_FIELD),
                                        coupon.optLong(LAST_VALIDITY_FIELD),
                                        coupon.optString(DESCRIPTION_FIELD),
                                        (float) coupon.optDouble(LATITUDE_FIELD),
                                        (float) coupon.optDouble(LONGITUDE_FIELD)
                                )
                        );
                    } catch (JSONException ex) {
                        Log.w(TAG, String.format("Warning in getOwnerCouponsById: The %d'th element in JSON array is not a json. moving on", i));
                        ex.printStackTrace();
                    }
                }
                return miniCouponInfos;
            } else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new APIOperationFailedException("getOwnerCouponsByOwnerId: Operation failed: " + e.getMessage(),
                    e instanceof IOException ? ServerResponse.CONNECTION_ERROR : ServerResponse.UNKNOWN_ERROR);
        }
    }

    public static ArrayList<MiniBusinessInfo> getOwnerBusinessesByOwnerId(int ownerId) throws APIOperationFailedException {
        try {
            HttpUrl url = NetworkUtils
                    .getBaseHttpUrlBuilder(PROFILE_BUSINESSES_OWNING_PATH)
                    .addQueryParameter(ID_PARAM, ownerId + "")
                    .build();

            Request request = new Request.Builder().get().url(url).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String responseString = responseBody.string();
            JSONObject jsonResponse = new JSONObject(responseString);

            Log.v("eragrea", responseString);
            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));

            if (!response.isSuccessful())
                throw new APIOperationFailedException("getOwnerBusinessesByOwnerId: operating failed: " + response.code(), serverResponse);

            if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                ArrayList<MiniBusinessInfo> miniBusinessInfos = new ArrayList<>();
                JSONArray data = jsonResponse.getJSONArray(DATA_FIELD);

                for (int i = 0; i < data.length(); i++) {
                    try {
                        JSONObject business = data.getJSONObject(i);
                        miniBusinessInfos.add(
                                new MiniBusinessInfo(
                                        business.optInt(ID_FIELD),
                                        business.optString(BUSINESS_NAME_FIELD),
                                        business.optInt(BUSINESS_CATEGORY_FIELD),
                                        (float) business.optDouble(BUSINESS_RATING_FIELD)
                                )
                        );
                    } catch (JSONException ex) {
                        Log.w(TAG, String.format("Warning in getOwnerBusinessesById: The %d'th element in JSON array is not a json. moving on", i));
                        ex.printStackTrace();
                    }
                }
                return miniBusinessInfos;
            } else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new APIOperationFailedException("getOwnerBusinessesByOwnerId: Operation failed: " + e.getMessage(),
                    e instanceof IOException ? ServerResponse.CONNECTION_ERROR : ServerResponse.UNKNOWN_ERROR);
        }
    }

    private static ServerResponse updateOwnerProfilePicture(int profileId, String imagePath) {
        try {
            MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/*");
            String filename = String.format(Locale.getDefault(), "owner-%d", profileId);
            File imageFile = new File(imagePath);

            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(UPLOAD_PROFILE_PICTURE_PATH).build();

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(OWNER_PROFILE_PICTURE_PARAM, filename, RequestBody.create(MEDIA_TYPE_IMAGE, imageFile))
                    .build();

            Request request = new Request.Builder().url(url).post(requestBody).build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();

            JSONObject jsonObject = new JSONObject(data);
            return ServerResponse.getServerResponseByIndex(jsonObject.getInt(Utils.RESPONSE_CODE_FIELD));
        } catch (Exception ex) {
            Log.e(TAG, "Error in updateOwnerProfilePicture: " + ex.getMessage());
            ex.printStackTrace();
            return ServerResponse.UNKNOWN_ERROR;
        }
    }

    public static class UpdateOwnerProfilePictureTask extends AsyncTaskLoader<ServerResponse> {
        final int profileId;
        final String imagePath;

        public UpdateOwnerProfilePictureTask(Context context, int profileId, String imagePath) {
            super(context);
            this.profileId = profileId;
            this.imagePath = imagePath;
        }

        @Nullable
        @Override
        public ServerResponse loadInBackground() {
            return updateOwnerProfilePicture(profileId, imagePath);
        }
    }

    public static class SendOwnerUpdatedInfoTask extends AsyncTaskLoader<ServerResponse> {
        public final OwnerAboutInfo updatedAboutInfo;

        public SendOwnerUpdatedInfoTask(Context context, OwnerAboutInfo updatedAboutInfo) {
            super(context);
            this.updatedAboutInfo = updatedAboutInfo;
        }

        @Nullable
        @Override
        public ServerResponse loadInBackground() {
            try {
                HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(OWNER_DATA_INFO_EDIT_PATH).build();

                String[] paramsKeys = new String[]{EMAIL_PARAM, FIRSTNAME_PARAM, LASTNAME_PARAM, GENDER_PARAM, LANGUAGES_PARAM, GRADUATED_OF_PARAM, PHONE_NUMBER_PARAM, FACEBOOK_PROFILE_URL_PARAM, LIVING_CITY_PARAM};
                String[] paramValues = new String[]{updatedAboutInfo.getEmail(), updatedAboutInfo.getFirstName(), updatedAboutInfo.getLastName(), updatedAboutInfo.getGender().ordinal() + "", updatedAboutInfo.getLanguages(), updatedAboutInfo.getGraduatedOf(), updatedAboutInfo.getPhone(), updatedAboutInfo.getFacebook(), updatedAboutInfo.getLivesIn()};

                FormBody.Builder requestBodyBuilder = new FormBody.Builder();

                boolean anyChange = false;
                for (int i = 0; i < paramsKeys.length; i++) {
                    if (paramsKeys[i] != null) {
                        requestBodyBuilder.add(paramsKeys[i], paramValues[i].trim());
                        anyChange = true;
                    }
                }

                if (!anyChange) return null; // Nothing to update
                requestBodyBuilder.add(ID_PARAM, updatedAboutInfo.getUserId() + "");

                Request request = new Request.Builder().post(requestBodyBuilder.build()).url(url).build();

                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();

                if (responseBody == null)
                    throw new NetworkErrorException("Could not retrieve response.");

                String responseString = responseBody.string();
                JSONObject jsonResponse = new JSONObject(responseString);
                ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(jsonResponse.getInt(RESPONSE_CODE_FIELD));

                if (!response.isSuccessful())
                    throw new APIOperationFailedException("SendOwnerUpdatedInfoTask: server responded bad http code: " + response.code(), serverResponse);

                return serverResponse;

            } catch (Exception ex) {
                Log.e(TAG, "SendOwnerUpdatedInfoTask: Exception thrown: " + ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        }
    }
}