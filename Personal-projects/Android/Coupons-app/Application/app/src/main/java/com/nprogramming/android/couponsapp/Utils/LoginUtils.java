package com.nprogramming.android.couponsapp.Utils;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.nprogramming.android.couponsapp.Activities.LoginActivity;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.RESPONSE_USER_ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_PORT;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.EMAIL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PASSWORD_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.LOGIN_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.STORAGE_SETTINGS.PERSISTANT_STORAGE_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.STORAGE_SETTINGS.PERSISTANT_STORAGE_USER_ID_KEY;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;


public class LoginUtils {
    /**
     * A utility for log in actions.
     */

    private static final int IMPOSSIBLE_USER_ID = -1;

    private static final String TAG = LoginUtils.class.getSimpleName();

    public enum LoginResponseStatus {
        LOGIN_SUCCESSFUL,
        LOGIN_UNAUTHORIZED,
        LOGIN_BAD_CREDENTIALS,
        LOGIN_SERVER_ERROR,
        LOGIN_CONNECTION_ERROR,
        LOGIN_UNKNOWN_ERROR
    }

    /**
     * Tried to log in with given email and password.
     *
     * @param context  Calling activity context.
     * @param email    user's email.
     * @param password user's password.
     * @return LoginResponseStatus object that describes login result.
     */

    public static LoginResponseData login(Context context, String email, String password) {
        try {
            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(LOGIN_PATH).build();

            RequestBody requestBody = new FormBody.Builder()
                    .add(EMAIL_PARAM, email.trim())
                    .add(PASSWORD_PARAM, password)
                    .build();

            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(url)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();
            JSONObject jsonObject = new JSONObject(data);

            if (!response.isSuccessful())
                return new LoginResponseData(LoginResponseStatus.LOGIN_SERVER_ERROR, -1);


            ServerResponse serverResponse = ServerResponse.values()[jsonObject.getInt(RESPONSE_CODE_FIELD)];

            if (serverResponse == ServerResponse.OWNER_PROFILE_AUTHENTICATION_SUCCSS) {
                int userId = jsonObject.optInt(RESPONSE_USER_ID_FIELD, IMPOSSIBLE_USER_ID);
                SharedPreferences sharedPreferences = context.getSharedPreferences(PERSISTANT_STORAGE_KEY, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(PERSISTANT_STORAGE_USER_ID_KEY, userId);
                editor.apply();

                return new LoginResponseData(
                        LoginResponseStatus.LOGIN_SUCCESSFUL,
                        userId);
            } else {
                return new LoginResponseData(
                        LoginResponseStatus.LOGIN_BAD_CREDENTIALS,
                        -1);
            }
        } catch (Exception exception) {
            Log.e(TAG, "Error in Login: " + exception.getMessage());
            exception.printStackTrace();
            return new LoginResponseData(LoginResponseStatus.LOGIN_CONNECTION_ERROR, -1);
        }
    }

    /**
     * Gets the logged in user's id.
     *
     * @param context context.
     * @return logged in user's id if logged in; otherwise IMPOSSIBLE_USER_ID.
     */

    public static int getLoggedInUserId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PERSISTANT_STORAGE_KEY, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(PERSISTANT_STORAGE_USER_ID_KEY, IMPOSSIBLE_USER_ID);
    }

    /**
     * Determines weather a owner profile is logged in now or not.
     *
     * @param context context.
     * @return true if user logged in; otherwise false.
     */

    public static boolean isUserLoggedIn(Context context) {
        return getLoggedInUserId(context) != IMPOSSIBLE_USER_ID;
    }

    /**
     * Logged the user out.
     *
     * @param parent - the activity that logged out..
     */

    public static void logout(Activity parent) {
        if (parent == null)
            throw new IllegalArgumentException("parent cannot be null.");

        SharedPreferences sharedPreferences = parent.getSharedPreferences(PERSISTANT_STORAGE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PERSISTANT_STORAGE_USER_ID_KEY);
        editor.apply();

        // Close activity
        parent.finishAffinity();

        // Open the login activity
        Intent intent = new Intent(parent, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        parent.startActivity(intent);
    }


    public static class LoginTask extends AsyncTaskLoader<LoginResponseData> {
        final String email;
        final String password;

        public LoginTask(Context context, String email, String password) {
            super(context);
            this.email = email;
            this.password = password;
        }

        @Nullable
        @Override
        public LoginResponseData loadInBackground() {
            return LoginUtils.login(getContext(), this.email, this.password);
        }
    }

    public static class LoginResponseData {
        public final LoginUtils.LoginResponseStatus loginResponseStatus;
        public final int userId;

        public LoginResponseData(LoginUtils.LoginResponseStatus loginResponseStatus, int userId) {
            this.loginResponseStatus = loginResponseStatus;
            this.userId = userId;
        }

        @Override
        public String toString() {
            return loginResponseStatus.toString();
        }
    }
}


