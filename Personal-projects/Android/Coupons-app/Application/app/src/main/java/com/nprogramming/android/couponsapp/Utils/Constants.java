package com.nprogramming.android.couponsapp.Utils;

import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;

public class Constants {
    public static final class SERVER_INFO {
        public static final String SERVER_HOST = "192.168.1.105";
        public static final int SERVER_PORT = 3000;
    }

    public static final class SERVER_REQUEST_PATHS {
        public static final String LOGIN_PATH = "authenticatebusinessowner";
        public static final String PROFILE_DATA_PATH = "businessownerdata";
        public static final String PROFILE_COUPONS_OWNING_PATH = "ownercoupons";
        public static final String GET_BUSINESS_BY_ID_PATH = "business";
        public static final String SEARCH_COUPONS_PATH = "searchcoupons";
        public static final String GET_COUPON_BY_ID_PATH = "coupon";
        public static final String PROFILE_BUSINESSES_OWNING_PATH = "ownerbusinesses";
        public static final String ADD_BUSINESS_PATH = "registerbusiness";
        public static final String ADD_COUPON_PATH = "registercoupon";
        public static final String OWNER_DATA_INFO_EDIT_PATH = "updateownerprofile";
        public static final String REGISTER_PATH = "registerbusinessowner";
        public static final String UPLOAD_PROFILE_PICTURE_PATH = "uploadprofilepicture";
        public static final String NEARBY_COUPONS_PATH = "nearbycoupons";

        public static final String BUSINESS_PROFILE_PICTURE_URL = String.format(Locale.getDefault(), "http://%s:%d/business/%%d", SERVER_HOST, SERVER_INFO.SERVER_PORT);
        public static final String OWNER_PROFILE_PICTURE_URL = String.format(Locale.getDefault(), "http://%s:%d/owner/%%d", SERVER_HOST, SERVER_INFO.SERVER_PORT);

    }

    public static final class SERVER_REQUEST_PARAMETERS {
        public static final String ID_PARAM = "id";
        public static final String OWNER_ID_PARAM = "ownerId";
        public static final String BUSINESS_NAME_PARAM = "businessName";
        public static final String CATEGORY_PARAM = "category";
        public static final String DESCRIPTION_PARAM = "description";
        public static final String ADDRESS_PARAM = "address";
        public static final String LATITUDE_PARAM = "latitude";
        public static final String LONGITUDE_PARAM = "longitude";
        public static final String EMAIL_PARAM = "emailAddress";
        public static final String PASSWORD_PARAM = "password";
        public static final String FIRSTNAME_PARAM = "firstname";
        public static final String LASTNAME_PARAM = "lastname";
        public static final String BIRTHDAY_PARAM = "birthday";
        public static final String LAST_VALIDITY_PARAM = "lastValidity";
        public static final String COUPON_TITLE_PARAM = "title";
        public static final String BUSINESS_ID_PARAM = "businessId";
        public static final String BUSINESS_LOGO_PICTURE_PARAM = "businessLogo";
        public static final String OWNER_PROFILE_PICTURE_PARAM = "profilePicture";
        public static final String RATING_PARAM = "rating";
        public static final String RADIUS_PARAM = "radius";
        public static final String OPENING_TIMES_PARAM = "openingTimes";
        public static final String GENDER_PARAM = "gender";
        public static final String LANGUAGES_PARAM = "languages";
        public static final String GRADUATED_OF_PARAM = "graduatedOf";
        public static final String PHONE_NUMBER_PARAM = "phoneNumber";
        public static final String FACEBOOK_PROFILE_URL_PARAM = "facebookProfileUrl";
        public static final String LIVING_CITY_PARAM = "livingCity";
        public static final String PAGE_NUMBER_PARAM = "page";
        public static final String PAGE_ITEM_COUNT_PARAM = "count";
    }

    public static final class DATA_FIELDS {
        public static final String ID_FIELD = "id";
        public static final String BUSINESS_CATEGORY_FIELD = "category";
        public static final String BUSINESS_RATING_FIELD = "rating";
        public static final String BUSINESS_ID_FIELD = "businessId";
        public static final String COUPON_TITLE_FIELD = "title";
        public static final String BUSINESS_NAME_FIELD = "businessName";
        public static final String LAST_VALIDITY_FIELD = "lastValidity";
        public static final String DESCRIPTION_FIELD = "description";
        public static final String PROFILE_PICTURE_FIELD = "id";
        public static final String FIRST_NAME_FIELD = "firstname";
        public static final String LAST_NAME_FIELD = "lastname";
        public static final String LIVES_IN_FIELD = "livingCity";
        public static final String GENDER_FIELD = "gender";
        public static final String LANGUAGES_FIELD = "languages";
        public static final String GRADUATED_OF_FIELD = "graduatedOf";
        public static final String PHONE_FIELD = "phoneNumber";
        public static final String EMAIL_FIELD = "emailAddress";
        public static final String FACEBOOK_FIELD = "facebookProfileUrl";
        public static final String BIRTHDAY_FIELD = "birthday";
        public static final String RESPONSE_USER_ID_FIELD = "profileId";
        public static final String ADDRESS_FIELD = "address";
        public static final String LATITUDE_FIELD = "latitude";
        public static final String LONGITUDE_FIELD = "longitude";
        public static final String CRETIRIAS_FIELD = "searchCretirias";
        public static final String BUSINESS_OPENING_TIMES_FIELD = "openingTimes";
        public static final String DATA_FIELD = "data";


        public static final String SUN_KEY = "sun";
        public static final String MON_KEY = "mon";
        public static final String TUE_KEY = "tue";
        public static final String WED_KEY = "wed";
        public static final String THU_KEY = "thu";
        public static final String FRI_KEY = "fri";
        public static final String SAT_KEY = "sat";

        public static final String[] DAYS_KEYS = new String[]{SUN_KEY, MON_KEY, TUE_KEY, WED_KEY, THU_KEY, FRI_KEY, SAT_KEY};
    }

    public static final class FORMATTING {
        public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
        public static final String BUSINESS_RATING_FORMAT = "%.1f";
        public static final String BUSINESS_OPENING_TIMES_DISPLAY_FORMAT = "%s: %s";
        public static final String COUPON_END_DATE_FORMAT = "Ends on %s";
        public static final String BUSINESS_SPINNER_ITEM_DISPLAY_FORMAT = "[#%d] %s";
        public static final String OPENING_TIME_REGEX_FORMAT = "^(([0-1]\\d)|([2][0-3])):([0-5]\\d)-(([0-1]\\d)|([2][0-3])):([0-5]\\d)$"; // HH:MM-HH:MM
        public static final String MINI_COUPON_DATE_STYLE_FORMAT = "dd MM";
        public static final int BUSINESS_NAME_MIN_LENGTH = 4;
        public static final int FIRSTNAME_MIN_LENGTH = 2;
        public static final int LASTNAME_MIN_LENGTH = 2;
        public static final int BUSINESS_DESCRIPTION_MIN_LENGTH = 10;
        public static final int PASSWORD_MIN_LENGTH = 4;
        public static final int MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH = 50;
        public static final int MINI_COUPON_DISPLAY_TITLE_MAX_LENGTH = 14;
        public static final int NEARBY_COUPONS_PAGE_ITEM_COUNT = 10;

        public static final float MINI_COUPON_DISPLAY_MAX_DISTANCE_FOR_METERS = 700.0f;
    }

    public static final class LOADER_IDS {
        public static final int SUBMIT_ADD_BUSINESS_LOADER_ID = 0;
        public static final int LOGIN_LOADER_ID = 1;
        public static final int REGISTER_LOADER_ID = 2;
        public static final int SUBMIT_OWNER_PROFILE_EDIT_LOADER_ID = 3;
        public static final int UPDATE_USER_PROFILE_PICTURE_LOADER_ID = 4;
        public static final int BUSINESS_PAGE_DATA_LOADER_ID = 5;
        public static final int COUPON_DETAILED_DATA_LODER_ID = 6;
        public static final int ADD_NEW_COUPON_LOADER_ID = 7;
        public static final int SEARCH_COUPONS_LOADER_ID = 8;
        public static final int GET_FAVOURITE_COUPONS_LOADER_ID = 9;
        public static final int ADD_COUPON_TO_FAVOURITE_LIST_LOADER_ID = 10;
        public static final int GET_NEARBY_COUPONS_LOADER_ID = 11;
        public static final int NEARBY_COUPONS_PAGE_LOADER_ID = 12;
    }

    public static final class EXTRA_KEYS {
        public static final String CURRENT_OWNER_ABOUT_INFO_EXTRA_KEY = "currentInfoExtra";
        public static final String ARGUMENTS_COUPONS_KEY = "owningCouponsProfileKey";
        public static final String IMAGE_PICKER_RESULT_IMAGE_PATH_KEY = "imagePickerPathKey";
        public static final String BUSINESS_ID_KEY = "businessId";
        public static final String COUPON_INFO_KEY = "couponInfo";
        public static final String ARGUMENTS_PHONE_LOCATION_KEY = "phoneLocation";
        public static final String COUPON_DATA_KEY = "couponData";
        public static final String BUSINESS_DATA_KEY = "businessData";
        public static final String COUPON_ID_KEY = "couponId";
        public static final String PAGE_NUMBER_KEY = "pageNumberKey";
    }

    public static final class REQUEST_IDS {
        public static final int NEW_BUSINESS_PLACE_PICKER_REQUEST = 1;
        public static final int NEW_BUSINESS_PROFILE_PICK_IMAGE_REQUEST = 2;
        public static final int NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST = 3;
        public static final int EDIT_OWNER_PROFILE_PICTURE_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST = 4;
        public static final int EDIT_OWNER_PROFILE_PICTURE_PICK_IMAGE_REQUEST = 5;
        public static final int PREFERENCE_LOCATION_PERMISSION_REQUEST = 6;
        public static final int ADD_NEW_BUSINESS_REQUEST = 7;
    }

    public static final class STORAGE_SETTINGS {
        public static final String PERSISTANT_STORAGE_KEY = Constants.class.getPackage().getName() + ".PERSISTANT_STORAGE_KEY";
        public static final String PERSISTANT_STORAGE_USER_ID_KEY = "userId";
        public static final String DATABASE_NAME = "couponsapp_db";
    }

    public static final class EXTERNAL_PROVIDERS {
        public static final String WAZE_NAVIGATE_INTENT_URI = "waze://?ll=%f, %f&navigate=yes";
        public static final String GOOGLE_NAVIGATION_INTENT_URI = "google.navigation:q=%f,%f&mode=d";

        public static final int GOOGLE_MAPS_DEFAULT_ZOOM_LEVEL = 15;
    }

    public static final class FRAGMENT_TAGS {
        public static final String NEARBY_COUPONS_FRAGMENT_TAG = "couponFragmentTag";
    }
}