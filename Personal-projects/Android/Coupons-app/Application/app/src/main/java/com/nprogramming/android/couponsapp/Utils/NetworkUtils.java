package com.nprogramming.android.couponsapp.Utils;

import okhttp3.HttpUrl;

import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_PORT;


public class NetworkUtils {

    public static HttpUrl.Builder getBaseHttpUrlBuilder(String path) {
        return new HttpUrl.Builder()
                .scheme("http")
                .host(SERVER_HOST)
                .port(SERVER_PORT)
                .addPathSegment(path);
    }
}
