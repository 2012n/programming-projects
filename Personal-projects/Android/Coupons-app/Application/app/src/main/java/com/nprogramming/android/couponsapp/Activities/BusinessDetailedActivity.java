package com.nprogramming.android.couponsapp.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Utils.BusinessUtils;
import com.nprogramming.android.couponsapp.Utils.BusinessUtils.BusinessPageData;
import com.nprogramming.android.couponsapp.Utils.Constants;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DAYS_KEYS;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTERNAL_PROVIDERS.GOOGLE_MAPS_DEFAULT_ZOOM_LEVEL;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTERNAL_PROVIDERS.GOOGLE_NAVIGATION_INTENT_URI;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTERNAL_PROVIDERS.WAZE_NAVIGATE_INTENT_URI;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_ID_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.BUSINESS_OPENING_TIMES_DISPLAY_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.BUSINESS_RATING_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.BUSINESS_PAGE_DATA_LOADER_ID;

public class BusinessDetailedActivity extends AppCompatActivity implements OnMapReadyCallback, LoaderManager.LoaderCallbacks<BusinessPageData> {
    private static final String TAG = BusinessDetailedActivity.class.getSimpleName();
    private CircleImageView ivBusinessLogo;
    private ImageView ivFacebook, ivWhatsapp, ivWaze, ivMaps;
    private ImageView[] ivStars;
    private TextView tvBusinessName, tvBusinessCategory, tvBusinessAddress, tvBusinessRating, tvBusinessDescription;
    private SupportMapFragment mapFragment;
    private LinearLayout linearLayoutOpeningTimes;
    private LatLng coordinates;
    private int businessId;
    private int ownerId;
    private int ratingValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_detailed);
        Intent parent = getIntent();

        if (parent == null || !parent.hasExtra(BUSINESS_ID_KEY)) {
            Toast.makeText(this, getString(R.string.message_error_open_business_detailed_activity), Toast.LENGTH_LONG).show();
            Log.v(TAG, "Error open businessDetailedActivity: no BUSINESS_ID_FIELD extra passed.");
            finish();
            return;
        }

        businessId = parent.getIntExtra(BUSINESS_ID_KEY, 0);

        setupViews();
        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_business_detailed_activity));
        attachActionsToLowerBar();
        prepareRatingBar();

        getSupportLoaderManager().restartLoader(BUSINESS_PAGE_DATA_LOADER_ID, null, this).forceLoad();
    }

    private void setupViews() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment_business_detailed_map);
        ivStars = new ImageView[]{
                findViewById(R.id.imageView_business_detailed_star1),
                findViewById(R.id.imageView_business_detailed_star2),
                findViewById(R.id.imageView_business_detailed_star3),
                findViewById(R.id.imageView_business_detailed_star4),
                findViewById(R.id.imageView_business_detailed_star5),
        };

        ivBusinessLogo = findViewById(R.id.imageView_business_detailed_business_logo);
        ivFacebook = findViewById(R.id.imageView_business_detailed_facebook);
        ivWaze = findViewById(R.id.imageView_business_detailed_waze);
        ivMaps = findViewById(R.id.imageView_business_detailed_maps);
        ivWhatsapp = findViewById(R.id.imageView_business_detailed_whatsapp);

        tvBusinessName = findViewById(R.id.textView_business_detailed_business_name);
        tvBusinessCategory = findViewById(R.id.textView_detailed_business_category);
        tvBusinessAddress = findViewById(R.id.textView_business_detailed_address);
        tvBusinessRating = findViewById(R.id.textView_business_deatiled_rating);
        tvBusinessDescription = findViewById(R.id.textView_business_detailed_description);

        linearLayoutOpeningTimes = findViewById(R.id.layout_business_detailed_opening_times);
    }

    private void prepareRatingBar() {
        for (int i = 0; i < ivStars.length; i++) {
            final int finalIdx = i;
            ivStars[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateRatingColors(finalIdx);
                }
            });
        }
    }

    private void updateRatingColors(int count) {
        for (int i = 0; i < ivStars.length; i++) {
            int color = i <= count ?
                    getResources().getColor(R.color.ratingBarStarredColor) :
                    getResources().getColor(R.color.ratingBarUnstarredColor);

            ivStars[i].setColorFilter(color);
        }

        this.ratingValue = (count + 1);
    }

    private void attachActionsToLowerBar() {
        final ImageView[] navigationIcons = new ImageView[]{ivWaze, ivMaps};
        final String[] navigationUris = new String[]{WAZE_NAVIGATE_INTENT_URI, GOOGLE_NAVIGATION_INTENT_URI};

        for (int i = 0; i < navigationIcons.length; i++) {
            final int idx = i;
            navigationIcons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uri = String.format(navigationUris[idx], coordinates.latitude, coordinates.longitude);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    if (intent.resolveActivity(getPackageManager()) != null)
                        startActivity(intent);
                    else
                        Toast.makeText(BusinessDetailedActivity.this, getString(R.string.message_error_open_external_application), Toast.LENGTH_LONG).show();
                }
            });
        }

        final ImageView[] shareIcons = new ImageView[]{ivFacebook, ivWhatsapp};
        final String[] packages = new String[]{"com.facebook.katana", "com.whatsapp"};

        for (int i = 0; i < packages.length; i++) {
            final int finalI = i;
            shareIcons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.message_business_share_format, businessId));
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage(packages[finalI]);
                    startActivity(sendIntent);
                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, GOOGLE_MAPS_DEFAULT_ZOOM_LEVEL));

        map.addMarker(new MarkerOptions()
                .title(tvBusinessAddress.getText().toString())
                .position(coordinates)
        );
    }

    @NonNull
    @Override
    public Loader<BusinessPageData> onCreateLoader(int id, @Nullable Bundle args) {
        return new BusinessUtils.GetBusinessPageDataTask(BusinessDetailedActivity.this, businessId);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<BusinessPageData> loader, BusinessPageData data) {
        this.coordinates = data.coordinates;
        this.ownerId = data.ownerId;

        Picasso.get().load(String.format(Constants.SERVER_REQUEST_PATHS.BUSINESS_PROFILE_PICTURE_URL, businessId)).into(ivBusinessLogo);
        tvBusinessDescription.setText(data.description);
        tvBusinessRating.setText(String.format(Locale.getDefault(), BUSINESS_RATING_FORMAT, data.rating));
        tvBusinessAddress.setText(data.address);
        tvBusinessName.setText(data.businessName);
        tvBusinessCategory.setText(data.category);

        for (int i = 0; i < DAYS_KEYS.length; i++) {
            String times = data.openingHours.optString(DAYS_KEYS[i], getString(R.string.label_close));
            TextView textView = new TextView(this);
            if (i % 2 == 0) // Make a zebra striping style background.
                textView.setBackgroundColor(getResources().getColor(R.color.darkZebra));

            if (times.isEmpty()) times = getString(R.string.label_close);

            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            textView.setText(String.format(BUSINESS_OPENING_TIMES_DISPLAY_FORMAT, DAYS_KEYS[i].toUpperCase(), times));
            linearLayoutOpeningTimes.addView(textView);
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<BusinessPageData> loader) {

    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }
}
