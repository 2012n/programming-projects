package com.nprogramming.android.couponsapp.Fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.COUPON_END_DATE_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_MAX_DISTANCE_FOR_METERS;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DISPLAY_TITLE_MAX_LENGTH;


/**
 * A simple {@link Fragment} subclass.
 */
public class MiniCouponFragment extends Fragment {
    private CircleImageView businessIcon;
    private TextView couponTitle;
    private TextView businessName;
    private TextView validsUntil;
    private TextView couponInfo;
    private TextView couponDistance;
    private Location phoneLocation;

    public MiniCouponFragment() {
        phoneLocation = null;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mini_coupon, container, false);
    }

    private void attachViews(View rootView) {
        businessIcon = rootView.findViewById(R.id.imageView_business_logo);
        couponTitle = rootView.findViewById(R.id.textView_sale_title);
        businessName = rootView.findViewById(R.id.textView_mini_business_business_title);
        validsUntil = rootView.findViewById(R.id.textView_valid_time_data);
        couponInfo = rootView.findViewById(R.id.textView_info_summery);
        couponDistance = rootView.findViewById(R.id.textView_mini_coupons_distance_value);
    }

    private void setViewsData(MiniCouponInfo data) {
        String description = data.getDescription();
        String title = data.getCouponTitle();

        Picasso.get()
                .load(data.getBusinessLogoPath())
                .placeholder(getContext().getDrawable(R.drawable.ic_shop))
                .into(businessIcon);

        businessName.setText(data.getBusinessName());
        validsUntil.setText(String.format(Locale.getDefault(), COUPON_END_DATE_FORMAT, Utils.getCouponStyleDateFormat(data.getValidityEnd())));

        if (description.length() > MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH)
            couponInfo.setText(description.substring(0, MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH - 3) + " ..");
        else
            couponInfo.setText(description);

        if (title.length() > MINI_COUPON_DISPLAY_DESCRIPTION_MAX_LENGTH)
            couponTitle.setText(title.substring(MINI_COUPON_DISPLAY_TITLE_MAX_LENGTH - 3) + " ..");
        else
            couponTitle.setText(title);

        if (phoneLocation != null) {
            Location couponLocation = new Location("");
            couponLocation.setLatitude(data.getBusinessLatitude());
            couponLocation.setLongitude(data.getBusinessLongitude());

            float distanceInMeters = couponLocation.distanceTo(phoneLocation);
            if (distanceInMeters < MINI_COUPON_DISPLAY_MAX_DISTANCE_FOR_METERS)
                couponDistance.setText(String.format(Locale.getDefault(), "%dm away", (int) distanceInMeters));
            else
                couponDistance.setText(String.format(Locale.getDefault(), "%.1fKM away", distanceInMeters / 1000.0));
        }
    }

    private void setPhoneLocation(Location phoneLocation) {
        this.phoneLocation = phoneLocation;
    }
}
