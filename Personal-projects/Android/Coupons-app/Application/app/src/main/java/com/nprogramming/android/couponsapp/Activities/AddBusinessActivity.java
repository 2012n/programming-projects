package com.nprogramming.android.couponsapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;
import com.nprogramming.android.couponsapp.Utils.BusinessUtils;
import com.nprogramming.android.couponsapp.Utils.Constants;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DAYS_KEYS;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_DATA_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.OPENING_TIME_REGEX_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.NEW_BUSINESS_PLACE_PICKER_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.NEW_BUSINESS_PROFILE_PICK_IMAGE_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Utils.fillCategoryDropbox;

public class AddBusinessActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Bundle>, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private CircleImageView profilePicture;
    private Spinner spinnerCategory;
    private TextView tvErrorMessage;
    private EditText etBusinessName, etDescription, etAddress;
    private EditText[] openingTimes;
    private Button buttonSubmitNewBusiness;

    private LatLng businessCoordinates;
    private String businessLogoPath = null;

    private UserDataCenter userDataCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business);

        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_add_business_activity));

        UserDataCenter.getInstanceAsync(this, this);
    }

    private void setSubmitButton() {
        buttonSubmitNewBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorMessages = validateFormData();

                if (errorMessages.length() != 0) {
                    tvErrorMessage.setText(errorMessages);
                    tvErrorMessage.setVisibility(View.VISIBLE);
                } else {
                    getSupportLoaderManager().restartLoader(Constants.LOADER_IDS.SUBMIT_ADD_BUSINESS_LOADER_ID, null, AddBusinessActivity.this).forceLoad();
                }
            }
        });
    }

    private void attachProfilePictureSelector() {
        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(AddBusinessActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    ActivityCompat.requestPermissions(AddBusinessActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST);
                else
                    openImagePicker();
            }
        });
    }

    private void openImagePicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, NEW_BUSINESS_PROFILE_PICK_IMAGE_REQUEST);
    }

    private void attachPlacePickerToEditText(EditText etAddress) {
        etAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(AddBusinessActivity.this), NEW_BUSINESS_PLACE_PICKER_REQUEST);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void setupViews() {
        spinnerCategory = findViewById(R.id.spinner_add_business_category);
        profilePicture = findViewById(R.id.imageview_add_business_profile_picture);
        etBusinessName = findViewById(R.id.editText_add_business_name);
        etAddress = findViewById(R.id.editText_add_business_address);
        etDescription = findViewById(R.id.editText_add_business_description);
        tvErrorMessage = findViewById(R.id.textView_add_business_error_message);
        buttonSubmitNewBusiness = findViewById(R.id.button_add_business_submit);

        openingTimes = new EditText[]{
                findViewById(R.id.editText_add_business_sunday_value),
                findViewById(R.id.editText_add_business_monday_value),
                findViewById(R.id.editText_add_business_tuesday_value),
                findViewById(R.id.editText_add_business_wednesday_value),
                findViewById(R.id.editText_add_business_thursday_value),
                findViewById(R.id.editText_add_business_friday_value),
                findViewById(R.id.editText_add_business_saturday_value)
        };
    }

    private String validateFormData() {
        int errorCount = 0;
        StringBuilder errorMessages = new StringBuilder();

        if (etBusinessName.getText().length() < Constants.FORMATTING.BUSINESS_NAME_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. Business' name should contain than %d characters.\n", ++errorCount, Constants.FORMATTING.BUSINESS_NAME_MIN_LENGTH));
        if (etAddress.getText().length() == 0)
            errorMessages.append(String.format(Locale.getDefault(), "%d. Must specify businessCoordinates.\n", ++errorCount));
        if (etDescription.getText().length() < Constants.FORMATTING.BUSINESS_DESCRIPTION_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. Business' description should contain than %d characters.\n", ++errorCount, Constants.FORMATTING.BUSINESS_DESCRIPTION_MIN_LENGTH));
        if (spinnerCategory.getSelectedItem().toString().equals(Utils.BusinessCategory.UNDEFINED.toString()))
            errorMessages.append(String.format(Locale.getDefault(), "%d. Business' category must be choosen.\n", ++errorCount));

        for (int i = 0; i < openingTimes.length; i++) {
            String value = openingTimes[i].getText().toString().trim();
            if (value.length() != 0 && !value.matches(OPENING_TIME_REGEX_FORMAT))
                errorMessages.append(String.format(Locale.getDefault(), "%d. Opening time for %s ia invalid.", ++errorCount, DAYS_KEYS[i]));
        }

        return errorMessages.toString().trim();
    }

    @NonNull
    @Override
    public Loader<Bundle> onCreateLoader(int id, @Nullable Bundle args) {
        int profileId = LoginUtils.getLoggedInUserId(this);

        switch (id) {
            case Constants.LOADER_IDS.SUBMIT_ADD_BUSINESS_LOADER_ID:
                String[] openingTimesStrings = new String[openingTimes.length];
                for (int i = 0; i < openingTimesStrings.length; i++)
                    openingTimesStrings[i] = openingTimes[i].getText().toString().trim();

                BusinessUtils.AddBusinessFormData data = new BusinessUtils.AddBusinessFormData(
                        profileId,
                        etBusinessName.getText().toString(),
                        spinnerCategory.getSelectedItem().toString(),
                        etDescription.getText().toString(),
                        etAddress.getText().toString(),
                        this.businessLogoPath,
                        businessCoordinates,
                        openingTimesStrings
                );

                tvErrorMessage.setVisibility(View.GONE);
                return new BusinessUtils.AddBusinessTask(this, data);
            default:
                throw new UnsupportedOperationException("AddBusinessActivity: onCreateLoader: No handler for id " + id);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Bundle> loader, Bundle data) {
        switch (loader.getId()) {
            case Constants.LOADER_IDS.SUBMIT_ADD_BUSINESS_LOADER_ID:
                Intent result = new Intent();
                if (data != null) {
                    ServerResponse serverResponse = data.getParcelable(RESPONSE_CODE_FIELD);
                    result.putExtra(RESPONSE_CODE_FIELD, (Parcelable) serverResponse);
                    if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                        userDataCenter.addOwnerBusiness((MiniBusinessInfo) data.getParcelable(BUSINESS_DATA_KEY));
                        finish();
                    } else {
                        AddBusinessActivity.this.tvErrorMessage.setText(getString(R.string.message_server_response, serverResponse.toString()));
                        AddBusinessActivity.this.tvErrorMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(AddBusinessActivity.this, getString(R.string.message_error_adding_new_business), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Bundle> loader) {

    }

    @Override
    public void onBackPressed() {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        this.userDataCenter = userDataCenter;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setupViews();
                fillCategoryDropbox(AddBusinessActivity.this, spinnerCategory);
                attachPlacePickerToEditText(etAddress);
                attachProfilePictureSelector();
                setSubmitButton();
            }
        });
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, getString(R.string.message_error_getting_data, message), Toast.LENGTH_LONG).show();
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NEW_BUSINESS_PLACE_PICKER_REQUEST:
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(data, this);
                    this.businessCoordinates = place.getLatLng();

                    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                    List<Address> addresses = null;
                    try {
                        addresses = geocoder.getFromLocation(businessCoordinates.latitude, businessCoordinates.longitude, 1);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    if (addresses.size() > 0) {
                        this.etAddress.setText(addresses.get(0).getAddressLine(0));
                    } else
                        this.etAddress.setText(getString(R.string.message_no_results));
                }
                break;
            case NEW_BUSINESS_PROFILE_PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data == null)
                        return;

                    Uri selectedImage = data.getData();

                    if (selectedImage == null)
                        return;

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    this.businessLogoPath = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(this.businessLogoPath);
                    profilePicture.setImageBitmap(bitmap);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case NEW_BUSINESS_READ_EXTERNAL_STORAGE_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openImagePicker();
                else
                    Toast.makeText(AddBusinessActivity.this, getString(R.string.message_image_picker_request_denied), Toast.LENGTH_LONG).show();
        }
    }

}
