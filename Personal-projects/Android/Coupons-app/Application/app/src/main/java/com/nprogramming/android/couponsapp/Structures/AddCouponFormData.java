package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;

import com.nprogramming.android.couponsapp.Utils.Utils;

public final class AddCouponFormData implements Parcelable {
    public final String businessId;
    public final String couponValidityEnd;
    public final String couponTitle;
    public final String couponDescription;

    public AddCouponFormData(int businessId, String couponTitle, String couponDescription, String couponValidityEnd) {
        this.businessId = businessId + "";
        this.couponTitle = couponTitle.trim();
        this.couponDescription = couponDescription.trim();
        this.couponValidityEnd = Utils.getTimestampFromDateString(couponValidityEnd.trim()) + "";
    }

    protected AddCouponFormData(Parcel in) {
        businessId = in.readString();
        couponValidityEnd = in.readString();
        couponTitle = in.readString();
        couponDescription = in.readString();
    }

    public static final Creator<AddCouponFormData> CREATOR = new Creator<AddCouponFormData>() {
        @Override
        public AddCouponFormData createFromParcel(Parcel in) {
            return new AddCouponFormData(in);
        }

        @Override
        public AddCouponFormData[] newArray(int size) {
            return new AddCouponFormData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(businessId);
        dest.writeString(couponValidityEnd);
        dest.writeString(couponTitle);
        dest.writeString(couponDescription);
    }
}
