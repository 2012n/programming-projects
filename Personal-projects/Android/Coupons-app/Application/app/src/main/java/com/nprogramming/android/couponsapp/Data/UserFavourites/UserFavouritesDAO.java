package com.nprogramming.android.couponsapp.Data.UserFavourites;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface UserFavouritesDAO {
    @Insert
    void starCouponForUser(UserFavouritesModel model);

    @Query("SELECT couponId FROM `UserFavouritesModel` WHERE userId=:userId")
    int[] getFavouriteCouponsByUserId(int userId);

    @Query("SELECT count(*) FROM `UserFavouritesModel` WHERE userId=:userId AND couponId=:couponId")
    int isStarred(int userId, int couponId);

    @Query("DELETE FROM `UserFavouritesModel` WHERE userId=:userId AND couponId=:couponId")
    void unstarCouponForUser(int userId, int couponId);

}
