package com.nprogramming.android.couponsapp.Utils;

import android.accounts.NetworkErrorException;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import org.json.JSONObject;

import java.io.File;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.ADDRESS_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_CATEGORY_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_NAME_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_OPENING_TIMES_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.BUSINESS_RATING_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DATA_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DAYS_KEYS;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.DESCRIPTION_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.ID_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LATITUDE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.LONGITUDE_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_DATA_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_HOST;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_INFO.SERVER_PORT;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.ADDRESS_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BUSINESS_LOGO_PICTURE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.BUSINESS_NAME_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.CATEGORY_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.DESCRIPTION_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.ID_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LATITUDE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.LONGITUDE_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.OPENING_TIMES_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.OWNER_ID_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.ADD_BUSINESS_PATH;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PATHS.GET_BUSINESS_BY_ID_PATH;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;


public class BusinessUtils {
    private static final String TAG = BusinessUtils.class.getSimpleName();
    private static final String DEFAULT_PROFILE_PICTURE_FILENAME = "business-0";

    private static Bundle addNewBusiness(AddBusinessFormData formData) {
        Bundle newBusinessData = new Bundle();
        try {

            MultipartBody.Builder multipartBuilder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(OWNER_ID_PARAM, formData.ownerId + "")
                    .addFormDataPart(BUSINESS_NAME_PARAM, formData.businessName)
                    .addFormDataPart(CATEGORY_PARAM, formData.category + "")
                    .addFormDataPart(DESCRIPTION_PARAM, formData.description)
                    .addFormDataPart(ADDRESS_PARAM, formData.address)
                    .addFormDataPart(LATITUDE_PARAM, formData.latLng.latitude + "")
                    .addFormDataPart(LONGITUDE_PARAM, formData.latLng.longitude + "")
                    .addFormDataPart(OPENING_TIMES_PARAM, formData.openingTimesJSON);

            if (formData.logoPath != null)
                multipartBuilder.addFormDataPart(BUSINESS_LOGO_PICTURE_PARAM, DEFAULT_PROFILE_PICTURE_FILENAME, RequestBody.create(
                        MediaType.parse("image/*"),
                        new File(formData.logoPath)
                ));

            HttpUrl url = NetworkUtils.getBaseHttpUrlBuilder(ADD_BUSINESS_PATH).build();

            RequestBody requestBody = multipartBuilder.build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            JSONObject responseBodyJson = new JSONObject(responseBody.string());

            ServerResponse serverResponse = ServerResponse.getServerResponseByIndex(responseBodyJson.getInt(RESPONSE_CODE_FIELD));

            if (serverResponse == ServerResponse.OPERATION_SUCCESS) {
                newBusinessData.putParcelable(
                        BUSINESS_DATA_KEY,
                        getMiniBusinessById(responseBodyJson.getInt(ID_FIELD)));
            }

            newBusinessData.putParcelable(RESPONSE_CODE_FIELD, serverResponse);
            return newBusinessData;
        } catch (Exception ex) {
            Log.e(TAG, "Problem creating new business: " + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    private static MiniBusinessInfo getMiniBusinessById(int businessId) {
        // Currently REST API server does not support retrieving array of ids
        try {
            HttpUrl url = new HttpUrl.Builder()
                    .scheme("http")
                    .host(SERVER_HOST)
                    .port(SERVER_PORT)
                    .addPathSegment(GET_BUSINESS_BY_ID_PATH)
                    .addQueryParameter(ID_PARAM, businessId + "")
                    .build();


            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();

            JSONObject jsonData = new JSONObject(data);

            if (jsonData.optInt(RESPONSE_CODE_FIELD) == ServerResponse.OPERATION_SUCCESS.ordinal()) {
                JSONObject business = jsonData.getJSONObject(DATA_FIELD);
                return new MiniBusinessInfo(
                        business.optInt(ID_FIELD),
                        business.optString(BUSINESS_NAME_FIELD),
                        business.optInt(BUSINESS_CATEGORY_FIELD),
                        (float) business.optDouble(BUSINESS_RATING_FIELD)
                );
            }
        } catch (Exception ex) {
            Log.e("CouponUtils", ex.getMessage());
            ex.printStackTrace();
            return null;
        }
        return null;
    }

    public static BusinessPageData getBusinessPageData(int businessId) {
        BusinessPageData businessPageData = null;
        try {
            HttpUrl httpUrl = new HttpUrl.Builder()
                    .scheme("http")
                    .host(SERVER_HOST)
                    .port(SERVER_PORT)
                    .addPathSegment(GET_BUSINESS_BY_ID_PATH)
                    .addQueryParameter(ID_PARAM, businessId + "")
                    .build();

            Request request = new Request.Builder()
                    .url(httpUrl)
                    .get()
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if (responseBody == null)
                throw new NetworkErrorException("Could not retrieve response.");

            String data = responseBody.string();
            JSONObject jsonData = new JSONObject(data);

            if (jsonData.optInt(RESPONSE_CODE_FIELD) == ServerResponse.OPERATION_SUCCESS.ordinal()) {
                JSONObject businessData = jsonData.optJSONObject(DATA_FIELD);
                if (businessData != null) {
                    businessPageData = new BusinessPageData(
                            businessData.optInt(ID_FIELD),
                            businessData.optInt(BUSINESS_ID_FIELD),
                            businessData.optString(BUSINESS_NAME_FIELD),
                            (float) businessData.optDouble(BUSINESS_RATING_FIELD),
                            businessData.optInt(BUSINESS_CATEGORY_FIELD),
                            businessData.optString(DESCRIPTION_FIELD),
                            businessData.optString(ADDRESS_FIELD),
                            businessData.optDouble(LATITUDE_FIELD),
                            businessData.optDouble(LONGITUDE_FIELD),
                            businessData.optString(BUSINESS_OPENING_TIMES_FIELD)
                    );
                }
            }

            return businessPageData;
        } catch (Exception ex) {
            Log.e(TAG, "Error in GetBusinessPage: " + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Register a new owner profile.
     */

    public static class GetBusinessPageDataTask extends AsyncTaskLoader<BusinessPageData> {
        final int businessId;

        public GetBusinessPageDataTask(@NonNull Context context, int businessId) {
            super(context);
            this.businessId = businessId;
        }

        @Nullable
        @Override
        public BusinessPageData loadInBackground() {
            return BusinessUtils.getBusinessPageData(businessId);
        }
    }

    public static class AddBusinessTask extends AsyncTaskLoader<Bundle> {
        private final AddBusinessFormData businessFormData;

        public AddBusinessTask(Context context, AddBusinessFormData businessFormData) {
            super(context);
            this.businessFormData = businessFormData;
        }

        @Override
        public Bundle loadInBackground() {
            return addNewBusiness(businessFormData);
        }
    }

    public static class AddBusinessFormData {
        final int ownerId;
        final int category;
        final String businessName;
        final String description;
        final String address;
        final String logoPath;
        final LatLng latLng;
        final String openingTimesJSON;

        public AddBusinessFormData(int ownerId, String businessName, String caterory, String description, String address, String logoPath, LatLng latLng, String[] openingTimes) {
            this.ownerId = ownerId;
            this.businessName = businessName.trim();
            this.category = Utils.BusinessCategory.valueOf(caterory).ordinal();
            this.description = description.trim();
            this.address = address.trim();
            this.logoPath = logoPath == null ? null : logoPath.trim();
            this.latLng = latLng;
            this.openingTimesJSON = openingTimesJson(openingTimes);
        }

        private String openingTimesJson(String[] openingTimes) {
            if (openingTimes.length != DAYS_KEYS.length)
                throw new IllegalArgumentException("openingTimes array should contain all days info.");

            StringBuilder jsonResult = new StringBuilder();
            jsonResult.append("{");
            for (int i = 0; i < DAYS_KEYS.length; i++)
                jsonResult.append((String.format(Locale.getDefault(), "\"%s\": \"%s\",", DAYS_KEYS[i], openingTimes[i])));
            jsonResult.deleteCharAt(jsonResult.length() - 1); // Remove last comma
            jsonResult.append("}");

            return jsonResult.toString().trim();
        }


    }

    public static class BusinessPageData {
        public final int businessId;
        public final int ownerId;
        public final float rating;
        public final String category;
        public final String description;
        public final String address;
        public final String businessName;
        public final LatLng coordinates;
        public final JSONObject openingHours;

        public BusinessPageData(int businessId, int ownerId, String businessName, float rating, int category, String description, String address, double latitude, double longitude, String openingHours) {
            this.businessId = businessId;
            this.ownerId = ownerId;
            this.businessName = businessName;
            this.rating = rating;
            this.category = Utils.getCategoryString(category);
            this.description = description;
            this.address = address;
            this.coordinates = new LatLng(latitude, longitude);
            this.openingHours = Utils.getJson(openingHours);
        }
    }
}
