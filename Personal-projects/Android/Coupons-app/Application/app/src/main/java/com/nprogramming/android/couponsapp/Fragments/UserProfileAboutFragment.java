package com.nprogramming.android.couponsapp.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.Activities.EditOwnerAboutInfoActivity;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Structures.OwnerAboutInfo;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.CURRENT_OWNER_ABOUT_INFO_EXTRA_KEY;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileAboutFragment extends Fragment implements UserDataCenter.OnUserDataChangedListener, UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private OwnerAboutInfo ownerAboutInfo;

    private View rootView;

    private TextView tvLivesIn;
    private TextView tvGender;
    private TextView tvLanguages;
    private TextView tvGraduatedOf;
    private TextView tvPhone;
    private TextView tvEmail;
    private TextView tvFacebook;
    private TextView tvBirthday;
    private FloatingActionButton fabEditAboutInfo;

    public UserProfileAboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_user_profile_about, container, false);

        UserDataCenter.getInstanceAsync(getContext(), this);

        return rootView;
    }

    private void fillViews(OwnerAboutInfo ownerAboutInfo) {
        fillColoredTextView(tvLivesIn, R.string.label_livesin, ownerAboutInfo.getLivesIn());
        fillColoredTextView(tvGender, R.string.label_gender, ownerAboutInfo.getGender().toString());
        fillColoredTextView(tvLanguages, R.string.label_languages, ownerAboutInfo.getLanguages());
        fillColoredTextView(tvGraduatedOf, R.string.label_graduated_of, ownerAboutInfo.getGraduatedOf());
        fillColoredTextView(tvBirthday, R.string.label_birthday, Utils.getDateDefaultFormat(ownerAboutInfo.getBirthday()));

        fillColoredTextView(tvPhone, R.string.label_phone, ownerAboutInfo.getPhone());
        fillColoredTextView(tvEmail, R.string.label_email, ownerAboutInfo.getEmail());
        fillColoredTextView(tvFacebook, R.string.label_facebook, ownerAboutInfo.getFacebook());
    }

    private void fillColoredTextView(TextView textView, int labelRedId, String data) {
        fillColoredTextView(textView, getString(labelRedId), data);
    }

    private void fillColoredTextView(TextView textView, String label, String data) {
        label += " ";
        Spannable coloredLabel = new SpannableString(label);
        coloredLabel.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ownerDataLabelColor)), 0, coloredLabel.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(coloredLabel);

        Spannable coloredData = new SpannableString(data);
        coloredData.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.ownerDataValueColor)), 0, coloredData.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.append(coloredData);
    }


    @Override
    public void onOwnerCouponAdded(MiniCouponInfo miniCouponInfo) {

    }

    @Override
    public void onOwnerCouponRemoved(int couponId) {

    }

    @Override
    public void onOwnerBusinessAdded(MiniBusinessInfo miniBusinessInfo) {

    }

    @Override
    public void onOwnerBusinessRemoved(int businessId) {

    }

    @Override
    public void onOwnerAboutDataChanged(OwnerAboutInfo newInfo) {
        fillViews(newInfo);
    }

    @Override
    public void onStarChanged(int couponId, CouponUtils.StarredState state) {

    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        ownerAboutInfo = userDataCenter.getOwnerAboutInfo();

        if (ownerAboutInfo == null)
            return;

        tvLivesIn = rootView.findViewById(R.id.profile_about_livesin);
        tvGender = rootView.findViewById(R.id.profile_about_gender);
        tvLanguages = rootView.findViewById(R.id.profile_about_languages);
        tvGraduatedOf = rootView.findViewById(R.id.profile_about_graduated_of);
        tvBirthday = rootView.findViewById(R.id.profile_about_birthday);

        tvPhone = rootView.findViewById(R.id.profile_about_phone);
        tvEmail = rootView.findViewById(R.id.profile_about_email);
        tvFacebook = rootView.findViewById(R.id.profile_about_facebook);

        fabEditAboutInfo = rootView.findViewById(R.id.floatingActionBar_edit_about_info);

        fabEditAboutInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editOwnerAboutInfoActivityIntent = new Intent(getContext(), EditOwnerAboutInfoActivity.class);
                editOwnerAboutInfoActivityIntent.putExtra(CURRENT_OWNER_ABOUT_INFO_EXTRA_KEY, ownerAboutInfo);
                startActivity(editOwnerAboutInfoActivityIntent);
            }
        });

        userDataCenter.registerDataChangedListenerCallback(this);
        fillViews(ownerAboutInfo);
    }

    @Override
    public void onError(String message) {

    }
}
