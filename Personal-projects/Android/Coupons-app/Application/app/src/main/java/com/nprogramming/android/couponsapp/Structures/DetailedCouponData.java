package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;

public class DetailedCouponData implements Parcelable {
    public int couponId, businessId;
    public long couponValidityEnd;
    public String couponTitle, businessName, businessAddress, couponDescription;
    public float latitude, longitude;

    public DetailedCouponData(int couponId, int businessId, String couponTitle, String businessName, String businessAddress, long couponValidityEnd, String couponDescription, float latitude, float longitude) {
        this.couponId = couponId;
        this.businessId = businessId;
        this.couponTitle = couponTitle;
        this.businessName = businessName;
        this.businessAddress = businessAddress;
        this.couponValidityEnd = couponValidityEnd;
        this.couponDescription = couponDescription;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DetailedCouponData(Parcel parcel) {
        this.couponId = parcel.readInt();
        this.businessId = parcel.readInt();
        this.couponTitle = parcel.readString();
        this.businessName = parcel.readString();
        this.businessAddress = parcel.readString();
        this.couponValidityEnd = parcel.readLong();
        this.couponDescription = parcel.readString();
        this.latitude = parcel.readFloat();
        this.longitude = parcel.readFloat();
    }

    public static final Parcelable.Creator<DetailedCouponData> CREATOR = new Creator<DetailedCouponData>() {
        @Override
        public DetailedCouponData createFromParcel(Parcel source) {
            return new DetailedCouponData(source);
        }

        @Override
        public DetailedCouponData[] newArray(int size) {
            return new DetailedCouponData[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(couponId);
        dest.writeInt(businessId);
        dest.writeString(couponTitle);
        dest.writeString(businessName);
        dest.writeString(businessAddress);
        dest.writeLong(couponValidityEnd);
        dest.writeString(couponDescription);
        dest.writeFloat(latitude);
        dest.writeFloat(longitude);
    }
}

