package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;

import com.nprogramming.android.couponsapp.Adapters.Pageable;

public class MiniCouponInfo implements Parcelable, Pageable {
    private int couponId;
    private int businessId;
    private String businessLogoPath;
    private String couponTitle;
    private String businessName;
    private long validityEnd;
    private String description;
    private float businessLatitude;
    private float businessLongitude;

    public MiniCouponInfo(int couponId, int businessId, String businessLogoPath, String couponTitle, String businessName, long validityEnd, String description, float businessLatitude, float businessLongitude) {
        this.couponId = couponId;
        this.businessId = businessId;
        this.businessLogoPath = businessLogoPath;
        this.couponTitle = couponTitle;
        this.businessName = businessName;
        this.validityEnd = validityEnd;
        this.description = description;
        this.businessLatitude = businessLatitude;
        this.businessLongitude = businessLongitude;
    }

    public MiniCouponInfo(Parcel parcel) {
        couponId = parcel.readInt();
        businessId = parcel.readInt();
        businessLogoPath = parcel.readString();
        couponTitle = parcel.readString();
        businessName = parcel.readString();
        validityEnd = parcel.readLong();
        description = parcel.readString();
        businessLatitude = parcel.readFloat();
    }

    public float getBusinessLatitude() {
        return businessLatitude;
    }

    public void setBusinessLatitude(float businessLatitude) {
        this.businessLatitude = businessLatitude;
    }

    public float getBusinessLongitude() {
        return businessLongitude;
    }

    public void setBusinessLongitude(float businessLongitude) {
        this.businessLongitude = businessLongitude;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getBusinessLogoPath() {
        return businessLogoPath;
    }

    public void setBusinessLogoPath(String businessLogoPath) {
        this.businessLogoPath = businessLogoPath;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public long getValidityEnd() {
        return validityEnd;
    }


    public void setValidityEnd(long validityEnd) {
        this.validityEnd = validityEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static final Parcelable.Creator<MiniCouponInfo> CREATOR = new Creator<MiniCouponInfo>() {
        @Override
        public MiniCouponInfo createFromParcel(Parcel parcel) {
            return new MiniCouponInfo(parcel);
        }

        @Override
        public MiniCouponInfo[] newArray(int size) {
            return new MiniCouponInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(couponId);
        parcel.writeInt(businessId);
        parcel.writeString(businessLogoPath);
        parcel.writeString(couponTitle);
        parcel.writeString(businessName);
        parcel.writeLong(validityEnd);
        parcel.writeString(description);
        parcel.writeFloat(businessLatitude);
        parcel.writeFloat(businessLongitude);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Integer)
            return this.couponId == (int) obj;
        return obj instanceof MiniCouponInfo && this.couponId == ((MiniCouponInfo) obj).couponId;
    }
}
