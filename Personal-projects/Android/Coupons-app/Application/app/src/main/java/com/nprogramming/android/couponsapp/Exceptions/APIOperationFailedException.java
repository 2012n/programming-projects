package com.nprogramming.android.couponsapp.Exceptions;

import com.nprogramming.android.couponsapp.Structures.ServerResponse;

public class APIOperationFailedException extends Exception {
    private ServerResponse serverResponse;

    public APIOperationFailedException() { }

    public APIOperationFailedException(String message, ServerResponse serverResponse) {
        super(message);
        this.serverResponse = serverResponse;
    }

    public APIOperationFailedException(String message) {
        super(message);
        this.serverResponse = ServerResponse.UNKNOWN_ERROR;
    }
}
