package com.nprogramming.android.couponsapp.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nprogramming.android.couponsapp.Activities.AddBusinessActivity;
import com.nprogramming.android.couponsapp.Activities.BusinessDetailedActivity;
import com.nprogramming.android.couponsapp.Adapters.MiniBusinessRecyclerViewAdapter;
import com.nprogramming.android.couponsapp.Data.UserDataCenter;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniBusinessInfo;
import com.nprogramming.android.couponsapp.Structures.ServerResponse;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_DATA_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.BUSINESS_ID_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.REQUEST_IDS.ADD_NEW_BUSINESS_REQUEST;
import static com.nprogramming.android.couponsapp.Utils.Utils.RESPONSE_CODE_FIELD;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileBusinessesFragment extends Fragment implements UserDataCenter.OnUserDataCenterGettingInstanceFinished {
    private static final String TAG = UserProfileBusinessesFragment.class.getSimpleName();

    private ArrayList<MiniBusinessInfo> miniBusinessInfos;
    private View rootView;

    private FloatingActionButton fabAddNewBusiness;

    public UserProfileBusinessesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_user_profile_businesses, container, false);
        UserDataCenter.getInstanceAsync(getContext(), this);
        return rootView;
    }

    private void setupBusinessesRecyclerView(View rootView) {
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerview_mini_businesses_list);
        fabAddNewBusiness = rootView.findViewById(R.id.floatingActionBar_add_owning_business);

        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        MiniBusinessRecyclerViewAdapter adapter = new MiniBusinessRecyclerViewAdapter(getContext());

        adapter.setOnBusinessItemClickListener(new MiniBusinessRecyclerViewAdapter.OnBusinessItemClickListener() {
            @Override
            public void onItemClick(int businessId) {
                Intent businessPageIntent = new Intent(getContext(), BusinessDetailedActivity.class);
                businessPageIntent.putExtra(BUSINESS_ID_KEY, businessId);
                startActivity(businessPageIntent);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADD_NEW_BUSINESS_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        ServerResponse serverResponse = data.getParcelableExtra(RESPONSE_CODE_FIELD);
                        if (serverResponse == ServerResponse.OPERATION_SUCCESS && data.hasExtra(BUSINESS_DATA_KEY)) {

                            Toast.makeText(getContext(), getString(R.string.message_business_added_successfully), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.message_unknown_server_error), Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onUserDataCenterCreated(UserDataCenter userDataCenter) {
        miniBusinessInfos = userDataCenter.getOwnerBusinessInfo();

        if (miniBusinessInfos == null) {
            Log.e(TAG, "MiniBusinessInfos argument not passed.");
            return;
        }

        Activity parent = getActivity();

        if (parent == null)
            throw new NullPointerException("Could not get parent activity.");

        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setupBusinessesRecyclerView(rootView);

                fabAddNewBusiness.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent newBusinessFormIntent = new Intent(getContext(), AddBusinessActivity.class);
                        startActivityForResult(newBusinessFormIntent, ADD_NEW_BUSINESS_REQUEST);
                    }
                });
            }
        });
    }

    @Override
    public void onError(String message) {

    }
}
