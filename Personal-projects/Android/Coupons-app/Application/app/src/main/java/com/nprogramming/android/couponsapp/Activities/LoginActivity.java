package com.nprogramming.android.couponsapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.LoginUtils.LoginResponseData;
import com.nprogramming.android.couponsapp.Utils.LoginUtils.LoginResponseStatus;
import com.nprogramming.android.couponsapp.Utils.LoginUtils.LoginTask;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.PASSWORD_MIN_LENGTH;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.LOGIN_LOADER_ID;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.EMAIL_PARAM;
import static com.nprogramming.android.couponsapp.Utils.Constants.SERVER_REQUEST_PARAMETERS.PASSWORD_PARAM;

public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<LoginResponseData> {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText editTextEmail, editTextPassword;
    private TextView tvErrorMessage;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LoginUtils.isUserLoggedIn(this)) {
            finish();
        }

        setContentView(R.layout.activity_login);

        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_login_activity));
        setupViews();
    }

    /**
     * Validates form data.
     * @return String filled with all detected errors.
     */

    private String validateFormData() {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        StringBuilder errorMessages = new StringBuilder();

        int errorCount = 0;

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n", ++errorCount, getString(R.string.message_bad_email_format)));
        if (password.length() < PASSWORD_MIN_LENGTH)
            errorMessages.append(String.format(Locale.getDefault(), "%d. %s.\n",++errorCount, getString(R.string.message_password_too_short)));

        return errorMessages.toString().trim();
    }

    /**
     * Binds views into variables and sets up various ui components.
     */

    private void setupViews() {
        Button buttonSubmit = findViewById(R.id.button_login);
        TextView tvRegisterLink = findViewById(R.id.textView_register_link);

        editTextEmail = findViewById(R.id.editText_login_email);
        editTextPassword = findViewById(R.id.editText_login_password);
        tvErrorMessage = findViewById(R.id.textView_error_message);


        progressBar = findViewById(R.id.progressbar_login);

        tvRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerActivity = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerActivity);
                finish();
            }
        });
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editTextEmail.getText().toString();
                String password = editTextPassword.getText().toString();

                String error_message = validateFormData();
                if (error_message.length() != 0) {
                    tvErrorMessage.setText(error_message);
                    tvErrorMessage.setVisibility(View.VISIBLE);
                    return;
                }

                Bundle args = new Bundle();
                args.putString(EMAIL_PARAM, email);
                args.putString(PASSWORD_PARAM, password);

                getSupportLoaderManager().restartLoader(LOGIN_LOADER_ID, args, LoginActivity.this).forceLoad();
            }
        });
    }


    @NonNull
    @Override
    public Loader<LoginResponseData> onCreateLoader(int id, @Nullable Bundle args) {
        if (args == null)
            throw new IllegalArgumentException("LoginActivity: onCreateLoader: args is null");
        if (!args.containsKey(EMAIL_PARAM) || !args.containsKey(PASSWORD_PARAM))
            throw new IllegalArgumentException("args must contain EMAIL_PARAM and PASSWORD_PARAM.");

        String email = args.getString(EMAIL_PARAM);
        String password = args.getString(PASSWORD_PARAM);

        tvErrorMessage.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        return new LoginTask(this, email, password);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<LoginResponseData> loader, LoginResponseData response) {
        progressBar.setVisibility(View.INVISIBLE);

        if (response.loginResponseStatus == LoginResponseStatus.LOGIN_SUCCESSFUL) {

            // Logged in successfully, move to profile.
            Intent profileIntent = new Intent(LoginActivity.this, OwnerProfileActivity.class);
            startActivity(profileIntent);
            finish();
        } else {
            tvErrorMessage.setVisibility(View.VISIBLE);
            tvErrorMessage.setText(getString(R.string.message_server_data_error, response.toString()));
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<LoginResponseData> loader) {

    }
}
