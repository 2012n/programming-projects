package com.nprogramming.android.couponsapp.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.Activities.OwnerProfileActivity;
import com.nprogramming.android.couponsapp.Activities.SearchActivity;
import com.nprogramming.android.couponsapp.Activities.FavouriteCouponsActivity;
import com.nprogramming.android.couponsapp.Activities.NearbyCouponsActivity;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class LowerNavigationBarFragment extends Fragment {
    public static final int NEARBY_IDX = 0;
    public static final int FAVOURITES_IDX = 1;
    public static final int SEARCH_IDX = 2;
    public static final int PROFILE_IDX = 3;

    public LowerNavigationBarFragment() {
        // Required empty public constructor
    }

    /**
     * Colors the navigation item to an active color.
     * @param lowerNavigationBarFragment fragment - the fragment to use.
     * @param tab - tab index.
     */
    public static void setNavItemActiveColor(LowerNavigationBarFragment lowerNavigationBarFragment, int tab) {
        if (tab < NEARBY_IDX || tab > PROFILE_IDX)
            throw new IllegalArgumentException(String.format(Locale.getDefault(), "Given tab argument is invalid; valid range is %d - %d.", NEARBY_IDX, PROFILE_IDX));

        LinearLayout lowerNavigateLayout = (LinearLayout) lowerNavigationBarFragment.getView();

        if (lowerNavigateLayout == null)
            throw new IllegalArgumentException("LowerNavigationBarFragment must have an already created view.");

        TextView selectedMenuTab = (TextView) lowerNavigateLayout.getChildAt(tab);

        int activeColor = ContextCompat.getColor(lowerNavigationBarFragment.getContext(), R.color.lowerNavigationBarItemActiveColor);

        Utils.setTextViewDrawableColor(selectedMenuTab, activeColor);
        selectedMenuTab.setTextColor(activeColor);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lower_navigation_bar, container, false);

        TextView nearby = rootView.findViewById(R.id.lower_navigation_nearby),
                favourites = rootView.findViewById(R.id.lower_navigation_favourite),
                search = rootView.findViewById(R.id.lower_navigation_search),
                profile = rootView.findViewById(R.id.lower_navigation_profile);

        nearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NearbyCouponsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), OwnerProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FavouriteCouponsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        return rootView;
    }

}
