package com.nprogramming.android.couponsapp.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.nprogramming.android.couponsapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.DEFAULT_DATE_FORMAT;
import static com.nprogramming.android.couponsapp.Utils.Constants.FORMATTING.MINI_COUPON_DATE_STYLE_FORMAT;

public class Utils {
    /**
     * The most general utils class.
     */

    public static final String RESPONSE_CODE_FIELD = "RESPONSE_STATUS";

    private static final String TAG = Utils.class.getSimpleName();

    public static @Nullable Location getPhoneLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    public enum BusinessCategory {
        UNDEFINED,
        HAMBURGER,
        PIZZA,
        SUSHI,
        COFFEE
    }

    public enum DatePickerSettings {
        SHOW_ALL,
        FUTURE_ONLY,
        PAST_ONLY
    }

    public static String getCategoryString(int idx) {
        return BusinessCategory.values()[idx].toString();
    }

    /**
     * Removes the exceptions mechanism when trying to parse a json.
     *
     * @param data json object as string.
     * @return JSONObject on success; otherwise null.
     */

    public static JSONObject getJson(String data) {
        JSONObject jsonData;
        try {
            jsonData = new JSONObject(data);
        } catch (JSONException ex) {
            Log.e(TAG, "Json cannot be parsed! message: " + ex.getMessage());
            return null;
        }

        return jsonData;
    }

    /**
     * Converts a given timestamp to a "<Num> <Month name>" date format.
     *
     * @param timestamp milliseconds timestamp.
     * @return converted timestamp as string.
     */
    public static String getCouponStyleDateFormat(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat sdf = new SimpleDateFormat(MINI_COUPON_DATE_STYLE_FORMAT, Locale.getDefault());

        return sdf.format(date);
    }

    public static String getDateDefaultFormat(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.getDefault());

        return simpleDateFormat.format(date);
    }

    public static long getTimestampFromDateString(String strdate) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);
            Date date = simpleDateFormat.parse(strdate);
            return date.getTime();
        } catch (ParseException ex) {
            return 0;
        }
    }

    /**
     * Attaches a datepicker to an edittext view so that when clicked a datepicker pops.
     *
     * @param editTextView the edittextview to attach the datepicker.
     */

    @SuppressLint("NewApi") // TODO: Rewrite to a more back-compatiable function.
    public static void attachDatepickerToEditText(final Context context, final EditText editTextView, final DatePickerSettings datePickerSettings) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);

                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String formattedDate = simpleDateFormat.format(calendar.getTime());

                editTextView.setText(formattedDate);
            }
        };

        // Attach datepicker view to the edittextview.
        editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        context,
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                switch (datePickerSettings) {
                    case FUTURE_ONLY:
                        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                        break;
                    case PAST_ONLY:
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        break;
                }

                datePickerDialog.show();
            }
        });
    }

    public static void fillCategoryDropbox(Context context, Spinner spinnerCategory) {
        ArrayAdapter<BusinessCategory> adapter = new ArrayAdapter<>(context, R.layout.support_simple_spinner_dropdown_item, Utils.BusinessCategory.values());
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);
    }

    public static void setupActionBarTitle(ActionBar actionBar, String title) {
        if (actionBar == null)
            throw new IllegalArgumentException("ActionBar must not be empty.");

        Spanned whiteActionBarText = Html.fromHtml("<font color='#ffffff'>" + title + "</font>");
        actionBar.setTitle(whiteActionBarText);
        actionBar.setElevation(0); // Removes shadow
    }

    public static void showErrorMessage(AppCompatActivity context, ViewGroup parent,  int iconResId, String text, boolean clear) {
        showErrorMessage(context, parent, context.getDrawable(iconResId), text, clear);
    }

    public static void showErrorMessage(AppCompatActivity context, ViewGroup parent,  Drawable icon, String message, boolean clear) {
        if (clear)
            parent.removeAllViews();

        context.getLayoutInflater().inflate(R.layout.layout_warning_message, parent);
        TextView warningLabel = parent.findViewById(R.id.textView_warning_message_label);
        warningLabel.setText(message);

        if (icon != null) {
            ImageView warningIcon = parent.findViewById(R.id.imageView_warning_message_icon);
            warningIcon.setImageDrawable(icon);
        }
    }

    public static void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
