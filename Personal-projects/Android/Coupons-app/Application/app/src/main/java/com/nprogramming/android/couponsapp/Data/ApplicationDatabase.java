package com.nprogramming.android.couponsapp.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nprogramming.android.couponsapp.Data.UserFavourites.UserFavouritesDAO;
import com.nprogramming.android.couponsapp.Data.UserFavourites.UserFavouritesModel;

import static com.nprogramming.android.couponsapp.Utils.Constants.STORAGE_SETTINGS.DATABASE_NAME;

@Database(entities = {UserFavouritesModel.class}, version = 2, exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {
    public abstract UserFavouritesDAO userFavouritesDAO();

    private static final Object LOCK = new Object();
    private static ApplicationDatabase instance;

    public static ApplicationDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (LOCK) {
                instance = Room.databaseBuilder(context.getApplicationContext(), ApplicationDatabase.class, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build();
            }

        }
        return instance;
    }
}
