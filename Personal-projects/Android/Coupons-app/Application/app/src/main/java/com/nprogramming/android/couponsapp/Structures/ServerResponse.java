package com.nprogramming.android.couponsapp.Structures;

import android.os.Parcel;
import android.os.Parcelable;

public enum ServerResponse implements Parcelable {
    OPERATION_SUCCESS,
    INTERNAL_SERVER_ERROR,
    REGISTER_MISSING_EMAIL_OR_PASSWORD,
    LOGIN_MISSING_EMAIL_OR_PASSWORD,
    OWNER_PROFILE_AUTHENTICATION_SUCCSS,
    OWNER_PROFILE_AUTHENTICATION_BAD_CREDENTIALS,
    REGISTER_BUSINESS_MISSING_OWNERID_NAME_CATEGORY,
    REGISTER_COUPON_MISSING_BUSINESSID,
    BAD_ID_ARGUMENT,
    UNKNOWN_ERROR,
    REGISTER_EMAIL_EXISTS,
    PROFILE_IMAGE_BAD_FORMAT,

    // These are not obtains from the server:
    CONNECTION_ERROR,
    ERROR_NOT_INDEXED,
    NO_SERVER_RESPONSE;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServerResponse> CREATOR = new Creator<ServerResponse>() {
        @Override
        public ServerResponse createFromParcel(Parcel in) {
            return ServerResponse.values()[in.readInt()];
        }

        @Override
        public ServerResponse[] newArray(int size) {
            return new ServerResponse[size];
        }
    };

    public static ServerResponse getServerResponseByIndex(int idx) {
        ServerResponse[] responses = ServerResponse.values();
        if (responses.length < idx)
            return ERROR_NOT_INDEXED;
        else
            return responses[idx];
    }
}