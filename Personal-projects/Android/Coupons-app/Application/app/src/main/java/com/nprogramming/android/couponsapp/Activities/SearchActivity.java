package com.nprogramming.android.couponsapp.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.nprogramming.android.couponsapp.Fragments.LowerNavigationBarFragment;
import com.nprogramming.android.couponsapp.Fragments.MiniCouponsListFragment;
import com.nprogramming.android.couponsapp.Fragments.SearchFormFragment;
import com.nprogramming.android.couponsapp.R;
import com.nprogramming.android.couponsapp.Structures.MiniCouponInfo;
import com.nprogramming.android.couponsapp.Utils.CouponUtils;
import com.nprogramming.android.couponsapp.Utils.CouponUtils.CouponSearchCretirias;
import com.nprogramming.android.couponsapp.Utils.LoginUtils;
import com.nprogramming.android.couponsapp.Utils.Utils;

import java.util.ArrayList;

import static com.nprogramming.android.couponsapp.Utils.Constants.DATA_FIELDS.CRETIRIAS_FIELD;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_COUPONS_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.EXTRA_KEYS.ARGUMENTS_PHONE_LOCATION_KEY;
import static com.nprogramming.android.couponsapp.Utils.Constants.LOADER_IDS.SEARCH_COUPONS_LOADER_ID;

public class SearchActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<MiniCouponInfo>>{
    private LinearLayout body;
    private ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setupViews();
        Utils.setupActionBarTitle(getSupportActionBar(), getString(R.string.title_search_activity));
        setNavigationBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.owner_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.owner_profile_menu_item_logout:
                LoginUtils.logout(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void setupViews() {
        body = findViewById(R.id.linearLayout_activity_search_body);
        loadingBar = findViewById(R.id.progressBar_search_loading);

        SearchFormFragment searchFormFragment = new SearchFormFragment();

        searchFormFragment.setOnSearchFormSubmittion(new SearchFormFragment.OnSearchFormSubmittion() {
            @Override
            public void onSearchFormSubmitted(boolean isFormValid, CouponSearchCretirias cretirias) {
                if (isFormValid) {
                    body.removeAllViews();
                    loadingBar.setVisibility(View.VISIBLE);
                    Bundle args = new Bundle();
                    args.putParcelable(CRETIRIAS_FIELD, cretirias);
                    getSupportLoaderManager().restartLoader(SEARCH_COUPONS_LOADER_ID, args, SearchActivity.this).forceLoad();
                }
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.linearLayout_activity_search_body, searchFormFragment);
        transaction.commit();
    }

    /**
     * makes Profile tab to be the active tab.
     */

    private void setNavigationBar() {
        LowerNavigationBarFragment fragment = (LowerNavigationBarFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_search_lower_bar);
        LowerNavigationBarFragment.setNavItemActiveColor(fragment, LowerNavigationBarFragment.SEARCH_IDX);
    }

    @NonNull
    @Override
    public Loader<ArrayList<MiniCouponInfo>> onCreateLoader(int id, @Nullable Bundle args) {
        switch (id) {
            case SEARCH_COUPONS_LOADER_ID:
                if (args != null && !args.containsKey(CRETIRIAS_FIELD))
                    throw new IllegalArgumentException("Argument must not be null and must contains CRETIRIAS_FIELD key.");

                CouponSearchCretirias cretirias = args.getParcelable(CRETIRIAS_FIELD);
                return new CouponUtils.GetCouponSearchResultsTask(this, cretirias);
            default:
                throw new UnsupportedOperationException("SearchActivity: onCreateLoader: No handler for id " + id);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<MiniCouponInfo>> loader, ArrayList<MiniCouponInfo> data) {
        switch (loader.getId()) {
            case SEARCH_COUPONS_LOADER_ID:
                loadingBar.setVisibility(View.GONE);
                if (data == null) {
                    Utils.showErrorMessage(SearchActivity.this, body, R.drawable.ic_warning, getString(R.string.message_server_data_error), true);
                    return;
                }

                body.removeAllViews();

                if (data.size() == 0) {
                    Utils.showErrorMessage(SearchActivity.this, body, R.drawable.ic_frown, getString(R.string.message_no_results), true);
                } else {
                    MiniCouponsListFragment miniCouponsListFragment = new MiniCouponsListFragment();

                    Bundle args = new Bundle();
                    args.putParcelableArrayList(ARGUMENTS_COUPONS_KEY, data);
                    args.putParcelable(ARGUMENTS_PHONE_LOCATION_KEY, Utils.getPhoneLocation(this));
                    miniCouponsListFragment.setArguments(args);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.linearLayout_activity_search_body, miniCouponsListFragment);
                    transaction.commit();
                }
            break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<MiniCouponInfo>> loader) {

    }
}
