import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor() { }

  getAuthors() {
    return ["Author1", "Author2", "Author53"];
  }
}
