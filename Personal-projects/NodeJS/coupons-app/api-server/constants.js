const constants = {
    'express': {
        EXPRESS_LISTEN_PORT: 3000,
        EXPRESS_LISTEN_IP: '192.168.1.105',
        BUSINESS_PROFILE_PICTURES_PATH: "/businesses",
        OWNER_PROFILE_PICTURES_PATH: "/owners",
        PROFILE_PICTURES_BASE_PATH: "profile-pics"
    },
    'router': {
        BUSINESS_OWNER_REGISTRATION_PATH: "/registerbusinessowner",
        BUSINESS_REGISTRATION_PATH: "/registerbusiness",
        COUPON_REGISTRATION_PATH: "/registercoupon",
        AUTHENTICATE_BUSINESS_OWNER_PATH: "/authenticatebusinessowner",
        RETRIEVE_BUSINESS_OWNER_DATA_PATH: "/businessownerdata",
        RETRIEVE_OWNER_BUSINESSES_PATH: "/ownerbusinesses",
        RETRIEVE_OWNER_COUPONS_PATH: "/ownercoupons",
        RETRIEVE_COUPON_PATH: "/coupon",
        RETRIEVE_BUSINESS_PATH: "/business",
        RETRIEVE_MINI_BUSINESSES_PATH: "/minibusinesses",
        RETRIEVE_COUPON_BY_SEARCH_PATH: "/searchcoupons",
        UPDATE_BUSINESS_OWNER_PROFILE_PATH: "/updateownerprofile",
        UPDATE_BUSINESS_PATH: "/updatebusiness",
        UPDATE_COUPON_PATH: "/updatecoupon",
        UPDATE_PROFILE_PICTURE_PATH: "/uploadprofilepicture",
        GET_ALL_BUSINESS_ID_AND_NAME: "/businesseslist",
        GET_NEARBY_COUPONS: "/nearbycoupons"
    },
    'database': {
        HOST: 'localhost',
        USERNAME: 'root',
        PASSWORD: 'root',
        DATABASE: 'couponsapplication',
        BUSINESS_OWNER_ACCOUNT_TABLE_NAME: "businessowneraccount",
        BUSINESSES_TABLE_NAME: "business",
        COUPONS_TABLE_NAME: "coupons"
    },
    'http_status_code': {
        COMPLETED_OK: 200,
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500
    },
    "response_code": {
        OPERATION_SUCCESS: 0,
        INTERNAL_SERVER_ERROR: 1,
        REGISTER_MISSING_EMAIL_OR_PASSWORD: 2,
        LOGIN_MISSING_EMAIL_OR_PASSWORD: 3,
        OWNER_PROFILE_AUTHENTICATION_SUCCESS: 4,
        OWNER_PROFILE_AUTHENTICATION_BAD_CREDENTIALS: 5,
        REGISTER_BUSINESS_MISSING_OWNER_ID_NAME_CATEGORY: 6,
        REGISTER_COUPON_MISSING_BUSINESS_ID: 7,
        BAD_ID_ARGUMENT: 8,
        UNKNOWN_ERROR: 9,
        REGISTER_EMAIL_EXISTS: 10,
        PROFILE_IMAGE_BAD_FORMAT: 11,
    },
    'headers': {
        RESPONSE_CODE: "RESPONSE_CODE"
    },
    'business_categories': {
        'Hamburgers': 0,
        'Pizza': 1,
        'Sushi': 2,
        'Coffee': 3
        // TODO: Add a list :-)
    },
    'general': {
        PROFILE_NOT_EXISTS_ID: -1,
        NO_DATABASE_CONNECTION_RESPONSE: {
            message: 'Handler is not connected to database.',
            code: 1,
            http_code: 500
        },
    }
};

module.exports = constants;
