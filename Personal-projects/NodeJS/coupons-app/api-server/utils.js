const constants = require("./constants.js");

const errorHandler = function (error, request, response, next) {
    console.error(error.stack);
    response.status(error.statusCode);
    response.send({
        RESPONSE_STATUS: constants.response_code.UNKNOWN_ERROR,
        message: 'Something broke!'
    });
}

module.exports = {
    ERROR_HANDLING_FUNCTION: errorHandler
};
