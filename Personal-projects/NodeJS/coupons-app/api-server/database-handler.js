const mysql = require("mysql");
const constants = require("./constants.js");
const SqlString = require('sqlstring');

const OWNER_PROFILE_INFO_FIELDS = ["emailAddress", "password", "firstname", "lastname", "address", "gender", "birthday", "languages", "graduatedOf", "phoneNumber", "facebookProfileUrl"];
const BUSINESS_FIELD = ["ownerId", "name", "rating", "category", "description", "address", "latitude", "longitude", "openingTimes"];
const COUPONS_FIELD = ["businessId", "lastValidity", "description", "title"];

class CouponsAppDatabaseHandler {

    constructor(host, user, password, dbname) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.dbname = dbname;
        this.isConnected = false;

        this.connection = mysql.createConnection({
            host: host,
            user: user,
            password: password,
            database: dbname,
            connectTimeout: 60000
        });
    }

    connect() {
        return new Promise((resolve, reject) => {
            this.connection.connect((err) => {
                if (err)
                    return reject(`Unable to connect ${this.user}@${this.host}/${this.dbname}: ${err}`);

                this.isConnected = true;
                return resolve(`Connected successfully to ${this.user}@${this.host}/${this.dbname}`);
            })
        })
    }

    /**
     * Registers a business owner profile.
     * param ownerProfileInfo: JSON that contains at least emailAddress and password.
     **/

    registerBusinessOwner(ownerProfileInfo) {
        return new Promise((resolve, reject) => {
            if (!ownerProfileInfo.emailAddress || !ownerProfileInfo.password)
                return reject({
                    message: 'emailAddress and/or password fields are missing.',
                    code: constants.response_code.REGISTER_MISSING_EMAIL_OR_PASSWORD,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            // Change all undefined to nulls.
            OWNER_PROFILE_INFO_FIELDS.forEach((field) => {
                // TODO: validate values
                if (!ownerProfileInfo.hasOwnProperty(field))
                    ownerProfileInfo[field] = null;
            });

            let query = `INSERT INTO ${constants.database.BUSINESS_OWNER_ACCOUNT_TABLE_NAME} VALUES (0, "${ownerProfileInfo.emailAddress}", "${ownerProfileInfo.password}", "${ownerProfileInfo.firstname}", "${ownerProfileInfo.lastname}", null, ${ownerProfileInfo.birthday}, null, null, null, null, null)`;
            this.connection.query(query, (err, result) => {
                if (err) {
                    if (err.errno === 1062)
                        err.errno = constants.response_code.REGISTER_EMAIL_EXISTS;

                    console.log(err);
                    return reject({
                        RESPONSE_STATUS: err.errno,
                        http_code: constants.http_status_code.COMPLETED_OK
                    });
                }
                return resolve(result);
            });
        });
    }

    /**
     * Adds a new business.
     */

    registerBusiness(businessInfo) {
        return new Promise((resolve, reject) => {
            if (!businessInfo.ownerId || !businessInfo.businessName || !businessInfo.category)
                return reject({
                    message: 'ownerId and/or name and/or category are missing.',
                    code: constants.response_code.REGISTER_BUSINESS_MISSING_OWNER_ID_NAME_CATEGORY,
                    http_code: constants.http_status_code.COMPLETED_OK
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            // Change all undefined to nulls.
            BUSINESS_FIELD.forEach((field) => {
                // TODO: validate values
                if (!Object.prototype.hasOwnProperty.call(businessInfo, field))
                    businessInfo[field] = null;
            });

            let query = `INSERT INTO ${constants.database.BUSINESSES_TABLE_NAME} VALUES (0, "${businessInfo.ownerId}", "${businessInfo.businessName}", 0.0, ${businessInfo.category}, "${businessInfo.description}", "${businessInfo.address}", ${businessInfo.latitude}, ${businessInfo.longitude}, ${SqlString.escape(businessInfo.openingTimes)})`;

            this.connection.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                }
                return resolve(result);
            });
        });
    }

    /**
     * adds new coupon
     **/

    registerCoupon(couponInfo) {
        return new Promise((resolve, reject) => {
            if (!couponInfo.businessId)
                return reject({
                    message: 'businessId is missing.',
                    code: constants.response_code.REGISTER_COUPON_MISSING_BUSINESS_ID,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            // Change all undefined to nulls.
            COUPONS_FIELD.forEach((field) => {
                // TODO: validate values
                if (!couponInfo.hasOwnProperty(field))
                    couponInfo[field] = null;
            });

            let query = `INSERT INTO ${constants.database.COUPONS_TABLE_NAME} VALUES (0, "${couponInfo.businessId}", "${couponInfo.lastValidity}", "${couponInfo.description}", "${couponInfo.title}")`;

            this.connection.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                }
                return resolve(result);
            });
        });
    }

    /**
     * Authenticates a business owner profile by given email and password.
     * param ownerLoginInfo: JSON that contains emailAddress and password fields.
     **/
    authenticateBusinessOwner(ownerLoginInfo) {
        return new Promise((resolve, reject) => {
            if (!ownerLoginInfo.emailAddress || !ownerLoginInfo.password)
                return reject({
                    message: 'emailAddress and/or password fields are missing.',
                    code: constants.response_code.LOGIN_MISSING_EMAIL_OR_PASSWORD,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            let query = `SELECT * FROM ${constants.database.BUSINESS_OWNER_ACCOUNT_TABLE_NAME} WHERE emailAddress = "${ownerLoginInfo.emailAddress}" AND password = "${ownerLoginInfo.password}"`;

            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result.length === 0 ? constants.general.PROFILE_NOT_EXISTS_ID : result[0].id);
            });
        });
    }

    /**
     * Returns all businesses for a given business owner id.
     * param businessOwnerId: business owner id.
     */
    getAllBusinessesOfBusinessOwnerByBusinessOwnerId(businessOwnerId) {
        return new Promise((resolve, reject) => {
            if (isNaN(parseInt(businessOwnerId)))
                return reject({
                    message: `businessOwnerId must be an integer, got ${typeof businessOwnerId}.`,
                    code: constants.response_code.BAD_ID_ARGUMENT,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            let query = `SELECT * FROM ${constants.database.BUSINESSES_TABLE_NAME} WHERE ownerId = ${businessOwnerId}`;

            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result);
            });
        });
    }

    /**
     * Returns all the coupons that associated with a given business owner id.
     * param businessOwnerId: business owner id.
     */
    getAllCouponsAssociatedWithOwnerId(businessOwnerId) {
        return new Promise((resolve, reject) => {
            if (isNaN(parseInt(businessOwnerId)))
                return reject({
                    message: `businessOwnerId must be an integer, got ${typeof businessOwnerId}.`,
                    code: constants.response_code.BAD_ID_ARGUMENT,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            let query = `SELECT ${constants.database.COUPONS_TABLE_NAME}.*, \
      ${constants.database.BUSINESSES_TABLE_NAME}.businessName as businessName, \
      ${constants.database.BUSINESSES_TABLE_NAME}.latitude as latitude, \
      ${constants.database.BUSINESSES_TABLE_NAME}.longitude as longitude \
      FROM coupons INNER JOIN business ON coupons.businessId = business.id WHERE business.ownerId = ${businessOwnerId}`;

            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result);
            });
        });
    }

    /**
     * Helper for get an item by id from a given table.
     * param tableName: name of the table.
     * param id: requested item's id.
     **/
    getItemById(tableName, id) {
        return new Promise((resolve, reject) => {
            if (isNaN(parseInt(id)))
                return reject({
                    message: `Argument id must be an integer, got ${typeof id}.`,
                    code: constants.response_code.BAD_ID_ARGUMENT,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            let query = `SELECT * from ${tableName} WHERE id = ${id}`;

            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                else if (result.length === 0)
                    return reject({
                        message: `No ${tableName} with id of ${id}`,
                        code: constants.response_code.BAD_ID_ARGUMENT,
                        http_code: constants.http_status_code.BAD_REQUEST
                    });
                return resolve(result[0]);
            });
        });
    }


    /**
     * Returns the businessOwner data for a given id.
     * param businessOwnerId: id to get his data.
     */
    getBusinessOwnerDataById(businessOwnerId) {
        return this.getItemById(constants.database.BUSINESS_OWNER_ACCOUNT_TABLE_NAME, businessOwnerId);
    }

    /**
     * Returns business by it's business id.
     * param businessId: business id.
     */

    getBusinessById(businessId) {
        return this.getItemById(constants.database.BUSINESSES_TABLE_NAME, businessId);
    }

    /**
     * Returns coupon by it's coupon id.
     * param couponId: coupon id.
     */
    getCouponById(couponId) {
        return new Promise((resolve, reject) => {
            if (isNaN(parseInt(couponId)))
                return reject({
                    message: `Argument id must be an integer, got ${typeof id}.`,
                    code: constants.response_code.BAD_ID_ARGUMENT,
                    http_code: constants.http_status_code.BAD_REQUEST
                });

            if (!this.isConnected)
                return reject(constants.general.NO_DATABASE_CONNECTION_RESPONSE);

            let query = `SELECT ${constants.database.COUPONS_TABLE_NAME}.*, ${constants.database.BUSINESSES_TABLE_NAME}.latitude, ${constants.database.BUSINESSES_TABLE_NAME}.longitude FROM ${constants.database.COUPONS_TABLE_NAME} INNER JOIN ${constants.database.BUSINESSES_TABLE_NAME} ON coupons.businessId = business.id  WHERE coupons.id = ${couponId}`;

            this.connection.query(query, (err, result) => {
                if (err) {
                    console.log(err);
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                }
                else if (result.length === 0)
                    return reject({
                        message: `No ${constants.database.COUPONS_TABLE_NAME} with id of ${couponId}`,
                        code: constants.response_code.BAD_ID_ARGUMENT,
                        http_code: constants.http_status_code.BAD_REQUEST
                    });
                return resolve(result[0]);
            });
        });
    }

    /**
     * Searches a coupons by the following possible cretirias:
     * 1. Category 2. LatLong coordinates and radius 3. Coupons validity end
     * 4. Business rating.
     * For each missing field, the filter will not apply.
     **/
    getCouponsByCretiria(cretiria) {
        let SEARCH_REQUIRED_PROPERTIES = ["category", "latitude", "longitude", "lastValidity", "rating", "radius"];
        // Change all undefined to nulls.
        SEARCH_REQUIRED_PROPERTIES.forEach((field) => {
            if (!cretiria.hasOwnProperty(field))
                cretiria[field] = null;
        });

        let query = `SELECT ${constants.database.COUPONS_TABLE_NAME}.*, ( 6371 * acos( cos( radians(${cretiria.latitude}) ) * cos( radians( business.latitude ) ) * cos( radians( business.longitude ) - radians(${cretiria.longitude}) ) + sin( radians(${cretiria.latitude}) ) * sin( radians( business.latitude ) ) ) ) AS distance, \
    ${constants.database.BUSINESSES_TABLE_NAME}.latitude, ${constants.database.BUSINESSES_TABLE_NAME}.longitude FROM coupons INNER JOIN ${constants.database.BUSINESSES_TABLE_NAME} ON coupons.businessId = business.id WHERE\
    (business.category = ${cretiria.category} OR ${cretiria.category} is null) AND\
    (coupons.lastValidity >= ${cretiria.lastValidity} OR ${cretiria.lastValidity} is null) AND\
    (business.rating <= ${cretiria.rating} OR ${cretiria.rating} is null)
    HAVING\
    distance < ${cretiria.radius} OR distance is null OR ${cretiria.radius} is null`;

        return new Promise((resolve, reject) => {
            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result);
            });
        });
    }

    /**
     * Gets a nearby coupons
     */

    getNearbyCoupons(args) {
        return new Promise((resolve, reject) => {
            if ((args.latitude && args.longitude && args.page && args.count) === undefined)
                return reject(CouponsAppDatabaseHandler.rejectErrorJSON("You must specify latitude, longitude, page and count arguments."));

            let query = `SELECT ${constants.database.COUPONS_TABLE_NAME}.*, ( 6371 * acos( cos( radians(${args.latitude}) ) * cos( radians( business.latitude ) ) * cos( radians( business.longitude ) - radians(${args.longitude}) ) + sin( radians(${args.latitude}) ) * sin( radians( business.latitude ) ) ) ) AS distance, \
        ${constants.database.BUSINESSES_TABLE_NAME}.latitude, ${constants.database.BUSINESSES_TABLE_NAME}.longitude FROM coupons INNER JOIN ${constants.database.BUSINESSES_TABLE_NAME} ON coupons.businessId = business.id\
        ORDER BY distance ASC\
        LIMIT ${(args.page - 1) * args.count}, ${args.count}`;

            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result);
            });
        });
    }

    updateGenericInfo(info, tableName) {
        return new Promise((resolve, reject) => {
            if (!info.id)
                return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));

            let data = "";
            let id = info.id;

            delete info.id; // Avoid the query to update it.
            Object.keys(info).forEach((key) => {
                data += `${key} = '${info[key]}', `;
            });

            if (data.length !== 0)
                data = data.slice(0, -2); // remove the last ", "
            else
                return resolve(0); // Nothing to update.

            let query = `UPDATE ${tableName} SET ${data} WHERE id = ${id}`;
            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result.affectedRows);
            });
        });
    }

    /**
     * Updates a owner profile info.
     * param ownerProfileInfo: an object with the new info.
     **/
    updateBusinessOwnerProfileInfo(ownerProfileInfo) {
        return this.updateGenericInfo(ownerProfileInfo, constants.database.BUSINESS_OWNER_ACCOUNT_TABLE_NAME);
    }

    /**
     * Updates a business record.
     * param businessInfo: an object with the new info.
     **/
    updateBusiness(businessInfo) {
        return this.updateGenericInfo(businessInfo, constants.database.BUSINESSES_TABLE_NAME);
    }

    /**
     * Updates a coupon record.
     * param couponInfo: an object with the new info.
     **/
    updateCoupon(couponInfo) {
        return this.updateGenericInfo(couponInfo, constants.database.COUPONS_TABLE_NAME);
    }

    getBusinessesIdAndNameList() {
        return new Promise((resolve, reject) => {
            let query = `SELECT id as businessId, businessName FROM ${constants.database.BUSINESSES_TABLE_NAME}`;
            this.connection.query(query, (err, result) => {
                if (err)
                    return reject(CouponsAppDatabaseHandler.rejectErrorJSON(err));
                return resolve(result);
            });
        });
    }

    static rejectErrorJSON(err) {
        let error = {
            message: err.message,
            RESPONSE_STATUS: err.errno,
            http_code: constants.http_status_code.BAD_REQUEST
        };

        console.log(error);
        return error;
    }
}

const db_handler = new CouponsAppDatabaseHandler(constants.database.HOST, constants.database.USERNAME, constants.database.PASSWORD, constants.database.DATABASE);

db_handler.connect().catch((err) => {
    throw err;
});

module.exports = db_handler;
