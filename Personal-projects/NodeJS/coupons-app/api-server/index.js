const express = require("express");
const bodyParser = require("body-parser");

const constants = require("./constants.js");
const router = require("./api-router.js");
const utils = require("./utils.js");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(utils.ERROR_HANDLING_FUNCTION);
app.use(express.static('profile-pics'));
app.use(router);

let server = app.listen(constants.express.EXPRESS_LISTEN_PORT, constants.express.EXPRESS_LISTEN_IP, () => {
    console.log(`Running on ${server.address().address}:${server.address().port}`);
});
